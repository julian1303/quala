/*
 *  $URL$
 *  $Author$
 *  $Date$
 *  
 *  $Copyright-Start$
 *
 *  Copyright (c) 2010
 *  RedPrairie Corporation
 *  All Rights Reserved
 *
 *  This software is furnished under a corporate license for use on a
 *  single computer system and can be copied (with inclusion of the
 *  above copyright) only for use on such a system.
 *
 *  The information in this document is subject to change without notice
 *  and should not be construed as a commitment by RedPrairie Corporation.
 *
 *  RedPrairie Corporation assumes no responsibility for the use of the
 *  software described in this document on equipment which has not been
 *  supplied or approved by RedPrairie Corporation.
 *
 *  $Copyright-End$
 */

package com.redprairie.les.varint;

import com.redprairie.mcs.MissingArgumentException;
import com.redprairie.moca.EditableResults;
import com.redprairie.moca.MocaArgument;
import com.redprairie.moca.MocaContext;
import com.redprairie.moca.MocaException;
import com.redprairie.moca.MocaResults;
import com.redprairie.moca.MocaType;
import com.redprairie.moca.NotFoundException;
import com.redprairie.moca.util.MocaUtils;
import com.redprairie.wmd.WMDConstants;
import com.redprairie.wmd.WMDErrors;
import com.redprairie.les.varint.CheckPreviousAllocationForAllocation.Pick;
import com.redprairie.wmd.util.InvAllocRuleUtils;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/**
 * This class contains the Java Method for the command <i>list available
 * inventory for pick in location</i>. For the specified work reference
 * (referred to as current pick here), the method returns inventory at source
 * location of the pick that matches the pick and is available for allocation.
 * <p>
 * If a pcklod is specified (this requires pcklodqty to also be specified), and
 * if inventory is found at the location with a matching lodnum, then a quantity
 * equaling pcklodqty from the load will be considered as unavailable for the
 * current pick. This enables inventory to be reserved without requiring a pick
 * to be created.
 * <p>
 * The method also considers other existing picks that source from the same
 * location. If such picks exist, and there is a match for these picks within
 * inventory that also matches the current pick, then that inventory will be
 * considered as unavailable for the current pick. For this check, only picks
 * using simple rules are considered; inventory is not committed for picks that
 * use complex rules.
 * 
 * <b><pre>
 * Copyright (c) 2010 RedPrairie Corporation
 * All Rights Reserved
 * </pre></b>
 * 
 * @author chala.d
 */
public class ListAvailableInventoryForPickInLocation {
    private final MocaContext moca;

    public ListAvailableInventoryForPickInLocation() {
        moca = MocaUtils.currentContext();
    }

    /**
     * This method maps to the command <i>list available inventory for pick in
     * location</i>.
     * 
     * @param wh_id Current warehouse ID
     * @param wrkref Work reference of pick for which available inventory (at
     * source location of the pick) is to be determined.
     * @param pcklod If specified, load from which inventory should be reserved 
     * @param pcklodqty Quantity from pcklod that should be reserved
     * @return Inventory available for the pick
     * @throws MocaException No-rows found exception if there is no inventory
     * available for the pick, or database query error.
     */
    public MocaResults execListAvailableInventoryForPickInLocation(
        String wh_id,
        String wrkref,
        String pcklod,
        Integer pcklodqty,
        Integer inc_subnum)
        throws MocaException {

        String alloc_rule_columns;
        String buffer;
        String order_by_clause;
        String rule_criteria;
        StringBuilder sb;
        String id_clause;
        MocaResults inv_res;
        MocaResults oth_res;
        MocaResults pck_res;
        MocaResults res;
        int[] inv_qty;
        int[] oth_qty;

        // This command requires wh_id and wrkref to list the available inv for
        // the pick
        wh_id = (wh_id == null) ? "" : wh_id.trim();
        wrkref = (wrkref == null) ? "" : wrkref.trim();
        if (wh_id.isEmpty()) {
            throw new MissingArgumentException("wh_id");
        }
        if (wrkref.isEmpty()) {
            throw new MissingArgumentException("wrkref");
        }

        // If inc_subnum is not passed then setting it
        // to 1 so that subnum and dtlnum are included
        // in the select statement
        if (inc_subnum == null) {
            inc_subnum = 1;
        }

        id_clause = (inc_subnum == 1) ?  " ivs.subnum, id.dtlnum,":  "";

        // If pcklod is passed in, pcklodqty is required so that we can reserve
        // the quantity for pcklod if the same load is also eligible for the
        // current pick.
        pcklod = (pcklod == null) ? "" : pcklod.trim();
        if (pcklod.isEmpty() == false && pcklodqty == null) {
            buffer = String.format("pcklod = '%s' was passed in but not " +
                "pcklodqty. pcklodqty is required if pcklod is specified.",
                pcklod);
            moca.trace(buffer);
            throw new MissingArgumentException("pcklodqty");
        }
        
        // Obtain pckwrk data for the specified work reference. We shall use
        // this select: (1) existing inventory at the location for the same item
        // that also matches rule criteria for this pick, and (2) other picks
        // with simple rules that source from the same location, for same item.
        //
        // The aim is to determine inventory available for the specified pick
        // after reducing quantities required by the other picks.
        buffer =
            "[select *" +
            "   from pckwrk_view" +
            "  where wrkref = '" + wrkref + "'] catch(-1403)";
        pck_res = moca.executeCommand(buffer);
        if (!pck_res.next()) {
            // This is an invalid pick reference
            throw new MissingArgumentException("wrkref");
        }
        
        sb = new StringBuilder();

        if (pck_res.getString("rule_nam") == null) {
            sb.append(" 1 = 1 ");
        }
        else {
            // Prepare rule criteria from the allocation rule for the specified
            // pick. The criteria will be used later as the where-clause for
            // selecting inventory from the location.
            buffer = String.format(
                "[select grpopr," +
                "        field_name," +
                "        operator," +
                "        value" +
                "   from alloc_rule_dtl" +
                "  where rule_nam = '%s'" +
                "    and wh_id = '%s'" +
                "  order by seqnum asc]",
                pck_res.getString("rule_nam"),
                wh_id);
            res = moca.executeCommand(buffer);
            if (res.getRowCount() == 0) {
                // The pick refers to an invalid rule name
                throw new MissingArgumentException("rule_nam");
            }
            while (res.next()) {
                // Use second argument, inventory data, as null to use field names
                // instead of inventory data on the left hand side
                InvAllocRuleUtils.addRule(sb, null, res);
            }
            res.close();
        }
        rule_criteria = sb.toString();
        
        res = null;
        
        // Now check the ALLOCATE-INV, ORDER-INV, INVENTORY-IN-LOCATION policy.
        // rtstr1 specifies inventory columns to order by when selecting
        // inventory at location during allocation. We just use the latest
        // policy, as ordered by the sort sequence.
        //
        // The policy specifies columns using table aliases il, ivs and id for
        // tables invlod, invsub and invdtl. So, we will be using the same
        // aliases in the queries below.
        buffer = String.format(
            "[select rtstr1" +
            "   from poldat_view" +
            "  where polcod = '%s'" +
            "    and polvar = '%s'" +
            "    and polval = '%s'" +
            "    and wh_id = '%s'" +
            "  order by srtseq desc] catch (-1403)",
            WMDConstants.POLCOD_ALLOCATE_INV,
            WMDConstants.POLVAR_ORDER_INV,
            WMDConstants.POLVAL_INVINLOC_ORDER_BY,
            wh_id);
        res = moca.executeCommand(buffer);
        order_by_clause = "";
        if (res.next() && res.isNull("rtstr1") == false) {
            String rtstr1 = res.getString("rtstr1").trim();
            if (rtstr1 != null && rtstr1.isEmpty() == false) {
                order_by_clause = " order by " + rtstr1;
            }
        }
        res.close();
        res = null;
        // If pick's lodlvl is 'L' then display
        // one row in 'LOOK_UP' form otherwise
        // same data will be displayed in multiple
        // rows.
        String prt_lodlvl = "";
        buffer = String.format(
                  "[select lodlvl"
                + "   from prtmst_view"
                + "  where prtnum = '%s'"
                + "    and prt_client_id = '%s'"
                + "    and wh_id = '%s'"
                + "] catch (-1403)",
                pck_res.getString("prtnum"),
                pck_res.getString("prt_client_id"),
                wh_id);
            res = moca.executeCommand(buffer);
            if (res.next()){
                prt_lodlvl = res.getString("lodlvl");
            }
            if (pck_res.getString("lodlvl").equals("L")
               && prt_lodlvl.equals("S")){
                id_clause = "";
                inc_subnum = 0;
            }
            res.close();
            res = null;

        // Get the list of all columns that can appear in allocation rules. The
        // table alias is for invdtl, see comment above for the policy query.
        alloc_rule_columns = InvAllocRuleUtils.getColumnList("id");

        // Now we select all inventory from the location for the part, using the
        // column names expected by allocation rules, rule criteria for the
        // specified pick, and the order-by clause from policy determined above.
        // See above comment for policy query for the table aliases used in this
        // query. We let no-rows-found error to propagate so that caller is
        // informed that there is no available inventory for the specified pick.
        // prtftp_dtl is included for untpal which should be returned, but this
        // can be queried only if an ftpcod is available.
        try{
        buffer =  String.format(
            "[select il.lodnum," +
            "        %1$s" +
            "        il.stoloc," +
            "        il.wh_id," +
            "        id.untcas," +
            "        sum(id.untqty) untqty," +
            "        sum(id.catch_qty) catch_qty," +
            "        pd.untqty untpal," +
            "        %2$s" + // select alloc_rule_columns
            "   from invlod il" +
            "   join invsub ivs" +
            "     on ivs.lodnum = il.lodnum" +
            "   join invdtl id" +
            "     on id.subnum = ivs.subnum" +
            "   left outer join prtftp_dtl pd" +
            "     on pd.prtnum = id.prtnum" +
            "    and pd.prt_client_id = id.prt_client_id" +
            "    and pd.ftpcod = id.ftpcod" +
            "    and pd.wh_id = il.wh_id" +
            "    and pd.pal_flg = 1" +
            "  where il.stoloc = '%3$s'" +
            "    and il.wh_id  = '%4$s'" +
            "    and id.prtnum = '%5$s'" +
            "    and id.prt_client_id = '%6$s'" +
            "    and (%7$s)" + // rule_criteria
            "  group by il.lodnum," +
            "        %1$s" +
            "        il.stoloc," +
            "        il.wh_id," +
            "        id.untcas," +
            "        id.ftpcod," +
            "        pd.untqty," +
			"        %2$s" + // group by alloc_rule_columns
            "   %8$s]", // order_by_clause
            id_clause,
            alloc_rule_columns,
            pck_res.getString("srcloc"),
            wh_id,
            pck_res.getString("prtnum"),
            pck_res.getString("prt_client_id"),
            rule_criteria,
            order_by_clause);
        inv_res =  moca.executeCommand(buffer);
        } 
        catch(MocaException error){
         // if the SQL retrun -1403 or 510, it means there are not enough quantity to pick. 
         // we should not throw out the meaningless error code, we should provide a 
         // meaningful error warning: 11428"Not enough quantity to pick". 
         int errCode = error.getErrorCode();
         if (errCode == WMDErrors.eDB_NO_ROWS_AFFECTED
             || errCode == WMDErrors.eSRV_NO_ROWS_AFFECTED) {
        
            throw new MocaException(WMDErrors.eINT_NOT_ENOUGH_QUANTITY);
         }
         else {
         // if this SQL get other exception, we throw it out directly so that keep 
         // the original logic.
             throw new MocaException(errCode);
         }
        }
        // For the inventory selected from the location (loc_invdtl), setup a
        // parallel array containing untqty from each row. We do this to
        // calculate how much of the untqty is available for picking, after
        // reducing the quantity required by existing picks.
        inv_qty = new int[inv_res.getRowCount()];
        for (int loc_index = 0; inv_res.next(); ++loc_index) {
            inv_qty[loc_index] = inv_res.getInt("untqty");
        }

        // If a pcklod was specified, remove quantities for it from inventory
        // queried above
        if (pcklod.isEmpty() == false && pcklodqty.intValue() > 0) {
            inv_res.reset();
            for (int inv_idx = 0; inv_res.next(); ++inv_idx) {
                if (inv_qty[inv_idx] <= 0 ||
                    inv_res.getString("lodnum").equals(pcklod) == false) {
                    continue;
                }
                if (pcklodqty.intValue() > inv_qty[inv_idx]) {
                    pcklodqty -= inv_qty[inv_idx];
                    inv_qty[inv_idx] = 0;
                } 
                else {
                    inv_qty[inv_idx] -= pcklodqty.intValue();
                    pcklodqty = 0;
                }
                buffer = String.format("Using inventory for pcklod '%s'. " +
                    "Remaining pcklodqty is %d.",
                    pcklod, pcklodqty);
                moca.trace(buffer);
                if (pcklodqty <= 0) {
                    break;
                }
            }
        }

        // Check if there is any inventory left that is available for the
        // specified pick. If not, throw a no-rows found exception.
        assertInvRemqtyNotZero(inv_qty, pck_res);

        // Get all the other existing picks with allocation rules from the same
        // location, for same item, that are not yet completed. If no such picks
        // exist, then any inventory at location matching the specified pick
        // is all available for the pick.
        buffer = String.format(
            "[select pckwrk_view.*," +
            "        alloc_rule_hdr.cplx_flg" +
            "   from pckwrk_view" +
            "   join alloc_rule_hdr" +
            "     on alloc_rule_hdr.rule_nam = pckwrk_view.rule_nam" +
            "    and alloc_rule_hdr.wh_id = pckwrk_view.wh_id" +
            "    and alloc_rule_hdr.cplx_flg is not null" +
            "  where pckwrk_view.wh_id  = '%s'" +
            "    and pckwrk_view.srcloc = '%s'" +
            "    and pckwrk_view.prtnum = '%s'" +
            "    and pckwrk_view.prt_client_id = '%s'" +
            "    and pckwrk_view.wrkref != '%s'" +
            "    and pckwrk_view.cmbcod <> 'dummy'" +
            "    and pckwrk_view.pcksts != 'C'" +
            "    and pckwrk_view.pckqty > pckwrk_view.appqty] catch(-1403)",
            wh_id,
            pck_res.getString("srcloc"),
            pck_res.getString("prtnum"),
            pck_res.getString("prt_client_id"),
            wrkref);
        oth_res = (EditableResults) moca.executeCommand(buffer);

        // For the other picks, set up a parallel array containing remaining
        // quantities. We use the array to track inventory consumed by these
        // picks so that the available inventory for the specified pick can be
        // determined.
        if (oth_res.getRowCount() > 0) {
            oth_qty = new int[oth_res.getRowCount()];
            for (int oth_idx = 0; oth_res.next(); ++oth_idx) {
                oth_qty[oth_idx] =
                    oth_res.getInt("pckqty") - oth_res.getInt("appqty");
            }
            
            // If the order is date controlled, we should check the expire_date
            // from the command "get best date controlled inventory" result set.
            String expire_date = null;
            
            buffer = String.format(
                    "check for date controlled inventory " +
                    "    where  wrkref = '" + wrkref + "'" +
                    "      and  wh_id  = '" + wh_id + "'");
            
            res = moca.executeCommand(buffer);
            
            if (res.next() && res.getString("ordinv") != null) {
                buffer = String.format(
                        "get best date controlled inventory" +
                        "    where  wrkref = '" + wrkref + "'" +
                        "      and  wh_id  = '" + wh_id + "'");
            
                res = moca.executeCommand(buffer);
            
                if(res.next() && res.getDateTime("expire_dte")!= null){
                    expire_date = new SimpleDateFormat(
                                  WMDConstants.JAVA_MOCA_DATE_FORMAT).
                                  format(res.getDateTime("expire_dte"));
                }
            }
            res.close();
            res = null;
            
            // Now, reduce quantities for the other picks from the inventory.
            // First, we reduce quantities where there is an exact match of
            // quantities (quantity required for the pick and quantity available
            // for picking from the load), specified by the last argument here.
            // Then, we reduce the quantities without requiring an exact match.
            reduceQtys(oth_res, oth_qty, inv_res, inv_qty,expire_date, true);
            if (!pck_res.getString("lodlvl").equals(WMDConstants.LODLVL_LOAD)) {
                reduceQtys(oth_res, oth_qty, inv_res, inv_qty,expire_date, false);
            }

            // Check if there is any inventory left that is available for the
            // specified pick. If not, throw a no-rows found exception.
            assertInvRemqtyNotZero(inv_qty, pck_res);
        }

        // Prepare the result set to be returned. It will contain all rows
        // from inv_res (inventory matching the specified pick) where there is
        // remaining quantity after considering pcklod and existing picks from
        // the location. 
        //
        // InvAllocRuleUtils.createEditableResults creates an editable result
        // set having columns for attributes that can appear in inventory
        // allocation rules. In addition to those columns, we add a few more
        // that give additional information.
        EditableResults return_res = null;
        return_res = InvAllocRuleUtils.createEditableResults(moca);
        return_res.addColumn("untcas", MocaType.INTEGER);
        return_res.addColumn("untpal", MocaType.INTEGER);
        return_res.addColumn("untqty", MocaType.INTEGER);
        return_res.addColumn("catch_qty", MocaType.DOUBLE);
        return_res.addColumn("lodnum", MocaType.STRING);
        if (inc_subnum == 1) {
            return_res.addColumn("subnum", MocaType.STRING);
            return_res.addColumn("dtlnum", MocaType.STRING);
        }
        return_res.addColumn("stoloc", MocaType.STRING);
        return_res.addColumn("wh_id", MocaType.STRING);
        inv_res.reset();
        for (int inv_idx = 0; inv_res.next(); ++inv_idx) {
            // Skip this if no more inventory is left in it for the current pick
            if (inv_qty[inv_idx] <= 0) {
				continue;
			}

            // Add a row for this inv_res row
            return_res.addRow();

            // Copy all attributes supported by allocation rules
            InvAllocRuleUtils.copy(return_res, inv_res);
            
            // For columns resulting from the sum function, column data type is
            // vendor-dependent (integer or double). So we include a data type
            // check for those columns.
            double catch_qty = 0.0;
            if (inv_res.getColumnType("catch_qty") == MocaType.DOUBLE) {
                catch_qty = inv_res.getDouble("catch_qty");
            } 
            else {
                catch_qty = (double) inv_res.getInt("catch_qty");
            }

            // Copy other columns.
            return_res.setIntValue("untcas", inv_res.getInt("untcas"));
            return_res.setIntValue("untpal", inv_res.getInt("untpal"));
            return_res.setIntValue("untqty", inv_qty[inv_idx]);
            return_res.setDoubleValue("catch_qty", catch_qty);
            return_res.setStringValue("lodnum", inv_res.getString("lodnum"));
            if (inc_subnum == 1) {
                return_res.setStringValue("subnum", inv_res.getString("subnum"));
                return_res.setStringValue("dtlnum", inv_res.getString("dtlnum"));
            }
            return_res.setStringValue("stoloc", inv_res.getString("stoloc"));
            return_res.setStringValue("wh_id", inv_res.getString("wh_id"));
        }

        // Done
        return return_res;
    }

    
    /*
     * To improve performance getData() is made local.  
     * getData() is called from check previous allocation from allocation command
     *
     * existingPckConsumedDtlnumLst is used to store dtlnums which are totally
     * consumed by picks which are removed from pckMap, so getAvailableInventory() in
     * CheckPreviousAllocationForAllocation.java can check if
     * detail is in the list to see if need to skip the detail number.
     */
    
    public HashMap<String,Pick> getData(String stoloc,
                                        String wh_id,
                                        String prtnum,
                                        String prt_client_id,
                                        List<String> existingPckConsumedDtlnumLst) throws MocaException {
        
        String wrkbuffer = "";
        MocaResults res;
        HashMap<String,CheckPreviousAllocationForAllocation.Pick> pckMap = new HashMap<String,Pick>();
        HashMap<String,Integer> usedInventory = new HashMap<String,Integer>();

            wrkbuffer = "and 1 = 1";

        /* Find all other picks against the same location that contain a complex rule.
         */
        try {
            res = moca.executeSQL("select pckwrk_view.wrkref, (pckwrk_view.pckqty - pckwrk_view.appqty) remqty, " +
                                   "       lodlvl, pckwrk_view.rule_nam " +
                                   "  from pckwrk_view, " +
                                   "       alloc_rule_hdr " +
                                   " where pckwrk_view.srcloc = :stoloc" +
                                   "   and pckwrk_view.wh_id  = :wh_id" +
                                   "   and pckwrk_view.prtnum = :prtnum" +
                                   "   and pckwrk_view.prt_client_id = :prt_client_id" +
                                   "   and pckwrk_view.pckqty > pckwrk_view.appqty " +
                                   "   and pckwrk_view.pcksts <> 'C' " +
                                   "   and pckwrk_view.rule_nam = alloc_rule_hdr.rule_nam " +
                                   "   and pckwrk_view.wh_id = alloc_rule_hdr.wh_id "+
                                   "   and alloc_rule_hdr.cplx_flg = 1 " + wrkbuffer,
                                   new MocaArgument("stoloc",stoloc),
                                   new MocaArgument("wh_id",wh_id),
                                   new MocaArgument("prtnum",prtnum),
                                   new MocaArgument("prt_client_id",prt_client_id));
            while(res.next()) {
                pckMap.put(res.getString("wrkref"),new Pick(res.getInt("remqty"), res.getString("lodlvl")));
            }
            moca.trace("Finished adding all picks to structure.");
            res.close();
            res = null;
            Iterator<String> it = pckMap.keySet().iterator();
            while(it.hasNext()) {    
                String tempKey = it.next();
                try {
                    // call the list available inventory for pick in location
                    res = execListAvailableInventoryForPickInLocation(
                             wh_id,
                             tempKey,
                             null,
                             null,
                             null);
                    while (res.next()) {
                        if (res.getRowCount() == 1){
                            /* Since there is only one row returned from 'list
                             * available inventory for pick in location', put
                             * the detail information to the pick structure.
                             */
                            pckMap.get(tempKey).put(res.getString("dtlnum"),
                                res.getInt("untqty"));
                            HashMap<String,Pick> temp = new HashMap<String,Pick>(pckMap);
                            pckMap = checkPartialUse(usedInventory,
                                                    temp,
                                                    res.getString("dtlnum"),
                                                    res.getInt("untqty"),
                                                    tempKey,
                                                    existingPckConsumedDtlnumLst);
                        }
                        else {
                            if(usedInventory.containsKey(res.getString("dtlnum"))) {
                                pckMap.get(tempKey).put(res.getString("dtlnum"),usedInventory.get(res.getString("dtlnum")));
                            }
                            else {
								pckMap.get(tempKey).put(res.getString("dtlnum"),res.getInt("untqty"));
							}
                        }
                    }
                }
                catch(MocaException e) {
                    continue;
                }
            }
        }
        catch(MocaException e){
            throw new MocaException(WMDErrors.eOK);
        }
        return pckMap;
    }
    private HashMap<String,Pick> checkPartialUse(HashMap<String,Integer> usedInventory,
                                                 HashMap<String,Pick> pckMap,
                                                 String dtlnum,
                                                 Integer quantity,
                                                 String wrkref,
                                                 List<String> existingPckConsumedDtlnumLst) {
        if((quantity - (pckMap.get(wrkref).getQtyRqd()) == 0) && !usedInventory.containsKey(dtlnum)){
            HashMap<String,Pick> temp = new HashMap<String,Pick>(pckMap);
            pckMap = removeSingleDetail(wrkref,dtlnum,temp,existingPckConsumedDtlnumLst);
        }
        else {
            if(usedInventory.containsKey(dtlnum)) {
                if(usedInventory.get(dtlnum)-pckMap.get(wrkref).getQtyRqd() == 0) {
                    HashMap<String,Pick> temp = new HashMap<String,Pick>(pckMap);                                        
                    pckMap = removeSingleDetail(wrkref,dtlnum,temp,existingPckConsumedDtlnumLst);
                    usedInventory.remove(dtlnum);
                    return pckMap;
                }
                else {
					usedInventory.put(dtlnum,usedInventory.get(dtlnum)-pckMap.get(wrkref).getQtyRqd());
				}
            }
            /* If dtlnum quantity is greater than quantity required by wrkref,
             * then used inventory should contain (quantity - wrkref(qty)).
             */
            else if (quantity - (pckMap.get(wrkref).getQtyRqd()) > 0) {
                usedInventory.put(dtlnum, quantity
                    - pckMap.get(wrkref).getQtyRqd());
            }
            else {
                usedInventory.put(dtlnum,quantity - pckMap.get(wrkref).getQtyRqd());
                existingPckConsumedDtlnumLst.add(dtlnum);
                pckMap.remove(wrkref);
            }
        }
        return pckMap;        
    }
    
    private HashMap<String,Pick> removeSingleDetail(String wrkref,
                                                    String dtlnum,
                                                    HashMap<String,Pick> pckMap,
                                                    List<String> existingPckConsumedDtlnumLst) {
   
        moca.trace("All inventory for dtlnum:  " + dtlnum +
                    " is needed by wrkref:  " + wrkref);
        existingPckConsumedDtlnumLst.add(dtlnum);
        pckMap.remove(wrkref);
        moca.trace("Removing wrkref:" + wrkref + " from removeSingleDetail");
        if(pckMap.isEmpty()) {
			return pckMap;
		}
        Iterator<String> it = pckMap.keySet().iterator();
        while(it.hasNext()){
            //tempKey is a wrkref.
            String tempKey = it.next();
            if(pckMap.get(tempKey).getMap().containsKey(dtlnum)){
                pckMap.get(tempKey).getMap().remove(dtlnum);
                if(pckMap.get(tempKey).getMap().isEmpty()) {
					return pckMap;
				}
                if(pckMap.get(tempKey).getMap().size() == 1){
                    Iterator<String> itTemp = pckMap.keySet().iterator();
                    HashMap<String,Pick> temp = new HashMap<String,Pick>(pckMap);
                    pckMap = removeSingleDetail(tempKey,itTemp.next(),temp,existingPckConsumedDtlnumLst);
                }
            }
        }
        return pckMap;
    }
    /**
     * This function checks if there is remaining quantity in inv_qty for the
     * pick in pck_res. If not, a no-rows found exception is thrown. This is
     * used to check if there is inventory for the specified pick after
     * quantities for inventory at location are reduced for pcklod or other
     * picks (that use simple allocation rule, inventory is not committed for
     * picks using complex allocation rules) that source from the same location.
     * 
     * @param inv_qty Remaining quantities for inventory at location
     * @param pck_res Pick data for the specified pick
     * @throws NotFoundException If no more inventory is available for pick
     */
    private void assertInvRemqtyNotZero(int[] inv_qty, MocaResults pck_res)
        throws NotFoundException {
        for (int inv_idx = 0; inv_idx < inv_qty.length; ++inv_idx) {
            if (inv_qty[inv_idx] > 0) {
				return;
			}
        }

        // There is no more inventory available for this pick
        String buffer = String.format("No inventory is available at " +
            "location '%s' for work reference '%s', after considering " +
            "inventory required for other existing picks at the location.",
            pck_res.getString("srcloc"),
            pck_res.getString("wrkref"));
        moca.trace(buffer);
        throw new NotFoundException();
    }
    
    /**
     * For picks that source from the same location as the current pick (pck),
     * their quantities (pck_qty) are reduced if a match is found with inventory
     * at location (inv) is found and there is sufficient quantity of the
     * inventory (inv_qty) available for the pick.
     * 
     * If exact_match_only is set, reduces quantities only if there is an exact
     * match. This enables a strategy of assigning exact quantities in an
     * initial pass and remainder in the next to be implemented.
     * 
     * @param pck Existing picks that source from current pick's source location
     * @param pck_qty (pckqty-appqty) array, corresponding to pck rows
     * @param inv Inventory at source location of current pick
     * @param inv_qty untqty array, corresponding to inv rows
     * @param exact_match_only If pck_qty and inv_qty should match exactly
     * @throws MocaException If a DB error occurs during matching process
     */
    private void reduceQtys(MocaResults pck, int[] pck_qty,
                            MocaResults inv, int[] inv_qty,
                            String expire_date,
                            boolean exact_match_only) throws MocaException {
        String buffer;
        pck.reset();
        for (int oth_idx = 0; pck.next(); ++oth_idx) {
            // If no more inventory is needed for this pick, skip to next pick
            if (pck_qty[oth_idx] <= 0) {
				continue;
			}

            // Check of some inventory at location can fill this pick
            inv.reset();
            for (int inv_idx = 0; inv.next(); ++inv_idx) {
                // If no more inventory is available in this load, skip it
                if (inv_qty[inv_idx] <= 0) {
					continue;
				}
                
                // If the pick does not match the inventory, skip to next one
                String result = InvAllocRuleUtils.match(moca, pck, inv);
                if (result != null) {
					continue;
				}
                
                // If the order is date controlled, we have to avoid reducing 
                // the quantity from the inventory which matches the expire date
                // from the command "get best date controlled inventory" as the
                // system expects the inventory matching this expire date during
                // picking.
                if(expire_date != null &&
                        inv.getDateTime("expire_dte") != null && 
                        !expire_date.equals(
                                new SimpleDateFormat(
                                        WMDConstants.JAVA_MOCA_DATE_FORMAT).
                                        format(inv.getDateTime("expire_dte")))){
                
                    // Reduce quantities since we have a match
                    int delta = pck_qty[oth_idx] - inv_qty[inv_idx];
                    if (exact_match_only && delta != 0) {
						continue;
					}
                    if (delta == 0) {
                        inv_qty[inv_idx] = 0;
                        pck_qty[oth_idx] = 0;
                    } 
                    else if (delta > 0) {
                        pck_qty[oth_idx] -= inv_qty[inv_idx];
                        inv_qty[inv_idx] = 0;
                    } 
                    else {
                        inv_qty[inv_idx] -=  pck_qty[oth_idx];
                        pck_qty[oth_idx] = 0;
                    }
                }

                // Indicate how the quantities are progressing
                buffer = String.format("Reduced quantity for pick '%s' from " +
                    "load '%s'. Remaining quantities are: for pick, %d; for " +
                    "load, %d.",
                    pck.getString("wrkref"),
                    inv.getString("lodnum"),
                    (pck_qty[oth_idx] < 0 ? 0 : pck_qty[oth_idx]),
                    (inv_qty[inv_idx] < 0 ? 0 : inv_qty[inv_idx]));
                moca.trace(buffer);
            }
        }
    }
}