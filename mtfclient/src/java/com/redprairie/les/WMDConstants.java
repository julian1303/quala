/*
 *  $URL$
 *  $Revision$
 *  $Author$
 *
 *  $Copyright-Start$
 *
 *  Copyright (c) 2010
 *  RedPrairie Corporation
 *  All Rights Reserved
 *
 *  This software is furnished under a corporate license for use on a
 *  single computer system and can be copied (with inclusion of the
 *  above copyright) only for use on such a system.
 *
 *  The information in this document is subject to change without notice
 *  and should not be construed as a commitment by RedPrairie Corporation.
 *
 *  RedPrairie Corporation assumes no responsibility for the use of the
 *  software described in this document on equipment which has not been
 *  supplied or approved by RedPrairie Corporation.
 *
 *  $Copyright-End$
 */

package com.redprairie.les;

import com.redprairie.mtf.foundation.presentation.CVirtualKey;
import com.redprairie.mtf.foundation.presentation.IVirtualKey;

/**
 * The declaration of constants that are of general interest to Warehouse
 * Management System (WMS). This includes declarations for the following
 * implementations at WMD product level:
 * <ol>
 * <li>Java MOCA components</li>
 * <li>Java RF Forms</li>
 * <li>JUnit tests.</li>
 * </ol>
 * 
 * <b>NOTE:</b> Constants that are only of interest to a particular subject
 * area should be defined for that subject area in the class:
 * <ul>
 * <li>com.redprairie.wmd.PackageName.WmdPackageName
 * </ul>
 * 
 * <b><pre>
 * Copyright (c) 2010 RedPrairie Corporation
 * All Rights Reserved
 * </pre></b>
 * 
 * @author smedic
 * @version $Revision$
 */
public class WMDConstants {

    // Asset Categories
    public static final String ASSET_CAT_INV                                    = "INV";

    // Allocate To Empty Codes for an area
    public static final String ATECOD_DISABLED                                  = "D";

    // Allocation Pick Types
    public static final String ALLOCATE_INVENTORY_MOVE                          = "INV-MOVE";
    public static final String ALLOCATE_PICK                                    = "PICK";
    public static final String ALLOCATE_PICKNREPLEN                             = "PICK-N-REPLEN";
    public static final String ALLOCATE_PICKNREPLENNSHIP                        = "PICK-N-REPLEN-N-SHIP";
    public static final String ALLOCATE_PICKNSHIP                               = "PICK-N-SHIP";
    public static final String ALLOCATE_REPLENISH                               = "REPLEN";
    public static final String ALLOCATE_TOPOFF_REPLEN                           = "TOPOFF-REPLEN";

    //Slotting Constants for attributes
    public static final String ATTR_NAME_ARECOD                                 = "AREA CODE";
    public static final String ATTR_NAME_PRTNUM                                 = "PART NUMBER";
    public static final String ATTR_NAME_PRT_CLIENT_ID                          = "PART CLIENT ID";
    public static final String ATTR_TYP_LIST                                    = "LIST";
    public static final String ATTR_TYP_STRING                                  = "CHAR STRING";

    // Colsts
    public static final String COLSTS_INPROC_NOT_CHANGABLE                      = "I";
    public static final String COLSTS_LOADING_NOT_CHANGABLE                     = "L";
    public static final String COLSTS_DEFER                                     = "D";

    // Batch status
    public static final String BATSTS_ALOC                                      = "ALOC";
    public static final String BATSTS_ALLOC_IN_PROC                             = "AINP";

    // Shipment Consolidation Constants.
    public static final String CONS_BY_ALL                                      = "ALL";
    public static final String CONS_BY_ORDNUM                                   = "ORDNUM";
    public static final String CONS_BY_RTCUST                                   = "RTCUST";
    public static final String CONS_BY_SHIP_ID                                  = "SHIP_ID";
    public static final String CONS_BY_STCUST                                   = "STCUST";
    public static final String CONS_BY_STOP_ID                                  = "STOP_ID";

    // Customer Order Required Constants.
    public static final String CUST_REQ_BTCUST                                  = "BTCUST";
    public static final String CUST_REQ_RTCUST                                  = "RTCUST";
    public static final String CUST_REQ_STCUST                                  = "STCUST";

    // Daily Transaction Logging Modes
    public static final String DLYTRN_MODE_ASYNC                                = "ASYNC";
    public static final String DLYTRN_MODE_OFF                                  = "OFF";
    public static final String DLYTRN_MODE_ON                                   = "ON";

    // Defaults
    public static final String DEFAULT_COLVAL                                   = "DEFAULT";
    public static final String DEFAULT_WH_ID                                    = "----";
    public static final String DEFAULT_CLIENT_ID                                = "----";
    public static final String DEFAULT_INV_ATTR                                 = "----";
    public static final String DEV_VARNAM_FOR_EXPLODE = "|ex_org_lodnum|stoloc|prtnum|new_lodnum|prt_client_id|lodflg|orig_subnum| ";
    public static final String DEV_VARNAM_FOR_UNLOAD                            = "UNLOAD";
    public static final String DEV_VARNAM_VEH_LOD_LIMIT_OVRD                    = "veh_lod_limit_ovrd";    

    // device refference
    public static final String DEV_VARNAM_FOR_TRANSFER                          = "xrf_src_stg";
    public static final String DEV_VALUE_FOR_VEHICLE_UNLIMITED                  = "VEHICLE_UNLIMITED";
    public static final String DEV_VARNAM_FOR_WRKQ_PRI_CHK_REQNUM               = "wrkq_pri_chk_reqnum";

    // Assigned Policy Suffix Constants.
    public static final String DEFPOL_SUFFIX                                    = "DEF";
    public static final String INVSTS_SUFFIX                                    = "STS";

    // Distro Timing
    public static final String DISTRO_TIMING_IDENTIFY                           = "IDFY";
    public static final String DISTRO_TIMING_RECEIVE                            = "RCV";
    public static final String DISTRO_TIMING_MSTRCPCLS                          = "MRC";

    // Exit Points
    public static final String EXITPNT_ALLOCINV                                 = "ALLOCINV";
    public static final String EXITPNT_BTOAUDCOMP                               = "BTOAUDCOMP";
    public static final String EXITPNT_OUTTRLRCLOSED                            = "OUTTRLRCLOSED";
    public static final String EXITPNT_CONTAINER_OPEN                           = "CONTAINER-OPEN";
    public static final String EXITPNT_CRTNCMPL                                 = "CRTNCMPL";
    public static final String EXITPNT_DEPTOPRCARE                              = "DEPTOPRCARE";
    public static final String EXITPNT_IDENTMANFSTCRTN                          = "IDENTMANFSTCRTN";
    public static final String EXITPNT_EXP_RESID                                = "EXP-RESID";
    public static final String EXITPNT_FSTLOADTRLR                              = "FSTLOADTRLR";
    public static final String EXITPNT_FST_LOD_PCK                              = "FST-LOD-PCK";
    public static final String EXITPNT_INVCMPL                                  = "INVCMPL";
    public static final String EXITPNT_INVMOV                                   = "INVMOV";
    public static final String EXITPNT_LOADTRLR                                 = "LOADTRLR";
    public static final String EXITPNT_LISTSTART                                = "LIST-START";
    public static final String EXITPNT_MANFSTCLSD                               = "MANFSTCLSD";
    public static final String EXITPNT_MANFSTCRTN                               = "MANFSTCRTN";
    public static final String EXITPNT_MANUAL                                   = "MANUAL";
    public static final String EXITPNT_MNFSTLSTCRTNORD                          = "MNFSTLSTCRTNORD";
    public static final String EXITPNT_MOVEFRMPRCARE                            = "MOVEFRMPRCARE";
    public static final String EXITPNT_MVLSTFRMPRCORD                           = "MVLSTFRMPRCORD";
    public static final String EXITPNT_MSTRCPTCHKIN                             = "MSTRCPTCHKIN";
    public static final String EXITPNT_MSTRCPTUNLOD                             = "MSTRCPTUNLOD";
    public static final String EXITPNT_ORDOVROPR                                = "ORDOVROPR";
    public static final String EXITPNT_PKOINIT                                  = "PKOINIT";
    public static final String EXITPNT_PKOPRTSCAN                               = "PKOPRTSCAN";
    public static final String EXITPNT_PKOERROR                                 = "PKOERROR";
    public static final String EXITPNT_PKOCLOSE                                 = "PKOCLOSE";
    public static final String EXITPNT_POST_IDENTIFY                            = "POST-IDENTIFY";
    public static final String EXITPNT_POST_LOAD_PLAN                           = "POST_LOAD_PLAN";
    public static final String EXITPNT_POST_PICK                                = "POST-PICK";
    public static final String EXITPNT_POST_RECEIVE                             = "POST-RECEIVE";
    public static final String EXITPNT_POST_ALLOCATE                            = "POST-ALLOCATE";
    public static final String EXITPNT_PRE_DEPOSIT                              = "PRE-DEPOSIT";
    public static final String EXITPNT_PRE_IDENTIFY                             = "PRE-IDENTIFY";
    public static final String EXITPNT_PRE_PACK                                 = "PRE-PACK";
    public static final String EXITPNT_PRE_PICK                                 = "PRE-PICK";
    public static final String EXITPNT_PRE_RECEIVE                              = "PRE-RECEIVE";
    public static final String EXITPNT_PREPFORSHP                               = "PREPFORSHP";
    public static final String EXITPNT_RCV_DEPOSIT                              = "RCV-DEPOSIT";
    public static final String EXITPNT_RCVTRKOPR                                = "RCVTRKOPR";
    public static final String EXITPNT_SHPALOCOPR                               = "SHPALOCOPR";
    public static final String EXITPNT_SHIPCREATED                              = "SHIPCREATED";
    public static final String EXITPNT_SHIPSTG                                  = "SHIPSTG";
    public static final String EXITPNT_SHPDCKOPR                                = "SHPDCKOPR";
    public static final String EXITPNT_SKIP_PICK                                = "SKIP-PICK";
    public static final String EXITPNT_SMLPKGOPR                                = "SMLPKGOPR";
    public static final String EXITPNT_STOPCMPL                                 = "STOPCMPL";
    public static final String EXITPNT_TRLRCLOSED                               = "TRLRCLOSED";
    public static final String EXITPNT_TRLRTODOCK                               = "TRLRTODOCK";
    public static final String EXITPNT_TRLRDISPATCHED                           = "TRLRDISPATCHED";
    public static final String EXITPNT_TRLRLOADED                               = "TRLRLOADED";
    public static final String EXITPNT_TRLRPRELDUNLD                            = "TRLRPRELDUNLD";
    public static final String EXITPNT_TRLRAUDIT                                = "TRLRAUDIT";
    public static final String EXITPNT_TRLRFROMDOCK                             = "TRLRFROMDOCK";
    public static final String EXITPNT_TYP_SERVICE_BCK                          = "SERVICE-BACKGROUND";
    public static final String EXITPNT_UNEXP_RESID                              = "UNEXP-RESID";
    public static final String EXITPNT_VEHCHNGVEHTYP                            = "VEHCHNGVEHTYP";
    public static final String EXITPNT_VEHLOGOFF                                = "VEHLOGOFF";
    public static final String EXITPNT_VEHLOGON                                 = "VEHLOGON";
    public static final String EXITPNT_TRLRCHECKIN                              = "TRLRCHECKIN";
    public static final String EXITPNT_TRLRTURNARND                             = "TRLRTURNARND";

    //Slotting Constants for attribute types
    public static final String IL_TYP_LOC                                       = "LOC";
    public static final String IL_TYP_ITEM                                      = "ITEM";

    // Inventory Move Types
    public static final String INVMOV_TYP_TRANSFER                              = "TRNS";
    
    // One Pass Only Values
    public static final String ONE_PASS_ONLY_ALWAYS                             = "A";

   // Move type
    public static final String MOVE_TYPE_IMMEDIATE                              = "I";

    // Shipment Status
    public static final String SHPSTS_LOADING                                   = "L";
    public static final String SHPSTS_TRANSFER                                  = "X";
    public static final String SHPSTS_LOADED                                    = "D";
    public static final String SHPSTS_LOAD_COMPLETE                             = "C";
    public static final String SHPSTS_IN_PROCESS                                = "I";
    public static final String SHPSTS_READY                                     = "R";
    public static final String SHPSTS_STAGED                                    = "S";
    

    // Shipment Line Status
    public static final String LINSTS_INPROCESS                                 = "I";
    public static final String LINSTS_PENDING                                   = "P";

    // Location Codes
    public static final String LOCCOD_EACHES                                    = "E";
    public static final String LOCCOD_LENGTH                                    = "L";
    public static final String LOCCOD_PALLET                                    = "P";
    public static final String LOCCOD_VOLUME                                    = "V";

    // Location Statuses
    public static final String LOCSTS_INV_ERROR                                 = "I";
    public static final String LOCSTS_PARTIAL                                   = "P";
    public static final String LOCSTS_EMPTY                                     = "E";
    public static final String LOCSTS_FULL                                      = "F";
    public static final String LOCSTS_LOCKED                                    = "L";

    // Override location status
    public static final String OVRD_LOCSTS_FULL                                 = "F";
    public static final String OVRD_LOCSTS_EMPTY                                = "E";
    public static final String OVRD_LOCSTS_NOCHANGE                             = "N";

    // Load levels
    public static final String LODLVL_DETAIL                                    = "D";
    public static final String LODLVL_DTL                                       = "D";
    public static final String LODLVL_LOAD                                      = "L";
    public static final String LODLVL_SUB                                       = "S";
    public static final String LODLVL_SUBLOAD                                   = "S";

    // Order Activity Logging Modes
    public static final String ORDACT_MODE_ASYNC                                = "ASYNC";
    public static final String ORDACT_MODE_OFF                                  = "OFF";
    public static final String ORDACT_MODE_ON                                   = "ON";
    
    // Constant for GS1 Bar Coding in Receive Without Order
    public static final String GTIN                                             = "GTIN";
    public static final String TRKALL                                           = "TRKALL";
    public static final String TRKPARTS                                         = "TRKPARTS";
    public static final String TRKPRTSEXPT                                      = "TRKPRTSEXPT";
    
    // Operation Codes
    public static final String OPRCOD_DIR_AUDIT_COUNT                           = "CNTAUD";
    public static final String OPRCOD_INBOUND_SERVICE                           = "INBSERV";
    public static final String OPRCOD_INVADJ                                    = "INVADJ";
    public static final String OPRCOD_LOAD                                      = "LOD";
    public static final String OPRCOD_OUTBOUND_SERVICE                          = "OUBSERV";
    public static final String OPRCOD_PICK                                      = "PCK";
    public static final String OPRCOD_UN_PICK                                   =  "UPCK";
    public static final String OPRCOD_RECEIVE                                   = "RCV";
    public static final String OPRCOD_RECOUNT                                   = "RCNT";
    public static final String OPRCOD_LIST_PICK                                 = "LPCK";
    public static final String OPRCOD_RESUME_PICK                               = "RLPCK";
    public static final String OPRCOD_STORE                                     = "STO";
    public static final String OPRCOD_STAGING_TRANSFER                          = "STN";
    public static final String OPRCOD_TRAILER_MOVE                              = "TRL";
    public static final String OPRCOD_UNDIR_AUDIT_COUNT                         = "UAUD";
    public static final String OPRCOD_PUTDIR                                    = "PUTDIR";
    public static final String OPRCOD_COUNT                                     = "CNT";
    public static final String OPRCOD_COUNT_PICK                                = "CNTPCK";
    public static final String OPRCOD_COUNT_DTL                                 = "CNTDTL";
    public static final String OPRCOD_COUNT_PHY                                 = "CNTPHY";
    public static final String OPRCOD_LIST_PICK_ORD                             = "LPCKORD";
    public static final String OPRCOD_LIST_PICK_RPL                             = "LPCKRPL";
    public static final String OPRCOD_LIST_PICK_WKO                             = "LPCKWKO";
    public static final String OPRCOD_TRLR_LOAD                                 = "LOAD";
    public static final String OPRCOD_TRAILER_AUDIT                             = "TAUD";
    public static final String OPRCOD_MANAUD                                    = "MANAUD";
    public static final String OPRCOD_SERNUMCAP                                 = "SERNUMCAP";
    public static final String OPRCOD_SHIPMENT_TRANSFER                         = "STN";
    public static final String OPRCOD_CASE_PICK                                 = "CPK";
    public static final String OPRCOD_PIECE_PICK                                = "PPK";
    public static final String OPRCOD_BULK_PICK                                 = "BPCK";
    public static final String OPRCOD_THRESHOLD_BULK_PICK                       = "TBPCK";
    public static final String OPRCOD_THRESHOLD_PICKING                         = "TPCK";
    public static final String OPRCOD_DEMAND_REPLENISHMENT                      = "PIARPL";
    public static final String OPRCOD_STAGE                                     = "STG";
    public static final String OPRCOD_CASE_REPLENISHMENT                        = "CRP";
    public static final String OPRCOD_TRANSFER                                  = "TRN";

    //Daily transaction lodnum values
    public static final String LODNUM_PASS                                      = "PASS";
    public static final String LODNUM_FAIL                                      = "FAIL";   
    
    // Pickable Code
    public static final String PCKCOD_SINGLE                                    = "S";
    public static final String PCKCOD_MIXED                                     = "M";
    public static final String PCKCOD_NOT_PICK                                  = "N";

    // Pickwork Status
    public static final String PCKSTS_HOLD                                      = "H";
    public static final String PCKSTS_PENDING                                   = "P";
    public static final String PCKSTS_RELEASED                                  = "R";
    public static final String PCKSTS_COMPLETED                                 = "C";

    // Cross Dock Codes
    public static final String XDKTYP_SHIPMENT                                  = "S";
    public static final String ALLOCATE_SHIPMENT_ONLY                           = "CREATE-SHIPMENTS-ONLY";

    // Policy Codes
    public static final String POLCOD_ALLOCATE_INV                              = "ALLOCATE-INV";
    public static final String POLCOD_ALLOCATION_OPERATIONS                     = "ALLOCATION-OPERATIONS";
    public static final String POLCOD_CONSOLIDATION                             = "ORDER-CONSOLIDATION";
    public static final String POLCOD_CUST_REQ_PROC                             = "CUSTOMER-REQUIREMNT-PROCESSING";
    public static final String POLCOD_DLYTRN                                    = "DLYTRN";
    public static final String POLCOD_IDENTIFY                                  = "IDENTIFY";
    public static final String POLCOD_BULK_PICKING                              = "BULK-PICKING";
    public static final String POLCOD_CROSS_DOCKING                             = "CROSS-DOCKING";
    public static final String POLCOD_INVENTORY_MANAGEMENT                      = "INVENTORY-MANAGEMENT";
    public static final String POLCOD_ORDACT                                    = "ORDACT";
    public static final String POLCOD_PACKOUT                                   = "PACKOUT";	
    public static final String POLCOD_PALLETCTL                                 = "PALLETCTL";
    public static final String POLCOD_PICK_STEALING                             = "PICK-STEALING";
    public static final String POLCOD_PROC_PICK_REL                             = "PICK-RELEASE";
    public static final String POLCOD_RCVTRKOPR                                 = "RCVTRKOPR";
    public static final String POLCOD_RDTALLOPR                                 = "RDTALLOPR";
    public static final String POLCOD_REPLENISHMENT                             = "REPLENISHMENT";
    public static final String POLCOD_RFID                                      = "RFID";
    public static final String POLCOD_SHIPPING                                  = "SHIPPING";
    public static final String POLCOD_SHIPPING_CARTONIZATION                    = "SHIPPING-CARTONIZATION";
    public static final String POLCOD_STORE_ASGLOC                              = "STORE-ASG-LOC";
    public static final String POLCOD_STORE_DEFARE                              = "STORE-DEF-AREA";
    public static final String POLCOD_STORE_PREFARE                             = "STORE-PREF-AREA";
    public static final String POLCOD_SYSTEM_INFORMATION                        = "SYSTEM-INFORMATION";
    public static final String POLCOD_UCC128_PROC                               = "UCC128-PROCESSING";
    public static final String POLCOD_WORK_ORDER_PROC                           = "WORK-ORDER-PROCESSING";
    public static final String POLVAL_WILDCARD                                  = "XXXX";
    public static final String POLCOD_BTO                                       = "BTO";
    public static final String POLCOD_EMERGENCY_REPLEN                          = "EMERGENCY-REPLENISHMENTS";
    public static final String POLVAL_ENABLE_SHORT_PROCESSING                   = "ENABLE-SHORT-PROCESSING";
    public static final String POLCOD_PARCEL_MANIFEST                           = "PARCEL-MANIFEST";
    public static final String POLCOD_PCKRELMGR                                 = "PCKRELMGR";
    public static final String POLCOD_PCKRSVMGR                                 = "PCKRSVMGR";
    public static final String POLCOD_LMS                                       = "LABOR-MANAGEMENT-SYSTEM";
    public static final String POLCOD_PRE_INV_PICKING                           = "PRE-INVENTORY-PICKING";
    public static final String POLCOD_MOVE_INVENTORY                            = "MOVE-INVENTORY";
    public static final String POLCOD_INVADJOPR                                 = "INVADJOPR";
    public static final String POLCOD_HOLD_PROCESSING                           = "HOLD-PROCESSING";
    public static final String POLCOD_PRDLINOPR                                 = "PRDLINOPR";
    public static final String POLCOD_CNTAUDOPR                                 = "CNTAUDOPR";
    public static final String POLCOD_CYCLE_COUNTING                            = "CYCLE-COUNTING";
    public static final String POLCOD_LIST_PICKING                              = "LIST-PICKING";
    public static final String POLCOD_THIRD_PARTY_LOGISTICS                     = "THIRD-PARTY-LOGISTICS";
    public static final String POLCOD_ASSET_TRACKING                            = "ASSET-TRACKING";
    public static final String POLCOD_RFID_YARD                                 = "RFID-YARD";

    // Policy Variables
    public static final String POLVAR_CONSOLIDATION_DEFAULT                     = "DEFAULT";
    public static final String POLVAR_ALLOCATION_RULES                          = "ALLOCATION-RULES";
    public static final String POLVAR_MISCELLANEOUS                             = "MISCELLANEOUS";
    public static final String POLVAR_INTEGRATION_SYS_ID                        = "INTEGRATION-SYS-ID";
    public static final String POLVAR_LOGGING                                   = "LOGGING";
    public static final String POLVAR_OPERATING_MODE                            = "OPERATING-MODE";
    public static final String POLVAR_ORDER_INV                                 = "ORDER-INV";
    public static final String POLVAR_THREADING                                 = "THREADING";
    public static final String POLVAR_WRKORD_POST_CRE_MOVE_TO                   = "POST-CREATE-MOVE-INVENTORY-TO";
    public static final String POLVAR_INSTALLED                                 = "INSTALLED";
    public static final String POLVAR_DEFAULT                                   = "NONE";
    public static final String POLVAR_BY_CATCH_QTY                              = "BY-CATCH_QTY";
    public static final String POLVAR_CONFIGURATION                             = "CONFIGURATION";
    public static final String POLVAR_PART_SHORTAGE_ENABLED                     = "PART-SHORTAGE";
    public static final String POLVAR_MISC                                      = "MISCELLANEOUS";
    public static final String POLVAR_INVENTORY                                 = "INVENTORY";
    public static final String POLVAR_ATTACHMENT_STRATEGY_FOR_AREAS             = "ATTACHMENT-STRATEGY-FOR-AREAS";
    public static final String POLVAR_LOADING                                   = "LOADING";
    public static final String POLVAR_ZERO_COUNT                                = "ZERO-COUNT";
    public static final String POLVAR_TRANSFER                                  = "TRANSFER";
    public static final String POLVAR_PRTNUM                                    = "prtnum";
    public static final String POLVAR_INVSUMDSP                                 = "INVSUMDSP";
    public static final String POLVAR_CLR_AST_DTL_ON_FLW_TO_AST_IDNTFY          = "CLR-ASSET-DTL-ON-FLW-TO-ASSET-IDENTIFY";
    public static final String POLVAR_RFID_FORMS                                = "RFID-FORMS";
    public static final String POLVAR_RELEASE_RULES                             = "RELEASE-RULES";
    public static final String POLVAR_AUTO_CLOSE_TRUCKS                         = "AUTO-CLOSE-TRUCKS";

    // Policy Values
    public static final String POLVAL_ABS_ORDINV_WIN                            = "ABS-ORDINV-WIN";
    public static final String POLVAL_ABS_ORDINV_CODE                           = "ABS-ORDINV-CODE";
    public static final String POLVAL_ALLOCATION_TYPE                           = "ALLOCATION-TYPE";
    public static final String POLVAL_ALLOW_DESTINATION_CHANGE                  = "ALLOW-DESTINATION-CHANGE";
    public static final String POLVAL_AUTO_ADD_EXST_LOD                        = "AUTO-ADD-TO-EXISTING-LOAD";
    public static final String POLVAL_ARECOD                                    = "ARECOD";
    public static final String POLVAL_COMMIT_DURING_ALLOC                       = "COMMIT-DURING-ALLOC";
    public static final String POLVAL_CONSOLIDATION_WINDOW                      = "WINDOW";
    public static final String POLVAL_DEF_RCV_LOC                               = "DEF-RCV-LOC";
    public static final String POLVAL_DEFAULT_MODE                              = "DEFAULT-MODE";
    public static final String POLVAL_ENABLED                                   = "ENABLED";
    public static final String POLVAL_INSTALLED                                 = "INSTALLED";
    public static final String POLVAL_PARALLEL_ALLOC                            = "IN-PARALLEL-WITH-ALLOCATION";
    public static final String POLVAL_PERFORM_CONSOLIDATION_BY                  = "PERFORM-CONSOLIDATION-BY";
    public static final String POLVAL_MATCHING_CRITERIA_FOR_ORDER               = "MATCHING-CRITERIA-FOR-ORDER";
    public static final String POLVAL_REL_GROUPING                              = "RELEASE-BY-GROUPING";
    public static final String POLVAL_REL_PALLET_VOLUME                         = "PALLET-VOLUME";
    public static final String POLVAL_RCV_ALLOW_UNEXPECTED_STATUS               = "ALLOW-UNEXPECTED-STATUS";
    public static final String POLVAL_RCV_ALLOW_UNEXPECTED_PART                 = "ALLOW-UNEXPECTED-PART";
    public static final String POLVAL_WKO_ALLOW_UNEXPECTED_PART                 = "ALLOW-UNEXPECTED-PART";
    public static final String POLVAL_SLOTTING_GROUP_RPLWRK_BY                  = "SLOTTING-GROUP-RPLWRK-BY";
    public static final String POLVAL_SHIPMENT_PROCESS_STATUS                   = "ALLOW-SEND-SHIPMENT-STATUS";
    public static final String POLVAL_STATUS_ROLE                               = "STATUS-ROLE";
    public static final String POLVAL_WRKORD_RCV_DEFAULTS                       = "RECEIVING-DEFAULTS";
    public static final String POLVAL_MAX_ALLOC_BATCHES_AHEAD                   = "MAX-ALLOC-BATCHES-AHEAD";
    public static final String POLVAL_WRKORD_CRT_DEFAULTS                       = "CREATE-INV-DEFAULTS";
    public static final String POLVAL_ACCEPTABLE_DELAY                          = "ACCEPTABLE-DELAY";
    public static final String POLVAL_REMOTE_MOCA_INFORMATION                   = "REMOTE-MOCA-INFORMATION";
    
    public static final String POLVAL_DYN_SLOT_TEMPLATE                         = "DYNAMIC-SLOT-PLAN-TEMPLATE";
    public static final String POLVAL_REMOVE_DYN_SLOT_PLAN                      = "REMOVE-PROCESSED-DYN-SLOT-PLAN"; 
    public static final String POLVAL_CANT_MODIFY_BTO_SHIPMENT_AFTER_INPR       = "CANT-MODIFY-BTO-SHIPMENT-AFTER-INPR";
    public static final String POLVAL_CANT_MODIFY_SHIPMENT_AFTER_INPR           = "CANT-MODIFY-SHIPMENT-AFTER-INPR";
    public static final String POLVAL_DEFER_SHIPMENT_CHANGE_WHILE_INPR          = "DEFER-SHIPMENT-CHANGE-WHILE-INPROCESS";
    public static final String POLVAL_CANT_MODIFY_SHIPMENT_AFTER_LOADING        = "CANT-MODIFY-SHIPMENT-AFTER-LOADING";
    public static final String POLVAL_KEEP_EXISTING_RESERVATION                 = "KEEP-EXISTING-RESERVATIONS";
    public static final String POLVAL_THREAD_COUNT                              = "THREAD-COUNT";
    
    public static final String POLVAL_DEFAULT_EXPDTE_FOR_RCV                    = "DEFAULT-EXPDTE-FOR-RCV";
    public static final String POLVAL_DEFAULT_EXPDTE_FOR_WKO                    = "DEFAULT-EXPDTE-FOR-WKO";
    public static final String POLVAL_DEFAULT_LAST_ASSET_CFG                    = "DEFAULT-LAST-ASSET-CONFIG";
    public static final String POLVAL_CONFIGURATION                             = "CONFIGURATION";
    public static final String POLVAL_DEFAULT                                   = "NONE";
    public static final String POLVAL_DSPL_CUST_INFO_PICKING                    = "DISPLAY-CUST-INFO-PICKING";
    public static final String POLVAL_VALIDATE_SSCC_LOAD                        = "VALIDATE-SSCC-LOAD";
    public static final String CNTTYP_RE_COUNT                                  = "R";
    public static final String NUMCOD_DISTRO_EXCP_ID                            = "distro_excp_id";
    public static final String POLVAL_DEFAULT_PART_DETAIL                       = "DEFAULT-PART-DTL";

    public static final String POLVAL_UOM_THRESHOLD                             = "UOM-THRESHOLD";
    public static final String POLVAL_ATTACH_TO_ANY                             = "ATTACH-TO-ANY";
    public static final String POLVAL_ATTACH_IF_UNIQUE                          = "ATTACH-IF-UNIQUE";
    public static final String POLVAL_ATTACH_IF_EXISTS                          = "ATTACH-IF-EXISTS";
    public static final String POLVAL_PALLET_SLOTS_CAPTURED                     = "PALLET-SLOTS-CAPTURED";
    public static final String POLVAL_REACOD                                    = "REACOD";
    public static final String POLVAL_REACOD_REQUIRED                           = "REACOD-REQUIRED";
    public static final String POLVAL_HOLD_CODE                                 = "HOLD-CODE";
    public static final String POLVAL_RF_SHRT_LEN_TIM_IN_SEC                    = "RF-SHORT-TIMER-LENGTH-IN-SECONDS";
    public static final String POLVAL_RF_LOOP_TIM_IN_SEC                        = "RF-LOOP-TIMER-LENGTH-IN-SECONDS";
    public static final String POLVAL_RF_PRODUCTION_RECEIVING_AGAINST_ITEM      = "RF_PRODUCTION_RECEIVING_AGAINST_ITEM";
    public static final String POLVAL_RESUME_PCK_PREV_OPR                       = "RESUME-PCK-PREV-OPR";
    public static final String POLVAL_FORCE_PICKUP_PREVIOUS                     = "FORCE-PICKUP-PREVIOUS";
    public static final String POLVAL_CURRENT_LOCATION_AT_LOGIN                 = "CURRENT-LOCATION-AT-LOGIN";
    public static final String POLVAL_CONF_THRESH_PICKUP                        = "CONF-THRESH-PICKUP";
    public static final String POLVAL_RC_PRT_CLIENT_PRTNUM                      = "rc1.prt_client_id||'|'||rc1.prtnum";
    public static final String POLVAL_MAX_IDENTICAL_LOADS                       = "MAX-IDENTICAL-LOADS";
    public static final String POLVAL_RF_TIMER_LENGTH_IN_SECONDS                = "RF-TIMER-LENGTH-IN-SECONDS";
    public static final String POLVAL_RF_LOOK_WORK_TIMEOUT                      = "RF-LOOK-WORK-TIMEOUT";
    public static final String POLVAL_CLR_ASSET_ENABLED                         = "CLR-ASSET-ENABLED";
    public static final String POLVAL_LOAD_PREPARATION                          = "LOAD-PREPARATION";
    public static final String POLVAL_ENABLE_SUB_ASSETS                         = "ENABLE_SUB_ASSETS";
    public static final String POLVAL_TRANSFER_INVENTORY                        = "TRANSFER-INVENTORY";
    public static final String POLVAL_ASSIGNMENT_FILTERS_ENABLED                = "ASSIGNMENT-FILTERS-ENABLED";
    public static final String POLVAL_CNT_MODE_IN_SNG_PRT_AREAS                 = "COUNT-MODE-IN-SNG-PART-AREAS";
    public static final String POLVAL_CNT_MODE_IN_MUL_PRT_AREAS                 = "COUNT-MODE-IN-MUL-PART-AREAS";
    public static final String POLVAL_AUTO_COMPLETE_IN_SNG_PART_AREA            = "AUTO-COMPLETE-IN-SNG-PART-AREA";
    public static final String POLVAL_LODNUM_MIN_AND_MAX                        = "LODNUM-MIN-AND-MAX";
    public static final String POLVAL_CREATE_TRUCKS                             = "CREATE-TRUCKS";
    public static final String POLVAL_INVOICE_RECEIVING                         = "INVOICE-RECEIVING";
    public static final String POLVAL_ALLOW_RCV_TRLR_CHECKIN                    = "ALLOW-RCV-TRLR-CHECKIN";
    public static final String POLVAL_PICK_N_PASS_AUTO_DROP_WRKZON              = "PICK-N-PASS-AUTO-DROP-WRKZON";
    public static final String POLVAL_MSG_CONTINUE_NEXT_ZONE                    = "MSG-CONTINUE-NEXT-ZONE";
    public static final String POLVAL_MSG_CONTINUE_NEXT_CNTBAT                  = "MSG-CONTINUE-NEXT-CNTBAT";
    public static final String POLVAL_SINGLE_SCAN_LOADING                       = "SINGLE-SCAN-LOADING";
    public static final String POLVAL_ENABLE_STACKABILITY                       = "ENABLE-STACKABILITY";
    public static final String POLVAL_FORCE_INV_DEP_VIS                         = "FORCE-INV-DEP-VIS";
    public static final String POLVAL_DEFAULT_UOM_FOR_GTIN                      = "DEFAULT-UOM-FOR-GTIN";
    public static final String POLVAL_PICK_N_PASS_AUTO_DROP_LOCATION            = "PICK-N-PASS-AUTO-DROP-LOCATION";
    public static final String POLVAL_SHOW_HOLD_DTL_ON_RF                       = "SHOW-HOLD-DTL-ON-RF";
    public static final String POLVAL_RECEIVING_DEFAULTS                        = "RECEIVING-DEFAULTS";
    public static final String POLVAL_ADDON_ID                                  = "ADDON_ID";
    public static final String POLVAL_ALLOW_PTAY_BFR_DTYPRC_CMPL                = "ALLOW-PUTAWAY-BEFORE-DUTYPROC-COMPL";
    public static final String POLVAL_ALLOW_CST_CNSGN_CRT_AT_RCPT               = "ALLOW-CSTMS-CNSGN-CREATE-AT-RCPT";
    public static final String POLVAL_BACKGROUND_POLL_MTF                       = "BACKGROUND-POLL-MTF";
    public static final String POLVAL_ALLOW_OVERUNDER_CONSUME                   = "ALLOW-OVERUNDER-CONSUME";
    public static final String POLVAL_CONFIRM_MULTIUSE_PART                     = "CONFIRM-MULTIUSE-PART";
    public static final String POLVAL_SHOW_LOC_ON_RF_OVRLOC                     = "SHOW-LOC-ON-RF-OVRLOC";
    public static final String POLVAL_CLOSE_STRATEGY                            = "CLOSE-STRATEGY";
    public static final String POLVAL_CLOSE_ON_RF_USER_CONF                     = "CLOSE-ON-RF-USER-CONF";
    public static final String POLVAL_PROMPT_FOR_EOD                            = "PROMPT-FOR-EOD";
    public static final String POLVAL_CONFIRM_CREATE_INVENTORY                  = "CONFIRM-CREATE-INVENTORY";
    public static final String POLVAL_DROP_AT_WRKZONE_CHANGE                    = "DROP-AT-WRKZONE-CHANGE";
    public static final String POLVAL_DOCNUM_ASSIGNMENT_LEVELS                  = "DOCNUM-ASSIGNMENT-LEVELS";
    public static final String POLVAL_CAPTURE_DOCUMENT_NUMBER                   = "CAPTURE-DOCUMENT-NUMBER";
    public static final String POLVAL_RF_PROD_RECEIVING_AGAINST_ITEM            = "RF_PRODUCTION_RECEIVING_AGAINST_ITEM";
    public static final String POLVAL_LABELS_AFTER_IDENTIFY                     = "LABELS-AFTER-IDENTIFY";
    public static final String POLVAL_LBL_LOD_AFTER_IDENT_PCK                   = "LABEL-LOAD-AFTER-UNDIR-PICK";
    public static final String POLVAL_PICKUP_FORM_WRKREF_ENABLED                = "PICKUP-FORM-WRKREF-ENABLED";
    public static final String POLVAL_PICKUP_FORM_DEST_ENABLED                  = "PICKUP-FORM-DEST-ENABLED";
    public static final String POLVAL_DST_ID_ENABLED                            = "DST-ID-ENABLED";
    public static final String POLVAL_LABEL_LOAD                                = "LABEL-LOAD";
    public static final String POLVAL_MERGE_ON_DEPOSIT                          = "MERGE-ON-DEPOSIT";
    public static final String POLVAL_CANCEL_PICK_WRKREF_ENABLED                = "CANCEL-PICK-WRKREF-ENABLED";
    public static final String POLVAL_CASE_CONFIRM_FORM_AFTER_CMP               = "CASE-CONFIRM-FORM-AFTER-CMP";
    public static final String POLVAL_TRANSFER_WRKREF_ENABLED                   = "TRANSFER-WRKREF-ENABLED";
    public static final String POLVAL_DEPOSIT_LODNUM_ENABLED                    = "DEPOSIT-LODNUM-ENABLED";
    public static final String POLVAL_DEFAULT_PICKING_ATTRIBUTES                = "DEFAULT-PICKING-ATTRIBUTES"; 
    public static final String POLVAL_DISPLAY_DEPOSIT_SUBUCC_IDENTIFIER         = "DISPLAY-DEPOSIT-SUBUCC-IDENTIFIER";
    public static final String POLVAL_CONFIRM_PD_LOAD_DEPOSIT                   = "CONFIRM-PD-LOAD-DEPOSIT";
    public static final String POLVAL_CROSSDOCK_LODNUM_ENABLED                  = "CROSSDOCK-LODNUM-ENABLED";
    public static final String POLVAL_AUTO_CLOSE_TRAILER                        = "AUTO-CLOSE-TRAILER";
    public static final String POLVAL_AUTO_DISPATCH_TRAILER                     = "AUTO-DISPATCH-TRAILER";
    public static final String POLVAL_AUTO_COMPLETE_STOP                        = "AUTO-COMPLETE-STOP";
    public static final String POLVAL_DOCNUM_REQUIRED_FOR_PAPERWORK             = "DOCNUM-REQUIRED-FOR-PAPERWORK";
    public static final String POLVAL_ALLOW_LTL_SHIPMENT_SPLIT                  = "ALLOW-LTL-SHIPMENT-SPLIT";
    public static final String POLVAL_ALLOW_AUTO_DEPOSIT                        = "ALLOW-AUTO-DEPOSIT";
    public static final String POLVAL_COUNT_WEEK                                = "COUNT-WEEK";
    public static final String POLVAL_DEF_NUM_DAYS_CYCLE_PERIOD                 = "DEF-NUM-DAYS-CYCLE-PERIOD";
    public static final String POLVAL_INSTALL_DATE                              = "INSTALL-DATE";

    // Pallet Control
    public static final String PALCTLSTS_DONE                                   = "DONE";
    public static final String PALCTL_STACKCOMPLETE                             = "ENABLE-STACKABILITY";
    public static final String WAITING_PLTCTL_PCKWRK_HOLD                       = "WPPH";
    public static final String WAITING_PLTCTL_PCKWRK_PEND                       = "WPPP";

    // Process Modes
    public static final String PRCMOD_ALLOCATE_ONLY                             = "a";
    
    // Empty Part
    public static final String PRTNUM_EMPTY                                     = "EMPTYPART";

    // Alternate Part Type
    public static final String ALT_PRT_TYPE                                     = "GTIN";

    // Replenishment Status
    public static final String RPLSTS_CROSS_DOCK                                = "X";
    public static final String RPLSTS_ISSUED                                    = "I";
    public static final String RPLSTS_WAIT_ORTEC                                = "O";
    public static final String RPLSTS_WAITING_FOR_WORK_ORDER                    = "W";

    // Trailer Safety Status Constants.
    public static final String SAFE_STS_UNKNOWN                                 = "U";

    // Move Inventory Special Indicator Value
    public static final String SPCIND_RDT_STG_XFR                               = "RDT-STG-XFR";
    public static final String SPCIND_PRA_REVERSE                               = "PRA-REVERSE";

    // Suggestion Codes
    public static final String SUGCOD_CURRENT_APPT                              = "CA";
    public static final String SUGCOD_FUTURE_APPT                               = "FA";
    public static final String SUGCOD_AVAIL_RCV_DCK_DR                          = "ARD";
    public static final String SUGCOD_MST_OPT_DCK_DR                            = "MODD";
    public static final String SUGCOD_SCD_OPT_DCK_DR                            = "SODD";
    public static final String SUGCOD_AVAIL_SHP_DCK_DR                          = "ASD";
    public static final String SUGCOD_MST_OPT_SHP_DCK_DR                        = "MOSDD";
    public static final String SUGCOD_NXT_OPT_SHP_DCK_DR                        = "NOSDD";

    // Trailer Status
    public static final String TRLSTS_CHECKED_IN                                = "CI";
    public static final String TRLSTS_CLOSED                                    = "C";
    public static final String TRLSTS_COMPLETE                                  = "COMP";
    public static final String TRLSTS_OPEN                                      = "O";
    public static final String TRLSTS_RECEIVING                                 = "R";
    public static final String TRLSTS_SUSPENDED                                 = "SUSP";
    
    //Master Receipt Status
    public static final String TRKSTS_CLOSED                                    = "C";
    public static final String TRKSTS_CHECKED_IN                                = "CI";
    public static final String TRKSTS_EXPECTED                                  = "EX";
    public static final String TRKSTS_RECEIVING                                 = "R";
    public static final String TRKSTS_SUSPENDED                                 = "SUSP";

    //Purchase Order Status
    public static final String RIMSTS_CLOSED                                    = "CLSD";
    public static final String RIMSTS_SUSPENDED                                 = "SUSP";
    // Uom Codes
    public static final String UOMCOD_CASE                                      = "CS";
    public static final String UOMCOD_EACH                                      = "EA";
    public static final String UOMCOD_PALLET                                    = "PA";
    
    // Work Order Statuses
    public static final String WKOSTS_PENDING                                   = "P";
    public static final String WKOSTS_IN_PROCESS                                = "I";

    // Work Types
    public static final String WRKTYP_MANUAL                                    = "M";
    public static final String WRKTYP_STAGE_TRANSFER                            = "S";
    public static final String WRKTYP_EMERGENCY                                 = "E";
    public static final String WRKTYP_TOPOFF                                    = "T";
    public static final String WRKTYP_TRIGGERED                                 = "G";
    public static final String WRKTYP_DEMAND                                    = "D";
    public static final String WRKTYP_REPLENISH                                 = "R";
    public static final String WRKTYP_REPLENISHMENT                             = "R";
    public static final String RPL_WRKTYP                                       = "MRTGED";
    

    // Work Statuses
    public static final String WRKSTS_ACK_CMP                                   = "ACK";
    public static final String WRKSTS_LOCKED                                    = "LOCK";
    public static final String WRKSTS_PENDING                                   = "PEND";
    public static final String WRKSTS_SUSPENDED                                 = "SUSP";
    public static final String WRKSTS_WAITING_ACK                               = "WAIT";
    public static final String WRKSTS_WAITING_FOR_RPL                           = "WRPL";
    public static final String MOCA_STD_DATE_FORMAT                             = "YYYYMMDDHH24MISS";
    public static final String POLVAL_ALLOCINV_LOC_ORDER                        = "LOCATION-ORDER-BY";
    public static final String POLVAL_INVINLOC_ORDER_BY                         = "INVENTORY-IN-LOCATION";
    public static final String WKO_SUPNUM_PROD                                  = "PROD";
    public static final String WKO_INVSLN_DEFAULT                               = "0000000000";

    // Serial Number Levels
    public static final String SER_LVL_LOAD                                     = "L";
    public static final String SER_LVL_SUBLOAD                                  = "S";
    public static final String SER_LVL_DETAIL                                   = "D";

    //Date Format Strings
    public static final String MOCA_FORMAT_YEAR                                 = "Y";
    public static final String MOCA_FORMAT_DAY                                  = "D";
    public static final String MOCA_FORMAT_MIN                                  = "MI";
    public static final String MOCA_FORMAT_HR                                   = "HH24";
    public static final String MOCA_FORMAT_SEC                                  = "S";
    public static final String MOCA_FORMAT_AM_PM                                = "t";
    public static final String MOCA_FORMAT_DAY_WEEK                             = "ddd";
    public static final String MOCA_FORMAT_FULL_DAY                             = "dddd";
    public static final String JAVA_MOCA_DATE_FORMAT                            = "yyyyMMddHHmmss";
    public static final String JAVA_FMT_YEAR                                    = "y";
    public static final String JAVA_FMT_DAY                                     = "d";
    public static final String JAVA_FMT_MIN                                     = "mm";
    public static final String JAVA_FMT_HR                                      = "HH";
    public static final String JAVA_FMT_SEC                                     = "s";
    public static final String JAVA_FMT_AM_PM                                   = "a";
    public static final String JAVA_FMT_DAY_OF_WEEK                             = "E";
    public static final String JAVA_FMT_FULL_DAY                                = "EEEE";
    public static final String JAVA_FMT_MINUTE                                  = "m";
    public static final String JAVA_FMT_TWELVE_HOUR                             = "h";
    public static final String JAVA_FMT_HOUR                                    = "H";
    public static final String JAVA_FMT_MONTH                                   = "M";
    public static final String JAVA_FMT_WEEK                                    = "w";
    public static final String JAVA_FMT_FULL_MONTH                              = "MMM";
    
    // Item Date Codes
    public static final String DATE_CODE_BOTH                                   = "B";   
    public static final String DATE_CODE_EXPIRATION                             = "E";
    public static final String DATE_CODE_MANUFACTURE                            = "M";
    
    // Operators
    public static final String OPR_MAX_COUNT                                    = "MAX COUNT";
    public static final String OPR_BETWEEN                                      = "BETWEEN";
    public static final String OPR_IN                                           = "IN";
    public static final String OPR_NOT_IN                                       = "NOT IN";
    public static final String OPR_LIKE                                         = "LIKE";
    public static final String OPR_NOT_LIKE                                     = "NOT LIKE";
    public static final String OPR_EQUAL                                        = "=";
    public static final String OPR_NOT_EQUAL                                    = "!=";
    public static final String OPR_GREATER_THAN                                 = ">";
    public static final String OPR_LESSER_THAN                                  = "<";
    public static final String OPR_GREATER_EQUAL                                = ">=";
    public static final String OPR_LESSER_EQUAL                                 = "<=";
    public static final String OPR_IS_NULL                                      = "IS NULL";
    public static final String OPR_IS_NOT_NULL                                  = "IS NOT NULL";
    
    //Field Names
    public static final String FLD_LOTNUM                                       = "LOTNUM";
    public static final String FLD_SUP_LOTNUM                                   = "SUP_LOTNUM";
    public static final String FLD_ORIGIN_CODE                                  = "ORGCOD";
    public static final String FLD_REVISION_LEVEL                               = "REVLVL";
    public static final String FLD_MANUFACTURING_DATE                           = "MANDTE";
    public static final String FLD_EXPIRE_DATE                                  = "EXPIRE_DTE";
    public static final String FLD_INV_ATTR_STR1                                = "INV_ATTR_STR1";
    public static final String FLD_INV_ATTR_STR2                                = "INV_ATTR_STR2";
    public static final String FLD_INV_ATTR_STR3                                = "INV_ATTR_STR3";
    public static final String FLD_INV_ATTR_STR4                                = "INV_ATTR_STR4";
    public static final String FLD_INV_ATTR_STR5                                = "INV_ATTR_STR5";
    public static final String FLD_INV_ATTR_STR6                                = "INV_ATTR_STR6";
    public static final String FLD_INV_ATTR_STR7                                = "INV_ATTR_STR7";
    public static final String FLD_INV_ATTR_STR8                                = "INV_ATTR_STR8";
    public static final String FLD_INV_ATTR_STR9                                = "INV_ATTR_STR9";
    public static final String FLD_INV_ATTR_STR10                               = "INV_ATTR_STR10";
    public static final String FLD_INV_ATTR_STR11                               = "INV_ATTR_STR11";
    public static final String FLD_INV_ATTR_STR12                               = "INV_ATTR_STR12";
    public static final String FLD_INV_ATTR_STR13                               = "INV_ATTR_STR13";
    public static final String FLD_INV_ATTR_STR14                               = "INV_ATTR_STR14";
    public static final String FLD_INV_ATTR_STR15                               = "INV_ATTR_STR15";
    public static final String FLD_INV_ATTR_STR16                               = "INV_ATTR_STR16";
    public static final String FLD_INV_ATTR_STR17                               = "INV_ATTR_STR17";
    public static final String FLD_INV_ATTR_STR18                               = "INV_ATTR_STR18";
    public static final String FLD_INV_ATTR_INT1                                = "INV_ATTR_INT1";
    public static final String FLD_INV_ATTR_INT2                                = "INV_ATTR_INT2";
    public static final String FLD_INV_ATTR_INT3                                = "INV_ATTR_INT3";
    public static final String FLD_INV_ATTR_INT4                                = "INV_ATTR_INT4";
    public static final String FLD_INV_ATTR_INT5                                = "INV_ATTR_INT5";
    public static final String FLD_INV_ATTR_FLT1                                = "INV_ATTR_FLT1";
    public static final String FLD_INV_ATTR_FLT2                                = "INV_ATTR_FLT2";
    public static final String FLD_INV_ATTR_FLT3                                = "INV_ATTR_FLT3";
    public static final String FLD_INV_ATTR_DTE1                                = "INV_ATTR_DTE1";
    public static final String FLD_INV_ATTR_DTE2                                = "INV_ATTR_DTE2";
    public static final String FLD_RTTN_ID                                      = "RTTN_ID";
    public static final String FLD_CSTMS_BOND_FLG                               = "CSTMS_BOND_FLG";
    public static final String FLD_DTY_STMP_FLG                                 = "DTY_STMP_FLG";

    public static final String CARRIER_TYPE_SMALL_PACKAGE                       = "S";

    //Group Operators
    public static final String GRP_OPR_OR                                       = "OR";
    public static final String GRP_OPR_AND                                      = "AND";

    // PICK Types
    public static final String PICK_TYPE_REPLEN                                 = "REPLEN";
    //Service Action Codes
    public static final String SERV_ACTION_COD_PRNTRPT                          = "PRNTRPT";
    public static final String SERV_ACTION_COD_RUNCMD                           = "RUNCMD";
    public static final String SERV_ACTION_COD_EMSEVT                           = "EMSEVT";
    public static final String SERV_NON_RCV                                     = "NON-RCV";

    // Count Adjustment Methods
    public static final String ADJ_MTHD_STD                                     = "M";
    public static final String ADJ_MTHD_LCK_DWN                                 = "A";
    public static final String ACTCOD_LEN                                       = "CODVAL_LEN";
    public static final int ADJ_MTHD_LEN                                        = 3;
    public static final int ADJ_REF_LEN                                         = 20;
    public static final int ADR_ID_LEN                                          = 20;
    public static final int AGE_PFLNAM_LEN                                      = 30;
    public static final int AISLE_ID_LEN                                        = 20;
    public static final int ALT_IDN_LEN                                         = 30;
    public static final int ARECOD_LEN                                          = 10;
    public static final int ASSET_ID_LEN                                        = 30;
    public static final int ASSET_NUM_LEN                                       = 30;
    public static final int ASSET_STAT_LEN                                      = 15;
    public static final int ASSET_TAG_LEN                                       = 40;
    public static final int ASSET_TYP_LEN                                       = 30;

    public static final int BARCODE_LEN                                         = 150;
    public static final int BLDG_ID_LEN                                         = 10;
    public static final int BTOAUD_ID_LEN                                       = 12;
    public static final int BTOAUD_TYP_LEN                                      = 20;
    public static final String BTOAUD_DTL_STS_LEN                               = "CODVAL_LEN";
    public static final int BTO_DLV_SEQ_LEN                                     = 30;
    public static final int BTO_SEQNUM_LEN                                      = 20;

    public static final int CAR_MOVE_ID_LEN                                     = 10;
    public static final int CARCOD_LEN                                          = 10;
    public static final int CARTYP_LEN                                          = 10;
    public static final int CATCH_COD_LEN                                       = 1;
    public static final String CATCH_UNTTYP_LEN                                 = "MU_LEN";
    public static final int CFGCOD_LEN                                          = 6;
    public static final int CLIENT_ID_LEN                                       = 32;
    public static final int CMDNOT_LEN                                          = 255;
    
    public static final int CMPKEY_LEN                                          = 20;
    public static final int CNFRM_SERV_ID_LEN                                   = 20;
    public static final int CNFRM_SERV_VAL_LEN                                  = 30;
    public static final int CSTMS_CNSGNMNT_ID_LEN                               = 18;
    public static final int CNT_ID_LEN                                          = 32;
    public static final int CNTBAT_LEN                                          = 10;
    public static final int CNTGRP_LEN                                          = 20;
    public static final int CNTSTS_LEN                                          = 1;
    public static final int CNTTYP_LEN                                          = 3;
    public static final int CNTZON_ID_LEN                                       = 14;
    public static final String CNFRM_ASSET_TAG_LEN                              = "ASSET_TAG_LEN";
    public static final int CODVAL_LEN                                          = 40;
    public static final int COLNAM_LEN                                          = 100;
    public static final int COLVAL_LEN                                          = 200;
    public static final int CSTMS_RCPT_TYP_LEN                                  = 2;
    public static final int CSTNUM_LEN                                          = 20;
    public static final int CTNCOD_LEN                                          = 4;

    public static final int DEVCOD_LEN                                          = 20;
    public static final int DISTRO_ID_LEN                                       = 35;
    public static final int DOC_NUM_LEN                                         = 20;
    public static final int DRIVER_NAM_LEN                                      = 40;
    public static final int DRIVER_LIC_NUM_LEN                                  = 40;
    public static final int DTLNUM_LEN                                          = 30;
    public static final int DTY_STMP_FLG_LEN                                    = 1;

    public static final int END_LOC_CNT_LEN                                     = 3;
    public static final int EXEC_NAM_LEN                                        = 100;
    public static final int EXEC_PARM_LEN                                       = 2000;
    public static final int EXITPNT_LEN                                         = 15;
    public static final int EXITPNT_TYP_LEN                                     = 30;

    public static final int FLAG_LEN                                            = 1;
    public static final int FRM_ID_LEN                                          = 40;
    public static final int FTPCOD_LEN                                          = 30;

    public static final int GENCOD_LEN                                          = 3;

    public static final String IB_ISSUE_LEN                                     = "CODVAL_LEN";
    public static final int INVMOV_TYP_LEN                                      = 4;
    public static final int INVNUM_LEN                                          = 35;
    public static final int INVSTS_LEN                                          = 4;
    public static final int INVSTS_PRG_LEN                                      = 4;
    public static final int INVTID_LEN                                          = 30;
    public static final int INVTYP_LEN                                          = 4;

    public static final int LBLBAT_LEN                                          = 6;
    public static final int LIST_ID_LEN                                         = 15;
    public static final int LIST_ID_LIST_LEN                                    = 2000;
    public static final int LIST_STS_LEN                                        = 4;
    public static final int LIST_TYP_LEN                                        = 3;
    public static final int LNGDSC_LEN                                          = 250;
    public static final int LOCSTS_LEN                                          = 1;
    public static final int LODLVL_LEN                                          = 1;
    public static final String LOGIN_ID_LEN                                     = "USR_ID_LEN";
    public static final int LOT_FMT_ID_LEN                                      = 10;
    public static final int LOTNUM_LEN                                          = 25;
    
    public static final int LODCOUNTER_LEN                                      = 2;

    public static final int MAN_ID_LEN                                          = 20;
    public static final int MLS_TEXT_LEN                                        = 2000;
    public static final int MODEL_LEN                                           = 20;
    public static final int MNU_GRP_LEN                                         = 20;
    public static final int MSGTXT_LEN                                          = 80;
    public static final int MU_LEN                                              = 30;

    public static final String NAV_BAR_ID_LEN                                   = "MNU_GRP_LEN";
    public static final int NON_INVTYP_LEN                                      = 6;
    public static final int NUMCOD_LEN                                          = 30;

    public static final int OPRCOD_LEN                                          = 9;
    public static final int OPRTYP_LEN                                          = 9;
    public static final int OPT_NAM_LEN                                         = 50;
    public static final int OPT_TYP_LEN                                         = 1;
    public static final int ORDLIN_LEN                                          = 10;
    public static final int ORDNUM_LEN                                          = 35;
    public static final int ORGCOD_LEN                                          = 25;
    public static final int ORIENTATION_LEN                                     = 1;

    public static final int PALLET_ORE_LEN                                      = 1;
    public static final int PALLET_POS_LEN                                      = 8;
    public static final int PALLET_ID_LEN                                       = 20;
    public static final int PALPOS_LEN                                          = 20;
    public static final int PAL_SLOT_LEN                                        = 20;
    public static final int PCFLIN_LEN                                          = 4;
    public static final int PCFTYP_LEN                                          = 1;
    public static final int PCKCOD_LEN                                          = 1;
    public static final int PCKLST_LEN                                          = 1;
    public static final int PCKSTS_LEN                                          = 1;
    public static final int PKGTYP_LEN                                          = 8;
    public static final int POLCOD_LEN                                          = 50;
    public static final int POLVAR_LEN                                          = 50;
    public static final int POLVAL_LEN                                          = 50;
    public static final String PRCLOC_LEN                                       = "STOLOC_LEN";
    public static final int PRDLIN_LEN                                          = 20;
    public static final int PRTFAM_LEN                                          = 10;
    public static final int PRTNUM_LEN                                          = 50;
    public static final int PRTTYP_LEN                                          = 10;

    public static final int REACOD_LEN                                          = 20;
    public static final int READER_ID_LEN                                       = 20;
    public static final int RCVKEY_LEN                                          = 15;
    public static final int REVLVL_LEN                                          = 25;
    public static final int RF_FRM_ID_LEN                                       = 60;
    public static final int RF_MSG_LEN                                          = 200;
    public static final int RMANUM_LEN                                          = 30;
    public static final int ROLE_ID_LEN                                         = 20;
    public static final int RTSTR1_LEN                                          = 150;
    public static final int RTTN_ID_LEN                                         = 10;

    public static final int SEQNUM_LEN                                          = 2;
    public static final int SERADJ_ID_LEN                                       = 10;
    public static final String SER_LVL_LEN                                      = "LODLVL_LEN";
    public static final int SER_NUM_LEN                                         = 40;
    public static final int SER_MSK_LEN                                         = 30;
    public static final int SER_NUM_TYP_ID_LEN                                  = 10;
    public static final int SER_TYP_LEN                                         = 20;
    public static final int SERV_ACTION_COD_LEN                                 = 10;
    public static final int SERV_ACTION_TYP_LEN                                 = 10;
    public static final int SERV_ID_LEN                                         = 20;
    public static final int SERV_INS_ID_LEN                                     = 20;
    public static final int SERV_INS_TYP_LEN                                    = 8;
    public static final int SERV_RESULT_LEN                                     = 10;
    public static final int SHIP_ID_LEN                                         = 30;
    public static final int SHIP_LINE_ID_LEN                                    = 10;
    public static final int SHORT_DSC_LEN                                       = 20;
    public static final int SLOT_LEN                                            = 10;
    public static final int SERV_TYP_LEN                                        = 10;
    public static final int SESNUM_LEN                                          = 10;
    public static final int SLOT_CODE_LEN                                       = 32;
    public static final int STOLOC_LEN                                          = 20;
    public static final int STOP_NAM_LEN                                        = 10;
    public static final int STOP_ID_LEN                                         = 10;
    public static final int STOP_SEAL_LEN                                       = 30;
    public static final int SUBNUM_LEN                                          = 30;
    public static final int SUPNUM_LEN                                          = 32;
    public static final int TRACTOR_NUM_LEN                                     = 10;
    public static final int TRACK_NUM_LEN                                       = 20;
    public static final int TRAKNM_LEN                                          = 60;
    public static final String TRK_CNSG_COD_LEN                                 = "CODVAL_LEN";
    public static final int TRKNUM_LEN                                          = 20;
    public static final int TRLR_COD_LEN                                        = 4;
    public static final int TRLR_NUM_LEN                                        = 20;
    public static final int TRLR_STAT_LEN                                       = 8;
    public static final int TRLR_SEAL1_LEN                                      = 30;
    public static final int TRLR_SEAL2_LEN                                      = 30;
    public static final int TRLR_SEAL3_LEN                                      = 30;
    public static final int TRLR_SEAL4_LEN                                      = 30;
    public static final int UCCCOD_LEN                                          = 20;
    public static final int UDIA_STRING_LEN                                     = 100;
    public static final int USEFLG_LEN                                          = 1;
    public static final int USR_ID_LEN                                          = 40;
    public static final int USR_PSWD_LEN                                        = 80;
    public static final int USRSERV_LNGDSC_LEN                                  = 18;

    public static final int VAR_NAM_LEN                                         = 40;
    public static final int VARLKP_LEN                                          = 2000;
    public static final int VEHTYP_LEN                                          = 10;
    public static final int VELZON_LEN                                          = 1;

    public static final int WAYBIL_LEN                                          = 20;
    public static final int WH_ID_LEN                                           = 32;
    public static final int WKONUM_LEN                                          = 20;
    public static final int WKOREV_LEN                                          = 10;
    public static final int WRKARE_LEN                                          = 8;
    public static final int WRKREF_LEN                                          = 12;
    public static final int WRKSTS_LEN                                          = 4;
    public static final int WRKTYP_LEN                                          = 1;
    public static final int WRKZON_LEN                                          = 10;
    public static final int XDKREF_LEN                                          = 10;
    public static final int POSID_LEN                                           = 10;
    
    // ALIAS SECTION 
    // Alias other column names to the real column names here 
    public static final String CTNNUM_LEN                                       = "SUBNUM_LEN";
    public static final String DSTLOC_LEN                                       = "STOLOC_LEN";
    public static final int PCFKEY_LEN                                          = 30;
    public static final String SRCLOC_LEN                                       = "STOLOC_LEN";
    public static final String TRLR_ID_LEN                                      = "STOLOC_LEN";
    public static final String YARD_LOC_LEN                                     = "STOLOC_LEN";
    public static final int INPQTY_LEN                                          = 5;
    public static final String ADJ_REF1_LEN                                     = "ADJ_REF_LEN";
    public static final String ADJ_REF2_LEN                                     = "ADJ_REF_LEN";

    //  Inbound Staging
    public static final String ARECOD_INBOUND_STAGING                           = "ISTG"; 
    public static final String ASSET_STAT_ACT                                   = "ACT";

    public static final String BTOAUD_TYP_BTOMETAUD                             = "BTOMETAUD";
    public static final String BTODTLFAIL                                       = "BTODTLFAIL";
    public static final String BTODTLPASS                                       = "BTODTLPASS";
    public static final String CALENDAR_TYPE_C                                  = "C";

    public static final String CANCOD_NO_REALLOC                                = "CANCEL-NO-REALLOC";
    public static final String CARTYP_LTL                                       = "L";
    public static final String CATCH_COD_RECEIVING                              = "R";
    public static final String CATCH_COD_SHIPPING                               = "S";
    public static final String CNTTYP_MAN                                       = "MAN";
    public static final String CNTTYP_NEAR_ZERO                                 = "Z";
    public static final String CNTTYP_COUNT_BACK                                = "CB";

    public static final String CNTSTS_CANCELLED                                 = "N";
    public static final String CNTSTS_RELEASED                                  = "R";
    public static final String CNTSTS_COMPLETE                                  = "C";
    public static final String CNTSTS_INPROGRESS                                = "I";

    // Constants for customs item type
    public static final String CSTMS_ITM_TYP_CUSTOMS                            = "C";
    public static final String CSTMS_ITM_TYP_EXCISE                             = "E";

    // Customs Status
    public static final String CSTMS_CNSGN_STAT_DTYPROC                         = "D";
    public static final String CSTMS_CNSGN_STAT_CMPL                            = "C";

    public static final int CYCCNT_CNTTYP_BLNDSUM                               = 1;
    public static final int CYCCNT_CNTTYP_BLNDDET                               = 2;
    public static final int CYCCNT_CNTTYP_DETSUM                                = 3;

    public static final String CUR_FRM_TYP_INBOUND                              = "INBOUND";
    public static final String CUR_FRM_TYP_OUTBOUND                             = "OUTBOUND";
    public static final String CUR_FRM_TYP_TRANSFER                             = "TRANSFER";

    public static final String EXITPNT_PRE_CTN_PICK                             = "PRE-CTN-PICK";
    public static final String EXITPNT_DOCKDOOROPR                              = "DOCKDOOROPR";

    public static final String EXITPNT_TYP_SERVICE_INB                          = "SERVICE-INBOUND";
    public static final String EXITPNT_TYP_SERVICE_OUB                          = "SERVICE-OUTBOUND";
    public static final String EXITPNT_TYP_SERVICE_NOI                          = "SERVICE-NONINVENTORY";
    public static final String EXITPNT_TYP_SERVICE_USR                          = "SERVICE-USER";

    // Customs Receipt Types
    public static final String FROM_EU_STATES                                   = "EU";
    public static final String OTHER_UK_WAREHOUSE                               = "OW";
    public static final String FROM_IMPORTATION                                 = "IM";

    // Customs UCR/Originator/Originator Ref
    public static final String AAD_CONSIGNOR_EXCISE_NUMBER                      = "aadcon";
    public static final String AAD_REFERENCE_NUMBER                             = "aadref";
    public static final String W8_CONSIGNOR_EXCISE_NUMBER                       = "w8cent";
    public static final String W8_REFERENCE_NUMBER                              = "w8ref";
    public static final String C88_ENTRY_NUMBER                                 = "c88en";
    public static final String C88_PORT_OF_ENTRY                                = "c88pe";
    public static final String FROM_SFD_OR_CFSP_ENTRY                           = "sfd";
  
    public static final String INVSTS_ANY                                       = "ANY";
    public static final String INVSTS_AVAILABLE                                 = "A";

    public static final String INVMOV_TYP_RCV                                   = "RCV";
    public static final String INVMOV_TYP_RPLN                                  = "RPLN";
    public static final String INVMOV_TYP_TRNS                                  = "TRNS";
    public static final String INVMOV_TYP_PICK                                  = "PICK";

    public static final String LIST_STS_PICKING_IN_PROCESS                      = "I";
    public static final String LIST_STS_RELEASED                                = "R";
    public static final String LIST_STS_COMPLETED                               = "C";

    public static final String NON_INVTYP_TRLR                                  = "TRLR";
    public static final String NON_INVTYP_VHCL                                  = "VHCL";

    public static final String NUMCOD_LODNUM                                    = "lodnum";
    public static final String NUMCOD_SUBNUM                                    = "subnum";
    public static final String NUMCOD_DTLNUM                                    = "dtlnum";
    public static final String NUMCOD_RAMNUM                                    = "rmanum";
    public static final String NUMCOD_SESNUM                                    = "sesnum";
    public static final String NUMCOD_LIST_GRP_ID                               = "list_grp_id";
    public static final String NUMCOD_COUNT_BATCH                               = "cntbat";
    public static final String NUMCOD_COUNT_ID                                  = "cnt_id";

    public static final String OPRCOD_DSTTRNCMP                                 = "DSTTRNCMP";
    public static final String OPRTYP_DEKIT                                     = "DEKIT";
    public static final String OPRTYP_AUDIT                                     = "AUDIT";
    public static final String OPRTYP_ADJUST                                    = "ADJUST";

    public static final String PCFTYP_FIXED_KIT                                 = "F";
    public static final String PCKCOD_MIXABLE                                   = "M";
    public static final String POLVAL_NEVER_ATTACH                              = "NEVER-ATTACH";
    public static final String PRCMOD_NOMOVE                                    = "nomove";
    public static final String PRTNUM_KIT                                       = "KITPART";
    public static final String PRTNUM_RETURN                                    = "RETURNPART";

    public static final String SERV_INS_TYP_YESNO                               = "YESNO";
    public static final String SERV_INS_TYP_ACKNOWL                             = "ACKNOWL";
    public static final String SERV_INS_TYP_NOCONFRM                            = "NOCONFRM";
    public static final String SERV_INS_VAL_YES                                 = "optYes";
    public static final String SERV_INS_VAL_NO                                  = "optNo";
    public static final String SERV_INS_VAL_ACKNOWLEDGED                        = "optAcknowledged";

    public static final String SERV_RESULT_NONE                                 = "NONE";
    public static final String SERV_RESULT_PASS                                 = "PASS";
    public static final String SERV_RESULT_FAIL                                 = "FAIL";

    public static final String SERV_ACTION_TYP_PASS                             = "PASS";
    public static final String SERV_ACTION_TYP_FAIL                             = "FAIL";

    public static final String SERV_TYP_INBSERV                                 = "INBSERV";
    public static final String SERV_TYP_OUBSERV                                 = "OUBSERV";
    public static final String SERV_TYP_BCKSERV                                 = "BCKSERV";
    public static final String SERV_TYP_NONINVSERV                              = "NONINVSERV";

    public static final String SER_TYP_OUTCAP_ONLY                              = "OUTCAP_ONLY";

    public static final String STOLOC_PERM_ADJUST                               = "PERM-ADJ-LOC";
    public static final String STOLOC_PERM_COUNTS                               = "PERM-CNT-LOC";
    public static final String STOLOC_PERM_CREATE                               = "PERM-CRE-LOC";
    public static final String STOLOC_PERM_EXPRCP                               = "PERM-EXP-LOC";
    public static final String STOLOC_PERM_SHPADJ                               = "PERM-SHP-LOC";

    public static final String TRLSTS_PENDING_DISPATCH                          = "P";
    public static final String TRLSTS_DISPATCHED                                = "D";
    public static final String TRLSTS_EXPECTED                                  = "EX";
    public static final String TRLSTS_OPEN_FOR_RCV                              = "OR";
    public static final String TRLSTS_UNLOADING                                 = "UNLD";
    public static final String TRLSTS_LOADED                                    = "L";

    public static final String TRLR_COD_SHIP                                    = "SHIP";
    public static final String TRLR_COD_RCV                                     = "RCV";
    public static final String TRLR_COD_STORAGE                                 = "STO";

    public static final String WRKTYP_KIT                                       = "K";
    public static final String WRKTYP_PICK                                      = "P";
    public static final String WRKTYP_BULK_PICK                                 = "B";

    public static final String OVRCOD_PERCENTAGE                                = "PRCT";
    public static final String OVRCOD_QUANTITY                                  = "UQTY";
    public static final String OVRCOD_COST_THRESH                               = "CST_THRESH";

    // Options for message prompts
    // C - Continue, R - Cancel and Reset Status or A - Cancel  
    public static final String CONT_CANC_RESET                                  = "C|R|A";
    
    // L - Load, S - Subload, C - Cancel
    public static final String LOAD_SUBLOAD_CANCEL                              = "L|S|C";
    
    // Continue
    public static final String OPTION_CONTINUE                                  = "C";
    
    // Cancel and Reset
    public static final String OPTION_CANC_RESET                                = "R";
    
    // Cancel
    public static final String OPTION_CANC                                      = "A";
    
    // Load
    public static final String OPTION_LOAD                                      = "L";
    
    // Cancel
    public static final String OPTION_CANCEL                                    = "C";

    // Constants for Date Code
    public static final String DTE_CODE_BOTH                                    = "B";
    public static final String DTE_CODE_EXPIRE_DTE                              = "E";
    public static final String DTE_CODE_MANDTE                                  = "M";

    // Constants for order inventory
    public static final String ORDER_INV_FEFO_ORDER                             = "FEFO-ORDER-BY";
    public static final String ORDER_INV_LEFO_ORDER                             = "LEFO-ORDER-BY";

    // Constants for Item master option for user defined inventory attributes
    public static final String UDIA_TRACK_CODE_NONE                             = "N";
    public static final String UDIA_TRACK_CODE_TRACKED                          = "T";
    public static final String UDIA_TRACK_CODE_REQUIRED                         = "R";
    public static final String FLD_ATTR_STR1                                    = "ATTR_STR1";
    public static final String FLD_ATTR_STR2                                    = "ATTR_STR2";
    public static final String FLD_ATTR_STR3                                    = "ATTR_STR3";
    public static final String FLD_ATTR_STR4                                    = "ATTR_STR4";
    public static final String FLD_ATTR_STR5                                    = "ATTR_STR5";
    public static final String FLD_ATTR_STR6                                    = "ATTR_STR6";
    public static final String FLD_ATTR_STR7                                    = "ATTR_STR7";
    public static final String FLD_ATTR_STR8                                    = "ATTR_STR8";
    public static final String FLD_ATTR_STR9                                    = "ATTR_STR9";
    public static final String FLD_ATTR_STR10                                   = "ATTR_STR10";
    public static final String FLD_ATTR_STR11                                   = "ATTR_STR11";
    public static final String FLD_ATTR_STR12                                   = "ATTR_STR12";
    public static final String FLD_ATTR_STR13                                   = "ATTR_STR13";
    public static final String FLD_ATTR_STR14                                   = "ATTR_STR14";
    public static final String FLD_ATTR_STR15                                   = "ATTR_STR15";
    public static final String FLD_ATTR_STR16                                   = "ATTR_STR16";
    public static final String FLD_ATTR_STR17                                   = "ATTR_STR17";
    public static final String FLD_ATTR_STR18                                   = "ATTR_STR18";
    public static final String FLD_ATTR_INT1                                    = "ATTR_INT1";
    public static final String FLD_ATTR_INT2                                    = "ATTR_INT2";
    public static final String FLD_ATTR_INT3                                    = "ATTR_INT3";
    public static final String FLD_ATTR_INT4                                    = "ATTR_INT4";
    public static final String FLD_ATTR_INT5                                    = "ATTR_INT5";
    public static final String FLD_ATTR_FLT1                                    = "ATTR_FLT1";
    public static final String FLD_ATTR_FLT2                                    = "ATTR_FLT2";
    public static final String FLD_ATTR_FLT3                                    = "ATTR_FLT3";
    public static final String FLD_ATTR_DTE1                                    = "ATTR_DTE1";
    public static final String FLD_ATTR_DTE2                                    = "ATTR_DTE2";

    public static final String ACTCOD_CASE_SPLIT                                = "CSSPLT";
    public static final String ACTCOD_IDENTIFICATION                            = "IDNTFY";
    public static final String ACTCOD_IDENTIFICATION_ADJ                        = "IDNTFY_AJ";
    public static final String ACTCOD_DISTRO_EXCEPTION                          = "DISTEXCP";
    public static final String ACTCOD_FLUID_LOAD                                = "FLDLD";
    public static final String ACTCOD_ASN_TRUSTED                               = "ASN_TRUST";
    public static final String ACTCOD_ASN_NON_TRUSTED                           = "ASN_NONTRUST";
    public static final String ACTCOD_SORTED_DEPOSIT                            = "DEP_SORT";
    public static final String ACTCOD_ASN_REMOVE                                = "ASN_REMOVE";
    public static final String ACTCOD_VEL_RECALC                                = "VELRECALC";
    public static final String ACTCOD_CRSDCK                                    = "CRSDCK";
    public static final String ACTCOD_MSTR_RCP_MOVE                             = "MSTR_RCP_MOVE";
    public static final String ACTCOD_IDNTFY_UNXP                               = "IDNTFY_UNXP";
    public static final String ACTCOD_TRLR_LOAD                                 = "TRLR_LOAD";
    public static final String ACTCOD_PUTSTOCMP                                 = "PUTSTOCMP";
    public static final String ACTCOD_INV_DEL                                   = "INVDEL";

    // RF activities
    public static final String RF_ACTCOD_DIRECTED_PUTAWAY                       = "PUT_DRCT";
    public static final String RF_ACTCOD_SORTED_PUTAWAY                         = "PUT_SORT";
    public static final String RF_ACTCOD_UNDIRECTED_PUTAWAY                     = "PUT_UNDR";
    public static final String RF_ACTCOD_CASE_TRANSFER                          = "CS_XFR";
    public static final String RF_ACTCOD_SERIAL_TRANSFER                        = "SER_XFR";
    public static final String RF_ACTCOD_ASN_TRUSTED                            = "ASN_TRUST";
    public static final String RF_ACTCOD_ASN_NON_TRUSTED                        = "ASN_NONTRUST";
    public static final String RF_ACTCOD_NON_ASN_RCV                            = "RCV";
    public static final String RF_ACTCOD_FULL_INV_MOVE                          = "FL_XFR";
    public static final String RF_ACTCOD_PART_INV_MOVE                          = "PL_XFR";
    public static final String RF_ACTCOD_FULL_INV_MOVE_PTWY                     = "FL_XFR_PTWY";
    public static final String RF_ACTCOD_CRSDCK                                 = "CRSDCK";
    public static final String RF_ACTCOD_OVRREP                                 = "OVRREP";
    public static final String RF_ACTCOD_PALLET_PICK                            = "PALPCK";
    public static final String RF_ACTCOD_CASE_PICK                              = "CASPCK";
    public static final String RF_ACTCOD_PIECE_PICK                             = "PCEPCK";
    public static final String RF_ACTCOD_OTHER_PICK                             = "OTRPCK";
    public static final String RF_ACTCOD_PALLET_REPLENISHMENT                   = "PALRPL";
    public static final String RF_ACTCOD_CASE_REPLENISHMENT                     = "CASRPL";
    public static final String RF_ACTCOD_OTHER_REPLENISHMENT                    = "OTHRPL";
    public static final String RF_ACTCOD_KIT_PICK                               = "KITPCK";
    public static final String RF_ACTCOD_LST_PICK                               = "LSTPCK";
    public static final String RF_ACTCOD_GENERIC_MOVE                           = "GENMOV";
    public static final String RF_ACTCOD_UNPICK                                 = "UPCK";
    public static final String RF_ACTCOD_MSTR_RCP_UNLD                          = "MSTR_RCP_UNLD";
    public static final String RF_ACTCOD_MSTR_RCP_MOVE                          = "MSTR_RCP_MOVE";
    public static final String RF_ACTCOD_BULK_PICK                              = "BPCK";
    
    // RF tools menu structure definitions 
    public static final String RF_TOOLS_MNU_NAV_BAR_ID                          = "MTF_TOOLS";
    public static final String RF_UNPICK_OPTION                                 = "mtfoptUnpick";
    public static final String RF_PICKING_MENU                                  = "mtfmnuPicking";
 
    // LMS (Labor Management Systems) code definitions
    public static final String RF_LMSCOD_DIRECT                                 = "D";
    public static final String RF_LMSCOD_INDIRECT                               = "I";
    public static final String RF_LMSTYP_TRAVEL                                 = "T";
    public static final String RF_LMSTYP_DISCRETE                               = "A";
    public static final String RF_LMSTYP_PLACE                                  = "P";
    public static final String RF_LMSTYP_OBTAIN                                 = "O";
    public static final String RF_LMS_ISTARTUP                                  = "ISTARTUP";

    // RF standard function key definitions 
    public static final IVirtualKey VK_FKEY_NEXT                                = CVirtualKey.VK_F2;
    public static final String FKEY_NEXT_CAPTION                                = "lblFkeyNext";
    public static final IVirtualKey VK_FKEY_LIST                                = CVirtualKey.VK_F2;
    public static final String FKEY_LIST_CAPTION                                = "lblFkeyList";
    public static final IVirtualKey VK_FKEY_AVAILABLE_SLOT                      = CVirtualKey.VK_F2;
    public static final String FKEY_AVAILABLE_SLOT_CAPTION                      = "lblFkeyAvailableSlot";
    public static final IVirtualKey VK_FKEY_BROWSE                              = CVirtualKey.VK_F3;
    public static final String FKEY_BROWSE_CAPTION                              = "lblFkeyBrowse";
    public static final IVirtualKey VK_FKEY_ALOC                                = CVirtualKey.VK_F3;
    public static final String FKEY_ALOC_CAPTION                                = "lblFkeyAloc";
    public static final IVirtualKey VK_FKEY_OVER                                = CVirtualKey.VK_F3;
    public static final String FKEY_OVER_CAPTION                                = "lblFkeyOver";
    public static final IVirtualKey VK_FKEY_ADD                                 = CVirtualKey.VK_F3;
    public static final String FKEY_ADD_CAPTION                                 = "lblFkeyAdd";
    public static final IVirtualKey VK_FKEY_OVERPOS                             = CVirtualKey.VK_F3;
    public static final String FKEY_OVERPOS_CAPTION                             = "lblFkeyOverPos";
    public static final IVirtualKey VK_FKEY_BARCOD                              = CVirtualKey.VK_F3;
    public static final String FKEY_BAR_COD_CAPTION                             = "lblFkeyBarcode";
    public static final IVirtualKey VK_FKEY_PREVIOUS                            = CVirtualKey.VK_F3;
    public static final String FKEY_PREVIOUS_CAPTION                            = "lblFkeyPrevious";
    public static final IVirtualKey VK_FKEY_DEL                                 = CVirtualKey.VK_F4;
    public static final String FKEY_DEL_CAPTION                                 = "lblFkeyDel";
    public static final IVirtualKey VK_FKEY_OVERRIDE                            = CVirtualKey.VK_F4;
    public static final String FKEY_OVERRIDE_CAPTION                            = "lblFkeyOverrideLoc";
    public static final IVirtualKey VK_FKEY_OVRRDE_PAL_POS                      = CVirtualKey.VK_F8;
    public static final String FKEY_OVRRDE_PAL_POS_CAPTION                      = "lblFkeyOvrrdePalPos";
    public static final IVirtualKey VK_FKEY_SKIP                                = CVirtualKey.VK_F4;
    public static final String FKEY_SKIP_CAPTION                                = "lblFkeySkip";
    public static final IVirtualKey VK_FKEY_PRTCFG_SKIP                         = CVirtualKey.VK_GOLD_F3;
    public static final String FKEY_PRTCFG_SKIP_CAPTION                         = "lblFkeySkip";
    public static final IVirtualKey VK_FKEY_DKIT                                = CVirtualKey.VK_F4;
    public static final String FKEY_DKIT_CAPTION                                = "lblFkeyDkit";
    public static final IVirtualKey VK_FKEY_SPLIT                               = CVirtualKey.VK_F4;
    public static final String FKEY_SPLIT_CAPTION                               = "lblFkeySplit";
    public static final IVirtualKey VK_FKEY_CHNGQUAL                            = CVirtualKey.VK_F4;
    public static final String FKEY_CHNGQUAL_CAPTION                            = "lblFkeyChngQual";
    public static final IVirtualKey VK_FKEY_ENA_TO_ID                           = CVirtualKey.VK_F5;
    public static final String FKEY_ENA_TO_ID_CAPTION                           = "lblFkeyEnaToId";
    public static final IVirtualKey VK_FKEY_MOV_BACK                            = CVirtualKey.VK_F3;
    public static final String FKEY_MOV_BACK_CAPTION                            = "lblFkeyMovBack";
    public static final IVirtualKey VK_FKEY_PRTFTP_DSP                          = CVirtualKey.VK_F8;
    public static final String FKEY_PRTFTP_DSP_CAPTION                          = "lblFkeyDsp";
    public static final IVirtualKey VK_FKEY_DONE                                = CVirtualKey.VK_F6;
    public static final String FKEY_DONE_CAPTION                                = "lblFkeyDone";
    public static final IVirtualKey VK_FKEY_DEPOSIT                             = CVirtualKey.VK_F6;
    public static final IVirtualKey VK_FKEY_UPDATE_TRLR_INFO                    = CVirtualKey.VK_F6;
    public static final String FKEY_UPDATE_TRLR_INFO_CAPTION                    = "lblFkeyUpdateTrlrInfo";
    public static final IVirtualKey VK_FKEY_ITMDTL                              = CVirtualKey.VK_GOLD_F2;
    public static final IVirtualKey VK_FKEY_COMPLETEWORK                        = CVirtualKey.VK_GOLD_F6;
    public static final String FKEY_ITEM_DETAIL                                 = "lblFkeyItmDtl";
    public static final String FKEY_COMPLETEWORK_CAPTION                        = "lblFkeyCompleteWork";
    public static final String FKEY_DEPOSIT_CAPTION                             = "lblFkeyDeposit";
    public static final IVirtualKey VK_FKEY_FORM_VARS                           = CVirtualKey.VK_F6;
    public static final String FKEY_FORM_VARS_CAPTION                           = "lblFkeyFormVars";
    public static final IVirtualKey VK_FKEY_CRTN_CMPL                           = CVirtualKey.VK_F8;
    public static final String FKEY_CRTN_CMPL_CAPTION                           = "lblFkeyCrtnCmpl";
    public static final IVirtualKey VK_FKEY_DEP_MODE                            = CVirtualKey.VK_F8;
    public static final String FKEY_DEP_MODE_CAPTION                            = "lblFkeyDepMode";
    public static final IVirtualKey VK_FKEY_GOTO_PAGE                           = CVirtualKey.VK_F8;
    public static final String FKEY_GOTO_PAGE_CAPTION                           = "lblFkeyGotoPage";
    public static final IVirtualKey VK_FKEY_CNG_INVSTS                          = CVirtualKey.VK_F7;
    public static final String FKEY_CNG_INVSTS_CAPTION                          = "lblFkeyCngInvsts";
    public static final IVirtualKey VK_FKEY_GOTO                                = CVirtualKey.VK_F7;
    public static final String FKEY_GOTO_CAPTION                                = "lblFkeyGoto";
    public static final IVirtualKey VK_FKEY_SHOW_NOTES                          = CVirtualKey.VK_GOLD_F1;
    public static final String FKEY_SHOW_NOTES_CAPTION                          = "lblFkeyShowNotes";
    public static final IVirtualKey VK_FKEY_SUM                                 = CVirtualKey.VK_F9;
    public static final String FKEY_SUM_CAPTION                                 = "lblFkeySum";
    public static final IVirtualKey VK_FKEY_ASTIDENTIFY                         = CVirtualKey.VK_F8;
    public static final String FKEY_ASTIDENTIFY_CAPTION                         = "lblFkeyAssetIdentify";
    public static final IVirtualKey VK_FKEY_RVS_RCPT                            = CVirtualKey.VK_F9;
    public static final String FKEY_RVS_RCPT_CAPTION                            = "lblFkeyRvsRcpt";
    public static final IVirtualKey VK_FKEY_NGTV_ADJ                            = CVirtualKey.VK_F9;
    public static final String FKEY_NGTV_ADJ_CAPTION                            = "lblFkeyNgtvAdj";
    public static final IVirtualKey VK_FKEY_SHOW_ATTR                           = CVirtualKey.VK_F4;
    public static final String FKEY_SHOW_ATTR_CAPTION                           = "lblFkeyShowAttr";
    public static final IVirtualKey VK_FKEY_SKP_LOC                             = CVirtualKey.VK_F5;
    public static final String FKEY_SKP_LOC_CAPTION                             = "lblFkeySkpLoc";
    public static final IVirtualKey VK_FKEY_UNSCANINV                           = CVirtualKey.VK_F4;
    public static final String FKEY_UNSCANINV_CAPTION                           = "lblFkeyUnscannedInv";
    public static final IVirtualKey VK_FKEY_IDENTIFY                            = CVirtualKey.VK_F5;
    public static final IVirtualKey VK_FKEY_BARCODE                             = CVirtualKey.VK_F7;
    public static final String FKEY_BAR_CODE_CAPTION                            = "lblFkeyBarcode";
    public static final IVirtualKey VK_FKEY_HOLD                                = CVirtualKey.VK_GOLD_F4;
    public static final String FKEY_HOLD_CAPTION                                = "lblFkeyHold";
    public static final IVirtualKey VK_CARTON_EXCEPTION                         = CVirtualKey.VK_F8;
    public static final String FKEY_CARTON_EXCEPTION_CAPTION                    = "lblCtnExp";
    public static final IVirtualKey VK_FKEY_CLEAR                               = CVirtualKey.VK_F4;
    public static final String FKEY_CLEAR_CAPTION                               = "lblFkeyClear";
    public static final IVirtualKey VK_FKEY_PUTAWAY                             = CVirtualKey.VK_F9;
    public static final String FKEY_PUTAWAY_CAPTION                             = "lblFkeyPutaway";
    public static final IVirtualKey VK_FKEY_INBQLT                              = CVirtualKey.VK_GOLD_F1;
    public static final String FKEY_INBQLT_CAPTION                              = "lblFkeyInbQlt";
    public static final IVirtualKey VK_FKEY_OVRD_VEH_LOD_LIM                    = CVirtualKey.VK_F9;
    public static final String FKEY_OVRD_VEH_LOD_LIM_CAPTION                    = "lblFkeyOvrdVehLodLim";
    public static final IVirtualKey VK_FKEY_OVRD_SLT                            = CVirtualKey.VK_F6;
    public static final String FKEY_OVRD_SLT                                    = "lblFkeySltOvrd";
    public static final IVirtualKey VK_FKEY_OVRD_VEH_LOD_LIM_UNLOAD             = CVirtualKey.VK_GOLD_F9;
    public static final String FKEY_OVRD_VEH_LOD_LIM_UNLOAD_CAPTION             = "lblFkeyOvrdVehLodLim";
    public static final IVirtualKey VK_FKEY_CREATEWORK                          = CVirtualKey.VK_F6;
    public static final String FKEY_CREATEWORK_CAPTION                          = "lblFkeyCreateWork";
    public static final IVirtualKey VK_FKEY_DTL_DISPLAY                         = CVirtualKey.VK_F4;
    public static final String FKEY_DTL_DISPLAY_CAPTION                         = "lblFkeyDtlDsp";
    public static final IVirtualKey VK_FKEY_OVRDLODCFG                          = CVirtualKey.VK_F5;
    public static final String FKEY_OVRD_ASSET_CAPTION                          = "lblFkeyOvrdLodCfg";
    public static final IVirtualKey VK_FKEY_CLOSE_RCV_TRLR                      = CVirtualKey.VK_F4;
    public static final String FKEY_CLOSE_RCV_TRLR_CAPTION                      = "lblFkeyCloseRcvTrlr";
    public static final IVirtualKey VK_FKEY_RESET_LOC                           = CVirtualKey.VK_F9;
    public static final String FKEY_RESET_LOC_CAPTION                           = "lblFkeyResetLoc";
    public static final IVirtualKey VK_FKEY_SET_LOC_CAPACITY                    = CVirtualKey.VK_F8;
    public static final String FKEY_SET_LOC_CAPACITY_CAPTION                    = "lblFkeySetLocCapacity";
    public static final IVirtualKey VK_FKEY_CALCULATE_CAPACITY                  = CVirtualKey.VK_F4;
    public static final String FKEY_CALCULATE_CAPACITY_CAPTION                  = "lblFkeyCalculateCapacity";
    public static final IVirtualKey VK_FKEY_LOCATION_DISPLAY                    = CVirtualKey.VK_F3;
    public static final String FKEY_LOCATION_DISPLAY_CAPTION                    = "lblFkeyLocDisplay";
    public static final IVirtualKey VK_FKEY_MASTER_LIST_INFO                    = CVirtualKey.VK_F4;
    public static final String FKEY_MASTER_LIST_INFO_CAPTION                    = "lblFkeyMasterListInfo";
    public static final IVirtualKey VK_FKEY_STARTWKO                            = CVirtualKey.VK_F8;
    public static final String FKEY_STARTWKO_CAPTION                            = "lblFkeyStartWko";
    public static final IVirtualKey VK_FKEY_STOPWKO                             = CVirtualKey.VK_F4;
    public static final String FKEY_STOPWKO_CAPTION                             = "lblFkeyStopWko";
    
    public static final IVirtualKey VK_FKEY_PERFORM_REPLENISHMENT               = CVirtualKey.VK_F6;
    public static final String FKEY_PERFORM_REPLENISHMENT_CAPTION               = "lblFkeyPerformReplenishment";
    public static final IVirtualKey VK_FKEY_GENERATE_REPLENISHMENT              = CVirtualKey.VK_F8;
    public static final String FKEY_GENERATE_REPLENISHMENT_CAPTION              = "lblFkeyGenerateReplenishment";
    public static final IVirtualKey VK_FKEY_REFRESH_INFO                        = CVirtualKey.VK_F4;
    public static final String FKEY_REFRESH_INFO_CAPTION                        = "lblFkeyRefreshInfo";

    public static final IVirtualKey VK_FKEY_DISCREPANCY                         = CVirtualKey.VK_F4;
    public static final String FKEY_DISCREPANCY_CAPTION                         = "lblFkeyDiscrepancy";
    public static final IVirtualKey VK_FKEY_RECEIPTDSP                          = CVirtualKey.VK_GOLD_F5;
    public static final String FKEY_RECEIPTDSP_CAPTION                          = "lblFkeyReceiptDsp";
    public static final IVirtualKey VK_FKEY_REFRESH_WORK                        = CVirtualKey.VK_F6;
    public static final String FKEY_REFRESH_WORK_CAPTION                        = "lblFkeyRefreshWork";
    public static final IVirtualKey VK_FKEY_REQ_NEXT_LIST_ID                    = CVirtualKey.VK_GOLD_F8;
    public static final String FKEY_REQ_NEXT_LIST_ID                            = "lblReqNextListId";
    public static final IVirtualKey VK_FKEY_SKIP_LIST_ID                        = CVirtualKey.VK_F5;
    public static final String FKEY_SKIP_LIST_ID                                = "lblFkeySkip";
    public static final IVirtualKey VK_FKEY_INV_DSP                             = CVirtualKey.VK_F5;
    public static final String FKEY_INV_DSP                                     = "lblInvDsp";
    public static final IVirtualKey VK_FKEY_TOOLS_MENU                          = CVirtualKey.VK_F7;
    public static final String FKEY_TOOLS_MENU                                  = "tools";
    public static final IVirtualKey VK_FKEY_UOM                                 = CVirtualKey.VK_F9;
    public static final String FKEY_UOM_CAPTION                                 = "lblFkeyQuantityCapture";

    // RF standard field height & length definitions 
    public static final int RF_UNTQTY_LEN_FOR_ENTER                             = 19;
    public static final int RF_UNTQTY_LEN                                       = 8;
    public static final int RF_UOMQTY_LEN                                       = 7;

    // RF standard field display width definitions 
    public static final int RF_FTPCOD_DSP_WIDTH                                 = 20;
    public static final int RF_PRTNUM_DSP_WIDTH                                 = 20;
    public static final int RF_PICKUP_DSPFLD_WIDTH                              = 25;
    public static final int RF_DEPOSIT_DSPFLD_WIDTH                             = 25;

    // RF Mode for Asset Identify 
    public static final String MODE_IDN_EMPTYASSET                              = "EA";
    public static final String MODE_IDN_NONEMPTYASSET                           = "IA";
    public static final String MODE_IDN_EXISTING_EMPTYASSET                     = "AA";

    // RF field text definitions
    public static final String RF_INS_RECEIVE                                   = "RCV";

    public static final String RF_OPR_UNDIR_CASE_TRANSFER                       = "UCT";
    public static final String RF_OPR_UNDIR_IDENTIFY                            = "UID";
    public static final String RF_OPR_UNDIR_RECEIVE                             = "URC";
    public static final String RF_OPR_UNDIR_TRANSFER                            = "UTR";
    public static final String RF_OPR_PALLET_BUILD                              = "UPB";
    public static final String RF_OPR_UNDIR_PICK                                = "UPK";
    public static final String RF_OPR_UNDIR_SERIAL_TRANSFER                     = "UST";
    public static final String RF_PRTNUM_KIT                                    = "KITPART";
    public static final String RF_TRLR_COD_SHIP                                 = "SHIP";
    public static final String RF_TRLR_COD_STORAGE                              = "STO";

    public static final String RF_TXT_CODE4_UNUSED                              = "----";
    public static final String RF_TXT_UOM_UNUSED                                = "--";

    public static final int RF_STS_EXISTS                                       = 512;

    // RF Password Expired and Warning Definitions
    public static final int RF_PASSWORD_EXPIRED                                 = 1014;
    public static final int RF_PASSWORD_WARNING                                 = 1099;

    // Skip Pick Prevalidation Information Level
    public static final String RF_SKP_COD_NOPROMPT                              = "NO-PROMPT";
    public static final String RF_SKP_COD_PROMPT                                = "PROMPT";
    public static final String RF_SKP_COD_NOTIFY                                = "NOTIFY";
    public static final String RF_SKP_COD_DISABLE                               = "DISABLE";
    
    // Skip Pick Prevalidation Action Code Level
    public static final String RF_ACTION_COD_SKIP                               = "S";
    public static final String RF_ACTION_COD_CANCEL                             = "C";

    // MOCA Cache Names
    // Receiving Policies:
    public static final String MOCACACHE_RCVPOL = "MOCA-CACHE-RCVPOL";
    
    // Message Text 
    public static final String MSG_ADJ_NOT_CMP                                  = "stsAdjustmentIncomplete";
    public static final String MSG_ADJ_COMPLETED                                = "stsAdjustmentCompleted";
    public static final String MSG_ADDED_TO_BATCH                               = "stsAddedToBatch";
    public static final String MSG_TOT_COUNT                                    = "stsTotalCount";
    public static final String MSG_AUDIT_COMPLETED                              = "stsAuditCompleted";
    public static final String MSG_AUD_NOT_CMP                                  = "stsAuditIncomplete";
    public static final String MSG_BATCH_CMP                                    = "stsBatchComplete";
    public static final String MSG_CALCULATE_LOC_CAPACITY                       = "stsCalculateLocationCapacity";
    public static final String MSG_CAPACITY_HAS_BEEN_UPDATED                    = "stsLocationCapacityHasBeenUpdated";
    public static final String MSG_CANNOT_COMP_LIST_PICK                        = "errCannotCompleteListPick";
    public static final String MSG_CANNOT_PERF_LIST_PICK                        = "errCannotPerformListPick";
    public static final String MSG_CANNOT_SCAN_KIT_PICK                         = "errCannotScanKitPick";
    public static final String MSG_CARTON_HAS_BEEN_COMPLETED                    = "stsCartonHasBeenCompleted";
    public static final String MSG_MAXQVL_CHANGED                               = "stsMaximumCapacityChanged";
    public static final String MSG_CMP_WRKREQ                                   = "errCompleteWrkreq";
    public static final String MSG_CNT_ERR                                      = "errGeneratingCycleCount";
    public static final String MSG_CNTBCK_HLD                                   = "stsCountBack";
    public static final String MSG_DISCREPANCY_FOUND                            = "stsDiscrepancyFound";
    public static final String MSG_DSTLOC_UNABLE_GET_NEXT                       = "errDstlocUnableGetNext";
    public static final String MSG_ENTER_ALT_OR_F6                              = "errEnterAltorF6";
    public static final String MSG_ERR_DEALLOC                                  = "errDeallocateLoad";
    public static final String MSG_ERR_DEPOSIT                                  = "errErrorDeposit";
    public static final String MSG_ERR_ERROR                                    = "errMarkLocationInError";
    public static final String MSG_ERR_LODLVL                                   = "errGetLoadInfo";
    public static final String MSG_EXE_FRM                                      = "errExeFormEntry";
    public static final String MSG_INCMPL_USR_SERV_CMPL                         = "errIncmplUsrServCmpl";
    public static final String MSG_INCMPL_CNTWRK                                = "errIncmplCntwrk";
    public static final String MSG_INCMPL_COUNT                                 = "errIncmplCount";
    public static final String MSG_INV_ADJQTY                                   = "errInvalidAdjQty";
    public static final String MSG_INV_CARTON                                   = "errInvalidCarton";
    public static final String MSG_INV_DEST                                     = "errInvalidDestination";
    public static final String MSG_INV_DSTLDTOLD                                = "errInvDestLoadToNewLoad";
    public static final String MSG_INV_FIELD                                    = "errFieldRequired";
    public static final String MSG_INV_LOC                                      = "errInvalidLocnum";
    public static final String MSG_INV_LOC_FOR_PAR_PAL                          = "errInvalidLocForParPal";
    public static final String MSG_INV_LODNUM                                   = "errInvalidLodnum";
    public static final String MSG_INV_LOT                                      = "errInvalidLotnum";
    public static final String MSG_INV_SHP_HLD_LODNUM                           = "errInvShpHldLodNum";
    public static final String MSG_INV_ORG                                      = "errInvalidOrgcod";
    public static final String MSG_INV_PART                                     = "stsInvalidPrtnum";
    public static final String MSG_INV_QTY                                      = "errInvalidQty";
    public static final String MSG_INV_REV                                      = "errInvalidRevlvl";
    public static final String MSG_INV_STATION                                  = "errInvalidStation";
    public static final String MSG_INV_STOLOC                                   = "stsInvalidStoloc";
    public static final String MSG_INV_SUBLOAD                                  = "errInvalidSubload";
    public static final String MSG_INV_WORK                                     = "stsInvalidWrkref";
    public static final String MSG_INV_INVTID                                   = "errInvalidInvtid";
    public static final String MSG_INV_STATUS                                   = "errInvalidInvsts";
    public static final String MSG_INV_SUP                                      = "errInvalidSupnum";
    public static final String MSG_INV_SUPLOT                                   = "errInvalidSuplotnum";
    public static final String MSG_INV_TRLREF                                   = "errInvalidTrlrRef";
    public static final String MSG_INV_TRLNUM                                   = "errInvalidTrlr_num";
    public static final String MSG_INV_TRKNUM                                   = "errInvalidTrknum";
    public static final String MSG_INV_EXITPNT                                  = "errInvalidExitpnt";
    public static final String MSG_INV_WH                                       = "errInvalidWarehouse";
    public static final String MSG_INV_WH_SERV                                  = "errInvalidWarehouseServ";
    public static final String MSG_INVID_PRTNUM_EMPTY                           = "errInvidPrtnumEmpty";
    public static final String MSG_LIST_PICK_COMPLETED                          = "stsListPickCompleted";
    public static final String MSG_LOAD_INFO                                    = "errGetLoadInfo";
    public static final String MSG_LOADS_ARE_PART_OF_DIFF_HOPS                  = "errLoadsArePartOfDiffHops";
    public static final String MSG_LOAD_NOT_PREPARED                            = "errLoadNotCompleted";
    public static final String MSG_LOC_HAS_PLTCTL_INV                           = "errLocHasPltctlInv";
    public static final String MSG_LOC_OR_PRT_RQD                               = "errLocationOrPartRequired";
    public static final String MSG_LOD_CONT_BULK_PCK_INV                        = "stsLodContBulkPickedInv";
    public static final String MSG_MAX_ROWS_INV_DISP                            = "stsMaxRowsInvDisp";
    public static final String MSG_MOVE_TRLR                                    = "dlgOkToMoveTrailer";
    public static final String MSG_MULT_PICK                                    = "stsMultiplePick";
    public static final String MSG_RF_NEVER_COMPLETE_STOP                       = "dlgRFNeverCompleteStop";
    public static final String MSG_NO_LIST_FOR_WRKLSTID                         = "errNoListForWorkListID";
    public static final String MSG_NO_MORE_INV_FOUND                            = "errNoMoreInvFound";
    public static final String MSG_NOT_AVAIL                                    = "errOnlyAvailableFromDeposit";
    public static final String MSG_NOT_CHGD                                     = "stsStatusNotChanged";
    public static final String MSG_NOT_INBSERV_OR_OUBSERV                       = "errNotInbservOrOubserv";
    public static final String MSG_NOT_PROC                                     = "errCannotOverrideProcessingLoc";
    public static final String MSG_NOT_OVERRIDE                                 = "errCannotOverrideSameLoc";
    public static final String MSG_NOT_XDK                                      = "errCannotOverrideCrossdockLoc";
    public static final String MSG_NOT_HOP                                      = "errCannotOverrideHopLoc";
    public static final String MSG_NOT_DISTRO                                   = "errCannotOverrideDistroInv";
    public static final String MSG_NO_RECS                                      = "stsNoRecsMatch";
    public static final String MSG_MULT_TRLRS                                   = "errMultTrlrsInSrcloc";
    public static final String MSG_PAL_PICK_FOR_LIST_IN_TEMP_LOC                = "errPalPickedForListFoundInTempLoc";
    public static final String MSG_PLACE_INV                                    = "errPlaceInventory";
    public static final String MSG_PROCEED                                      = "dlgOkToLogLMSWrk";
    public static final String MSG_PUTAWAY                                      = "stsPutaway";
    public static final String MSG_RESERVE_SPACE                                = "errReserveSpace";
    public static final String MSG_RESET_LOC                                    = "stsLocationReset";
    public static final String MSG_SET_LOC_FULL                                 = "stsLocationFull";
    public static final String MSG_SRCLOC_REQ                                   = "errSrclocRequired";
    public static final String MSG_SHIPMENT_SPLIT_DONE                          = "dlgShipmentSplitDone";
    public static final String MSG_TRAILER_IS_BUSY                              = "errTrlrBusy";
    public static final String MSG_TRLREF_REQ                                   = "stsEnterTrlrRef";
    public static final String MSG_EXP_WARN                                     = "dlgExpirationWarning";
    public static final String MSG_EXPED_WARN                                   = "dlgExpiredWarning";
    public static final String MSG_UNEXPECTED_REMQTY                            = "stsUnexpectedRemQty";
    public static final String MSG_WRKREF_PICKED                                = "errWrkrefAlreadyPicked";
    public static final String MSG_SKIP_PICK                                    = "dlgNotifySkipPick";
    public static final String MSG_CANCEL_PICK                                  = "dlgNotifyCancelPick";
    public static final String MSG_STARTER_PALLET                               = "stsStarterPallet";
    public static final String MSG_PRINTER                                      = "stsPrintedToPrinter";
    public static final String MSG_INV_MANDTE                                   = "errInvalidManufactureDte";
    public static final String MSG_INV_EXPIREDTE                                = "errInvalidExpirationDte";
    public static final String MSG_LODNUM_NOT_MATCH_RULE                        = "errLodnumSatisfyRuleAttr";
    public static final String MSG_LOC_NOT_REC_STG                              = "errLocNotRecStg";
    public static final String MSG_INV_RTTNID                                   = "errInvalidRotationID";
    public static final String MSG_VEH_LIM_MET                                  = "stsVehLimit";
    public static final String MSG_SCAN_LOD                                     = "stsScanLoad"; 
    public static final String MSG_ERR_BAD_RCV_RESCOD                           = "errBadRcvRescod"; 
    public static final String MSG_COUNT_IN_PROGRESS                            = "errCountInProgressByAnotherUser";
    public static final String MSG_LOCSTS_FULL                                  = "stsLocstsFull";
    public static final String MSG_LOAD_PICK_EXIST_AGAINST_LOAD                 = "errLoadPickExistAgainstLoad";
    public static final String MSG_WKO_STARTED_SUCCESS                          = "stsWkoStartedSuccessfully";
    public static final String MSG_WKO_STOPPED_SUCCESS                          = "stsWkoStoppedSuccessfully";
    public static final String MSG_ALL_WKO_STARTED                              = "stsAllWkoHaveBeenStarted";
    public static final String MSG_ALL_WKO_STOPPED                              = "stsAllWkoHaveBeenStopped";
    public static final String MSG_LOCATION_CHANGED                             = "stsLocationChanged";
    public static final String MSG_CHECK_ASSET_TYPE                             = "dlgCheckAssetType";
    
    // Status Text 
    public static final String STS_CNT_REQ                                      = "stsCycleCountRequired";
    public static final String STS_CARTON_COMPLETED                             = "stsCartonCompleted";
    public static final String STS_ID_UNPICKED                                  = "stsIdentifierUnpicked";
    public static final String STS_ITEM_UPDATED                                 = "stsItemUpdated";
    public static final String STS_LOAD_RELABELED                               = "stsLoadRelabeled";
    public static final String STS_MANUAL_REPL_CMPL                             = "stsManualReplenishment";
    public static final String STS_NO_AVAIL_RPL_PCKWRK                          = "stsNoAvailRplPckWrk";
    public static final String STS_NO_UPD_MADE_TRLR                             = "stsNoUpdMadeTrlr";
    public static final String STS_PALBLD_ORDER_COMPLETE                        = "stsPalBldOrderComplete";
    public static final String STS_PALBLD_SHIPMENT_COMPLETE                     = "stsPalBldShipmentComplete";
    public static final String STS_PART_UPDATED                                 = "stsPartUpdated";
    public static final String STS_REC_MODIFIED                                 = "stsRecModified";
    public static final String STS_SEND_MSG                                     = "stsSendMsg";
    public static final String STS_FAILED_MSG                                   = "stsSendMsgFailed";
    public static final String STS_QUALITY_ISSUE_LOGGED                         = "stsQualityIssueLogged";
    public static final String STS_RPL_GENERATED                                = "stsRplPckWrkGenerated";
    public static final String STS_RPL_ALREADY_GENERATED                        = "stsRplPckWrkAlreadyGenerated";
    public static final String STS_RPL_REL_NOT_ASSIGNED                         = "stsNotAssigned";
    public static final String STS_SUBLOAD_COMPLETE                             = "stsSubloadComplete";
    public static final String STS_STS_CHGD                                     = "stsStatusChanged";
    public static final String STS_SNT_LMS_TRANS                                = "stsLmsIndirectSent";
    public static final String STS_TOPOFF_REP                                   = "stsTopOffReplenishment";
    public static final String STS_FAILED_REPLEN                                = "stsReplenishmentFailed";
    public static final String STS_SERV_ACKNOWL_TIP                             = "stsServAcknowlTip";
    public static final String STS_TRK_OPEN                                     = "stsTrkOpen";
    public static final String STS_TRK_NOT_CLOSED                               = "stsTrkNotClosed";
    public static final String STS_TRLR_OPEN                                    = "stsTrlrOpen";
    public static final String STS_TRLR_NOT_CLOSED                              = "stsTrlrNotClosed";
    public static final String STS_TRLR_CLOSED                                  = "stsTrlrClosed";
    public static final String STS_TRK_CLOSED                                   = "stsTrkClosed";
    public static final String STS_PALBLD_COMPLETE_BY_CNSL_RULE                 = "stsPalBldCompleteByConsolRuleConfig";

    // Dialog Text 
    public static final String DLG_PICKING_INCOMPLETE_FOR_LOAD                  = "dlgPickingIncompleteForLoad";
    public static final String DLG_ADUIT_INCOMPLETE                             = "dlgAuditIncomplete";
    public static final String DLG_CREATE_MANUALREPLEN                          = "dlgOkToCreateManualReplenishment";
    public static final String DLG_OK_CAPTURE_SLOT                              = "dlgOkCaptureSlot";
    public static final String DLG_CATCH_QTY_TOL                                = "dlgCatchQtyToleranceNotMet";
    public static final String DLG_CHECK_IN_TRLR                                = "dlgTrailerNotCheckedIn";
    public static final String DLG_CNFRM_SERV_VAL                               = "dlgOkToCnfrmServVal";
    public static final String DLG_CNFRM_SEND_TO_ALL                            = "dlgCnfrmSendToAll";
    public static final String DLG_CNFRM_SEND                                   = "dlgCnfrmSend";
    public static final String DLG_CNS_EXCEEDS_DLV                              = "dlgCnsExceedsDlv";
    public static final String DLG_CREATE_TOPOFFREPLEN                          = "dlgOkToCreateTopoffReplenishment";
    public static final String DLG_DEPOSIT_LOAD                                 = "dlgOkToDepositLoad";
    public static final String DLG_DEP_DEF_VEH_FULL_OPT                         = "dlgOkToDepDefVehFullOpt";
    public static final String DLG_INVSTS_NOT_INLINE_WITH_EXPDTE                = "dlgInvstsNotInlineWithExpireDteRDT";
    public static final String DLG_INVSTS_NOT_INLINE_WITH_MANDTE                = "dlgInvstsNotInlineWithMandteRDT";
    public static final String DLG_INVSTS_OUTSIDE_AGEPFL                        = "dlgInvstsOutsideAgepflRDT";
    public static final String DLG_LOCATION_EMPTY                               = "dlgLocationEmpty";
    public static final String DLG_LOSE_LOADS                                   = "dlgWillLoseRemainingLoads";
    public static final String DLG_LOSE_SCANNED_PROD                            = "dlgWillLoseScannedProduct";
    public static final String DLG_MAX_CNS_QTY_TOL                              = "dlgMaxCnsQtyTol";
    public static final String DLG_MIN_CNS_QTY_TOL                              = "dlgMinCnsQtyTol";
    public static final String DLG_MAX_PRD_QTY_TOL                              = "dlgMaxPrdQtyTol";
    public static final String DLG_MORE_TO_RCV                                  = "dlgMoreToRcv";
    public static final String DLG_NEW_INVTID_LVL                               = "dlgNewInvtidLvl";
    public static final String DLG_NO_EXIST_OK_TO_ADD                           = "dlgInvNoExistOkToAdd";
    public static final String DLG_OK_PCK_PREV_LOAD                             = "dlgOkToPickPreviousLoad";
    public static final String DLG_OK_SPL_EST_LOAD                              = "dlgOkToSplitExistedLoad";
    public static final String DLG_OK_PCK_UP                                    = "dlgOkPickup";
    public static final String DLG_OK_PUT_FRM_STG                               = "dlgOkToPutawayFromStaging";
    public static final String DLG_OK_SKIP_PICK                                 = "dlgPromptSkipPick";    
    public static final String DLG_OK_CANCEL_PICK                               = "dlgPromptCancelPick";    
    public static final String DLG_OK_TO_CREATE_ASSET                           = "dlgOKToCreateAsset";
    public static final String DLG_OK_TO_CANCEL_CAPTURE                         = "dlgOkToCancelCapture";
    public static final String DLG_OK_TO_CNFRM_SERV                             = "dlgOkToCnfrmServ";
    public static final String DLG_OK_TO_CNFRM_SERV_INS                         = "dlgOkToCnfrmServIns";
    public static final String DLG_OK_TO_CHANGE_STATUS                          = "dlgOkToChangeStatus";
    public static final String DLG_OK_TO_CHANGE_AST_TYP                         = "dlgOkToChangeAstTyp";
    public static final String DLG_OK_TO_CHANGE_SLOT                            = "dlgOkToChangeSlot";
    public static final String DLG_OK_TO_CLOSE_DISC_RCV_TRLR                    = "dlgOkToCloseDiscRcvTrlr";
    public static final String DLG_OK_TO_CLOSE_DISC_RCV_TRK                     = "dlgOkToCloseDiscRcvTrk";
    public static final String DLG_OK_TO_CLOSE_MST_RCP                          = "dlgOkToCloseMstRcp";
    public static final String DLG_OK_TO_CLOSE_RCV_TRLR                         = "dlgOkToCloseRcvTrlr";
    public static final String DLG_OK_TO_CMPL_ADJUST                            = "dlgOkToCompleteAdjustment";
    public static final String DLG_OK_TO_CMPL_AUDIT                             = "dlgOkToCompleteAudit";
    public static final String DLG_OK_TO_CMPL_CYCNT                             = "dlgOKToCompleteCycleCount";
    public static final String DLG_OK_TO_CREATE                                 = "dlgOkToCreateInventory";
    public static final String DLG_OK_TO_CREATE_CARTON                          = "dlgOkToCreateCarton";
    public static final String DLG_OK_TO_CREATE_LOAD                            = "dlgOkToCreateLoad";
    public static final String DLG_OK_TO_DELETE                                 = "dlgOkToDeleteInventory";
    public static final String DLG_OK_TO_CREATE_INVSERV                         = "dlgOkToCreateInvServ";
    public static final String DLG_OK_TO_CREATE_WORK                            = "dlgOkToCreateWork";
    public static final String DLG_OK_TO_CONTINUE_IDENTIFY                      = "dlgOkToContinueIdentify";
    public static final String DLG_OK_TO_CONTINUE_RECEIVING                     = "dlgOkToContinueReceiving";
    public static final String DLG_OK_TO_DELETE_INVSERV                         = "dlgOkToDeleteInvServ";
    public static final String DLG_OK_TO_DEPOSIT_KIT                            = "dlgOkToDepositKit";
    public static final String DLG_OK_TO_COMPLETE_KIT                           = "dlgOKToCompleteKit";
    public static final String DLG_OK_TO_DISPATCH_TRLR                          = "dlgOkToDispatchTrlr";
    public static final String DLG_OK_TO_ERROR_LOC                              = "dlgOkToErrorLocation";
    public static final String DLG_OK_TO_EXIT_IN_INLE_REPLEN                    = "dlgOkToExitInLineReplen";
    public static final String DLG_OK_TO_OVERRIDE                               = "dlgOkToOverride";
    public static final String DLG_OK_TO_OPEN_CARTON                            = "dlgOkToOpenCarton";
    public static final String DLG_OK_TO_OPEN_LOAD                              = "dlgOkToOpenLoad";
    public static final String DLG_OK_TO_PERFORM_REPL                           = "dlgOkToPerformRepl";
    public static final String DLG_OK_TO_PLAY_ADJUST                            = "dlgOkToPlayAdjustment";
    public static final String DLG_OK_TO_REOPEN_TRK                             = "dlgOkToReopenTrk";
    public static final String DLG_OK_TO_REOPEN_TRLR                            = "dlgOkToReopenTrlr";
    public static final String DLG_OK_TO_SKIP_CREATE                            = "dlgOkToSkipCreate";
    public static final String DLG_OK_TO_SKIP_DELETE                            = "dlgOkToSkipDelete";
    public static final String DLG_OK_TO_SKIP_UPDATE                            = "dlgOkToSkipUpdate";
    public static final String DLG_OK_TO_SPLIT_SHIPMENT                         = "dlgOkToSplitShipment";
    public static final String DLG_OK_TO_SPLIT_TRAILER                          = "dlgOkToSplitShipmentCmplStop";
    public static final String DLG_OK_TO_UNPICK                                 = "dlgOkToUnpick";
    public static final String DLG_OK_TO_UNPICK_PARTIAL                         = "dlgOkToUnpickPartial";
    public static final String DLG_OK_TO_UPDATE                                 = "dlgOkToUpdate";
    public static final String DLG_OVR_RCV_WRN                                  = "dlgOvrRcvWrn";
    public static final String DLG_PICKS_EXIST                                  = "dlgPicksExist";
    public static final String DLG_PRT_NOT_INV                                  = "dlgPartNotOnInvoice";
    public static final String DLG_SUBNUM_EXISTS_OK_DELETE                      = "dlgSubnumExistsOkDelete";
    public static final String DLG_LOAD_FBFRONT_AFTER                           = "dlgLoadFBFRONTAfter";
    public static final String DLG_SEQ_MISSING_SEQ                              = "dlgSeqMissingToContinue";
    public static final String DLG_OK_OVRP_RPL                                  = "dlgOkToOverReplenishment";
    public static final String DLG_OK_OVRP_RPL_CAS                              = "dlgOkToOverReplenishmentCase";
    public static final String DLG_OK_TO_AUDIT                                  = "dlgOkToContinueAudit";
    public static final String DLG_STRANDED_LOC                                 = "dlgStrandedLoc";
    public static final String DLG_OK_TO_OVRD_VEH_LOD_LIM                       = "dlgOkToOvrdVehLodLim";
    public static final String DLG_OK_TO_MOVE_LOST_INVENTORY                    = "dlgOkToMoveLostInv";
    public static final String DLG_TRLR_NUM_NOT_UNIQUE                          = "dlgTrlrNumNotUnique";
    public static final String DLG_OK_TO_LOC_WITH_COMQTY                        = "dlgOkToCntComLoc";
    public static final String DLG_OK_TO_LOC_WITH_COMQTY_DIRMOD                 = "dlgOkToCntComLocDirMod";
    public static final String DLG_RCV_COMPLETE                                 = "dlgIsReceivingComplete";
    public static final String DLG_RPLWRK_CANT_RELEASE                          = "dlgRplwrkCantRelease";
    public static final String DLG_OK_COMPLETE_STOP                             = "dlgOkToCompleteStop";
    public static final String DLG_PASSWORD_EXPIRE_WARNING                      = "dlgPasswordExpireWarning";
    public static final String DLG_CHANGE_PASSWORD_NOW                          = "dlgChangePasswordNow";
    public static final String DLG_TURN_TRAILER_AROUND                          = "dlgTurnTrailerAround";
    public static final String DLG_MSTR_RCPT_MOVE                               = "dlgMstRcptMove";
    public static final String DLG_OK_TO_UNLOAD_MSTRCP                          = "dlgOkToUnloadMstRcp";
    public static final String DLG_OK_TO_DEPOSIT                                = "dlgOkToDeposit";
    public static final String DLG_DOES_INVENTORY_MATCH                         = "dlgDoesInventoryMatch";
    public static final String DLG_CONFIRM_LOC_FOR_LOST_INVENTORY               = "dlgCnfrmLocForLstInv";
    public static final String DLG_DOES_WAIT_FOR_REPL_FINISHED                  = "dlgOkToWaitForReplenish";
    public static final String DLG_PICKS_FOR_ZONE_COMPLETE                      = "dlgOkToConinuePicksforNextZone";
    public static final String DLG_COUNTS_COMPLETE_FOR_COUNT_BATCH              = "dlgOkToConinueCountsFromNextCntbat";
    public static final String DLG_MIXED_PART                                   = "MIXEDPART";
    public static final String DLG_NO_DESCRIPTION                               = "No Description";
    public static final String DLG_UNXP_PRT                                     = "dlgUnexpectedItem";
	public static final String DLG_OK_WITHOUT_IDF_ASST                          = "dlgOkWithoutIdfAsset";
    public static final String DLG_OK_USE_EXIST_TO_ID                           = "dlgOkToUseExistToId";
    public static final String DLG_ASN_ON_DIFF_MASTER_RECEIPT                   = "dlgAsnOnDifferentMasterReceipt";
    
    // Error Text 
    public static final String ERR_ADD_INV_EXISTS_NOT_READY_TO_LOAD             = "errAddInvExistsNotReadyToLoad";
    public static final String ERR_ADJ_NOT_ALLOWED                              = "errAdjNotAllowed";
    public static final String ERR_ALLOC_LOC                                    = "errAllocateLoc";
    public static final String ERR_ALL_STOPS_COMPLETE                           = "errAllStopsComplete";
    public static final String ERR_ATLEAST_ONE_FLD_REQ                          = "errAtleastOneFldReq";
    public static final String ERR_ASG_RFT                                      = "errAssignRFT";
    public static final String ERR_ASSET_ID_FOR_PICK                            = "errInvalidAssetIdForPick";
    public static final String ERR_ASSET_ID_EXISTS                              = "errAssetIdExists";
    public static final String ERR_AUDIDED_LODNUM                               = "errAuditedLod";
    public static final String ERR_AUDIDED_SLOT                                 = "errAuditedSlot";
    public static final String ERR_AUDIT_FAIL                                   = "errAuditFail";
    public static final String ERR_AUDIT_FAIL_NO_INV                            = "errAuditFailNoInv";
    public static final String ERR_BAD_LOGIN                                    = "errBadLogin";
    public static final String ERR_BTO_AUD_FAILED                               = "errBtoAudFailed";
    public static final String ERR_CANT_MOV_INV_OUT_OF_PALLET                   = "errCantMovInvOutofPal";
    public static final String ERR_CARCOD_NOT_LTL                               = "errCarcodNotLtl";
    public static final String ERR_CARCOD_RQD                                   = "errCarcodRqd";
    public static final String ERR_CARTON_EXISTS                                = "errCartonExists";
    public static final String ERR_CARTON_COMPLETE                              = "errCartonPickIsComplete";
    public static final String ERR_CARTON_IN_PROCESS                            = "errCartonInProcess";
    public static final String ERR_CARTON_INCOMPLETE                            = "errCartonIncomplete";
    public static final String ERR_CARTON_NOT_REL                               = "errCartonPickNotReleased";
    public static final String ERR_CARTON_NEXT_MOVE                             = "errCartonNextMove";
    public static final String ERR_CASE_ON_BUNDLED_LOAD                         = "errCaseOnBundledLoad";
    public static final String ERR_CASIDN_INV_LODLVL_PART                       = "errCasIdnInvalidLodlvlForPart";
    public static final String ERR_CASIDN_NOT_SUPT_PART_SERNUM                  = "errCasIdnNotSuptPartWithSernum";
    public static final String ERR_CATCH_QTY_RQD                                = "errCatchQtyRqd";
    public static final String ERR_CMPLTING_ADJUST                              = "errCompletingAdjustment";
    public static final String ERR_CMP_WRKREQ                                   = "errCompleteWrkreq";
    public static final String ERR_COMPLETED_CNT                                = "errCompletedCount";
    public static final String ERR_CNSG_ID_EXISTS                               = "errCnsgIdExists";
    public static final String ERR_COUNT_AUDIT_INPROC                           = "errCountAuditInprocess";
    public static final String ERR_DELETE_INV                                   = "errDeletingInventory";
    public static final String ERR_DEPLOC_NOT_PD_LOC                            = "errDepLocNotPDLoc";
    public static final String ERR_DEA_WRKREQ                                   = "errDeassignWrkreq";
    public static final String ERR_DEASSIGN_USER_FROM_CTN                       = "errDeassignUserFromCtn";
    public static final String ERR_DISCREPANCY_IN_STG                           = "errDiscrepancyInStagingLane";
    public static final String ERR_DROP_AT_WRKZON_CHANGE                        = "errDropAtWrkzonChange"; 
    public static final String ERR_WRKZON_CHANGE                                = "errWorkzoneChange";
    public static final String ERR_DSTID_SAME_SRCID                             = "errDstIdSameSrcId";
    public static final String ERR_DTLNUM_REQUIRED                              = "errDtlnumRequired";
    public static final String ERR_DOCK_OR_CAR_MOVE_RQD                         = "errDockOrCarMoveRqd";
    public static final String ERR_DOCK_OR_TRLR_RQD                             = "errDockOrTrlrRqd";
    public static final String ERR_DSTLOC_REQUIRED                              = "errDstlocRequired";
    public static final String ERR_DST_LOAD_NOT_IN_SHPLOC                       = "errDstLodNotInShpLoc";
    public static final String ERR_DST_LOC_NOT_IN_SHP_STG_AREA                  = "err10437";
    public static final String ERR_GEN_LODNUM                                   = "errGeneratingLoadNumber";
    public static final String ERR_GEN_SESNUM                                   = "errGenSesnum";
    public static final String ERR_GET_WRKQUE                                   = "errGetWorkQueueInfo";
    public static final String ERR_GEN_SUBNUM                                   = "errGeneratingSubNumber";
    public static final String ERR_ID_NOT_PICKED                                = "errIdNotPicked";
    public static final String ERR_INPUT_RQD                                    = "errInputRqd";
    public static final String ERR_INV_ADJ_INPROC                               = "err10708";
    public static final String ERR_INV_AGE_PROF                                 = "errInvalidAgeProfile";
    public static final String ERR_INV_AT_WRONG_LOC                             = "errInvAtWrongLoc";
    public static final String ERR_INV_BTO_SEQNUM                               = "errInvalidBtoSeqnum";
    public static final String ERR_INV_BTO_DLV_SEQ                              = "errInvalidBtoDlvSeq";
    public static final String ERR_INVALID_ASSET_ID                             = "errInvalidAssetID";
    public static final String ERR_INVALID_ASSET_TYP                            = "errInvalidAssetTyp";
    public static final String ERR_INV_ASSET_TYP_FOR_LIST                       = "errInvalidAssetTypeForList";
    public static final String ERR_INV_CARTON                                   = "errInvalidCarton";
    public static final String ERR_INV_CANCOD                                   = "errInvalidCancod";
    public static final String ERR_INV_CARTON_FOR_ASSET                         = "errInvalidCartonForAsset";
    public static final String ERR_INV_CARCOD                                   = "errInvalidCarcod";
    public static final String ERR_INV_CARMOVEID                                = "errInvalidCarMoveId";
    public static final String ERR_INV_CLIENT_ID                                = "errInvalidClientId";
    public static final String ERR_INV_CNTBAT                                   = "errInvaidCountBatch";
    public static final String ERR_INV_CNTRY                                    = "errInvalidCntryCod";
    public static final String ERR_INV_CNTZON                                   = "errInvalidCountZone";
    public static final String ERR_INV_CONFIG                                   = "errInvalidConfigCode";
    public static final String ERR_INV_CSTMS_CNSGNMNT_ID                        = "err10208";
    public static final String ERR_INV_DETAIL                                   = "errInvalidDetail";
    public static final String ERR_INV_DOCKDOOR                                 = "errInvalidDockDoor";
    public static final String ERR_INV_DOCK_DOOR_YARD_LOC                       = "err10490";
    public static final String ERR_INV_FTPCOD                                   = "errInvalidFtpcod";
    public static final String ERR_INV_IDENTIFIER_FOR_MANCNT                    = "errInvIdManCnt";
    public static final String ERR_INV_INVNUM                                   = "errInvalidInvnum";
    public static final String ERR_INV_LIST_STS                                 = "err11029";
    public static final String ERR_INV_INVSTS                                   = "errInvalidInvsts";
    public static final String ERR_INV_INVSTS_RVR_RCP                           = "errInvalidInvstsRvrRcp";
    public static final String ERR_INV_STOP_SEQ                                 = "errInvStopSeq";
    public static final String ERR_INVALID_TOID                                 = "errInvalidToId";
    public static final String ERR_INVALID_VOL_DATA                             = "errInvalidDataForVol";
    public static final String ERR_INV_NOT_IN_WH                                = "errInventoryNotInWarehouse";
    public static final String ERR_INV_PRTNUM                                   = "errInvalidPrtnum";
    public static final String ERR_INV_PRTNUM_BTO_DLV_SEQ                       = "errInvPrtnumForBtoDlvSeq";
    public static final String ERR_INV_PRTNUM_CLIENT                            = "errInvalidPrtnumClientid";
    public static final String ERR_INV_PRTTYP                                   = "errInvalidPartType";
    public static final String ERR_INV_RCPT_TYP                                 = "errInvalidRcptType";
    public static final String ERR_INV_RCVDCK                                   = "errInvalidRcvdck";
    public static final String ERR_INV_SER_NUM                                  = "errInvalidSernum";
    public static final String ERR_INV_SLOT                                     = "errInvalidSlot";
    public static final String ERR_INV_STOLOC                                   = "errInvalidStoloc";
    public static final String ERR_INV_DTLTRN_SER_NUM                           = "errInvalidDtlTrnSernum";
    public static final String ERR_INV_UNTQTY_BTO_DLV_SEQ                       = "errInvUntqtyForBtoDlvSeq";
    public static final String ERR_INV_UOMCOD                                   = "errInvalidUomcod";
    public static final String ERR_INV_ACTIVITY                                 = "errInvalidActivity";
    public static final String ERR_INV_ADJ_SUP_APP                              = "errInvAdjSupApp";
    public static final String ERR_INV_NOT_MEET_DATE                            = "err11327";
    public static final String ERR_INVALID_DIR_AUDIT_LOC                        = "errInvalidDirAuditLoc";
    public static final String ERR_INVALID_AUDIT_LOC                            = "errInvalidAuditLocation";
    public static final String ERR_INVALID_AUDIT_QTY                            = "errInvalidAuditQty";
    public static final String ERR_INVALID_USER_AUDIT_LOC                       = "errInvalidUserAuditatLocation";
    public static final String ERR_INVALID_AUDIT_LOD                            = "errInvalidAuditLoad";
    public static final String ERR_INVALID_FTPCOD                               = "errInvalidFtpcod";
    public static final String ERR_INVALID_INV_ID                               = "errInvalidIdentifier";
    public static final String ERR_INVALID_PALLET_ID                            = "errInvalidPalletId";
    public static final String ERR_INVALID_SRLDTL_QTY                           = "errInvalidSrlDtlQty";
    public static final String ERR_STATUS_CHANGE                                = "err12001";
    public static final String ERR_INV_TAKE_TO_SERV_LOC                         = "errInventoryTakeToServLoc";
    public static final String ERR_INV_EXISTS_ON_TRLR                           = "errInventoryExistsOnTrailer";
    public static final String ERR_LOAD_NOT_BUNDLED                             = "errLoadNotBundled";
    public static final String ERR_LOAD_NOT_IN_ADJLOC                           = "errLoadNotInAdjLoc";
    public static final String ERR_SUB_NOT_IN_ADJLOC                            = "errSubNotInAdjLoc";
    public static final String ERR_LOAD_NOT_MATCH                               = "errLoadNotMatch";
    public static final String ERR_LOADING_INV                                  = "errLoadingAdjustInvList";
    public static final String ERR_LOC_OR_TRLR_RQD                              = "errLocOrTrlrNumRequired";
    public static final String ERR_LOC_TRLR_OR_CARMOV_RQD                       = "errLocOrTrlrNumOrCarMovRequired";
    public static final String ERR_LOCATION_CONFIRMATION_FAILED                 = "errCnfrmLocFailedForRecovery";
    public static final String ERR_LOD_EXISTS                                   = "errLodnumExists";
    public static final String ERR_SUB_LOD_EXISTS                               = "err10728";
    public static final String ERR_DTL_LOD_EXISTS                               = "err10729";
    public static final String ERR_SUB_EXISTS                                   = "errSubNumExists";
    public static final String ERR_LOD_EXISTS_IN_NON_RECEIVING                  = "errLodExistsInNonReceiving";
    public static final String ERR_LOD_LONG                                     = "errLodnumTooLong";
    public static final String ERR_LOD_MXD_PRT_RECREATE                         = "errLodMxdPrtRecreate";
    public static final String ERR_LOD_SHORT                                    = "errLodnumTooShort";
    public static final String ERR_LODNUM_REQUIRED                              = "errLodnumRequired";
    public static final String ERR_MANDTE_SMALLER_EXPIREDTE                     = "errMandteSmallerExpiredte";
    public static final String ERR_MAX_UNITS                                    = "errMaxUnitsExceeded";
    public static final String ERR_MAX_LOADS                                    = "errMaxLoadsExceeded";
    public static final String ERR_MIXED_CLIENT_NOT_ALLOWED                     = "errMixedClientNotAllowed";
    public static final String ERR_MOV_INVENTORY                                = "errMoveInventory";
    public static final String ERR_MOV_NOT_ALLOWED                              = "errMoveNotAllowed";
    public static final String ERR_MUL_RECEIPTS                                 = "errMulReceipts";
    public static final String ERR_MUL_RECEIPTS_AT_LOC                          = "errMulReceiptsAtLoc";
    public static final String ERR_NEW_QTY_EXCEEDED                             = "errNewQtyExceeded";
    public static final String ERR_NO_AUTHORIZATION_SPLIT_SHIPMENT              = "dlgNoPermissionToSplitShipment";
    public static final String ERR_NO_AV_RCVSTG                                 = "errNoAvRcvAStg";
    public static final String ERR_NO_BATCH                                     = "errNoPicksInBatch";
    public static final String ERR_NO_CARMOV_FOR_TRLR                           = "errNoCarmovForTrlr";
    public static final String ERR_NO_INV_IN_CUR_BLDG                           = "errNoInvInCurBldg";
    public static final String ERR_NO_INV_TO_LOAD                               = "err10442";
    public static final String ERR_NO_PRTCLTID                                  = "errNoPartClientID";
    public static final String ERR_NO_MORE_CNT                                  = "errNoMoreCnt";
    public static final String ERR_NO_MULTIPLE_COUNTS                           = "errNoMultipleCounts";
    public static final String ERR_NO_INV_AT_LOC                                = "errNoInventoryAtLoc";
    public static final String ERR_NO_INV_AT_DEVICE                             = "errNoInventoryAtDevice";
    public static final String ERR_NO_MUL_KIT                                   = "errNoMultiKits";
    public static final String ERR_NO_MUL_SER                                   = "errNoMultiSerialized";
    public static final String ERR_NO_MULTIPLE_LOADS                            = "errNoMultipleLoads";
    public static final String ERR_NO_MATCHED_WORK_ORDER                        = "errNoMatchedWorkOrder";
    public static final String ERR_NO_PICKS_PENDING                             = "stsNoPicksPending";
    public static final String ERR_NO_REM_INV_TO_SCAN                           = "errNoRemInventoryToScan";
    public static final String ERR_NO_STOPS_TO_LOAD                             = "errNoStopsToLoad";
    public static final String ERR_NO_SERV_STOLOC_AVAILABLE                     = "errNoServStolocAvailable";
    public static final String ERR_NO_TRLR_AT_DOOR                              = "errNoTrlrAtDoor";
    public static final String ERR_NO_TRK_AT_DOOR                               = "errNoTrkAtDoor";
    public static final String ERR_NO_TRKNUM_FOR_TRLR                           = "errNoTrknumForTrlr";
    public static final String ERR_NO_UOMCODES_FOUND                            = "errNoUOMCodesFound";
    public static final String ERR_NO_VALUE_INPUT                               = "errMustInputConfirmValue";
    public static final String ERR_NOTHING_FOUND                                = "dlgNothingFound";
    public static final String ERR_NOT_SHIPPING_TRLR                            = "errNotShippingTrlr";
    public static final String ERR_OVR_REC_NOT_ALLOWED                          = "errOvrRecNotAllowed";
    public static final String ERR_PASSWORD_EXPIRED                             = "errPasswordExpired";
    public static final String ERR_PCK_NOT_CMP_FOR_SLOT                         = "errPickNotCmpForSlot";
    public static final String ERR_PENDING_BOX_IN_CARTON                        = "errPendingBoxInCarton";
    public static final String ERR_PICKING_INCOMPLETE_FOR_LOAD                  = "errPickingIncompleteForLoad";
    public static final String ERR_PRP_COUNT                                    = "errPrepareCycleCount";
    public static final String ERR_PRT_NOT_INV                                  = "errPartNotOnInvoice";
    public static final String ERR_PRT_NOT_WKO                                  = "errPartNotOnWorkOrder";
    public static final String ERR_QTY_DIV_KIT                                  = "errQtyMustBeDivbyKitQty";
    public static final String ERR_QTY_DIV_UC                                   = "errQtyMustBeDivbyUC";
    public static final String ERR_QTY_EXCEEDS_INV_ATTR_MATCH                   = "errQtyExceedsInvAttrMatch";
    public static final String ERR_QTY_POSITIVE                                 = "errQtyMustBePostive";
    public static final String ERR_RCV_ID_REQUIRED                              = "errRcvIdIsRequired";
    public static final String ERR_RCV_TRLR_HAS_DISC                            = "errReceiveTrailerHasDiscrepancies";
    public static final String ERR_RCV_TRK_HAS_DISC                             = "errReceiveTruckHasDiscrepancies";
    public static final String ERR_RCV_TRK_HAS_INV                              = "errReceiveTruckHasInventory";
    public static final String ERR_REACOD_REQUIRED                              = "errReacodIsRequired";
    public static final String ERR_RELABEL_LOAD                                 = "errRelabelingLoad";
    public static final String ERR_RF_HAS_INV                                   = "errRFHasInventory";
    public static final String ERR_SCANQTY_LESS_THAN_COMQTY                     = "errScanQtyLessThanComQty";
    public static final String ERR_SCANQTY_LESS_THAN_COMQTY_CONTINUE_YN         = "errScanQtyLessThanComQtyContinueYN";
    public static final String ERR_SERVICE_REQ                                  = "errServiceRequired";
    public static final String ERR_SERVICE_REQ_DOCKDROPR                        = "errReqServForDockDoorOprNotCompleted";
    public static final String ERR_SERNUM_REQUIRED                              = "errSernumRequired";
    public static final String ERR_SERNUM_SCANNED                               = "errSernumScanned";
    public static final String ERR_SERNUM_BEEN_SCANNED                          = "errSernumBeenScanned";
    public static final String ERR_SND_LMS_TRANS                                = "errSendLmsIndirect";
    public static final String ERR_SPLIT_LOAD                                   = "errSplitLoadforNextHop";
    public static final String ERR_STGLAN                                       = "errStgLan";
    public static final String ERR_SUBNUM_REQUIRED                              = "errSubnumRequired";
    public static final String ERR_TRLR_CLOSED                                  = "errTrlrClosed";
    public static final String ERR_TRLR_DISPATCHED                              = "errTrlrDispatched";
    public static final String ERR_TRLR_IS_RECEIVING                            = "errTrlrIsReceiving";
    public static final String ERR_TRLR_IS_SHIPPING                             = "errTrlrIsShipping";
    public static final String ERR_TRLR_NOT_AT_LOC                              = "errTrlrNotAtLoc";
    public static final String ERR_TRLR_NOT_MSTR_RCPT                           = "errTrlrNotMstrRcpt";
    public static final String ERR_TRLR_NUM_RQD                                 = "errTrlrnumRqd";
    public static final String ERR_TRLR_TRK_NUM_RQD                             = "errTrlrTrknumRqd";
    public static final String ERR_TRKNUM_NO_MATCH                              = "errTrknumNoMatch";
    public static final String ERR_TRKNUM_NOT_AT_LOC                            = "errTrknumNotAtLoc";
    public static final String ERR_TRKNUM_NOT_CI                                = "errTrknumNotCi";
    public static final String ERR_UPDATE_RF_INVADJ                             = "errUpdateRfInvadj";
    public static final String ERR_ZONE_PICKS_COMPLETE                          = "errPicksForZoneComplete";
    public static final String ERR_UNCOMPLETE_SER_INV                           = "errUncompleteSerInv";
    public static final String ERR_SERIALIZED_PART_NOT_ALLOWED                  = "errSerializedPartNotAllowed";
    public static final String ERR_MISSING_VAL                                  = "errMissingValue";
    public static final String ERR_MIXED_PALLET_NOT_ALLOWED                     = "errMixedPalletNotAllowed";
    public static final String ERR_MIXED_UOMCOD_NOT_ALLOWED                     = "errMixedUomcodNotAllowed";
    public static final String ERR_MULT_WKO_IN_SAME_PRDLIN                      = "errMultWkoOnSamePrdlin";
    public static final String ERR_INVALID_AREA_FOR_SHIP_STAGING_OVERRIDE       = "err11370";
    public static final String ERR_INVALID_CARRIER_FOR_SHIP_STAGING_OVERRIDE    = "err11371";
    public static final String ERR_LOC_FULL                                     = "errLocFull";
    public static final String ERR_LOC_FULL_ERR                                 = "errLocFullErr";
    public static final String ERR_LOGICAL_LOCATION                             = "errLogicalLocation";    
    public static final String ERR_NO_CATCH_QTY_FOR_REM_INV                     = "errNoCatchQtyForRemInv";
    public static final String ERR_NO_MOVE_MSTR_RCPT                            = "errNoMoveMstrRcpt";
    public static final String ERR_NO_TRLR_MSTR_RCPT                            = "errNoTrlrMstrRcpt";
    public static final String ERR_MSTR_RCPT_LOC_ERR                            = "errMstrRcptLocErr";
    public static final String ERR_MSTR_RCPT_FLG_ERR                            = "errMstrRcptFlgErr";
    public static final String ERR_MSTR_RCPT_UNLOADED                           = "errMstrRcptUnloaded";
    public static final String ERR_MSTR_RCPT_WRK_EXISTS                         = "errMstrRcptWrkExists";
    public static final String ERR_WRKREF_BELONGS_ASSET_LIST                    = "errWrkrefBelongstoAssetList";
    public static final String ERR_WRKREF_BELONGS_LIST                          = "errWrkrefBelongstoList";
    public static final String ERR_WKOR_NOT_STARTED                             = "err11248";
    public static final String ERR_INV_STOP_SEQ_LOAD_BY_BUILDING                = "err11456";
    public static final String ERR_CAN_NOT_OVERPICK_CATCH_QTY                   = "errCanNotOverPickCatchQty";
    public static final String ERR_IMPROPER_SEQ_TO_FLUID_LOAD                   = "errImproperSeqToFluidLoad";
    public static final String ERR_CHNG_AST_BFR_PUTAWAY                         = "errChngAstBfrPutaway";
    public static final String ERR_SUB_CNFRMD_AGAINST_ASN_LOAD                  = "errSubCnfrmdAgainstASNLoad";
    public static final String ERR_DTL_CNFRMD_AGAINST_ASN_LOAD                  = "errDtlCnfrmdAgainstASNLoad";
    public static final String ERR_SERNUM_CNFRMD_AGAINST_ASN_LOAD               = "errSerNumCnfrmdAgainstASNLoad";
    public static final String ERR_CONFLICTING_OR_MULT_DSTLOC_FOUND             = "err11373";
    public static final String ERR_INV_INT                                      = "err2109";
    public static final String ERR_INV_FLOAT                                    = "dlgttlInputInvalid";
    public static final String ERR_TRANSFER_PARTIAL_LOAD                        = "errTranserPartialLoad";
    public static final String ERR_PALLET_QTY_DOES_NOT_MATCH                    = "err10530";
    public static final String ERR_WITHIN_RANGE                                 = "errNumberNotWithinRange";
    public static final String ERR_INV_DST_LOD                                  = "errInvDstLod";
    public static final String ERR_LOAD_NOT_ON_TRLR                             = "errLoadNotOnTrlr";
    public static final String ERR_UNABLE_UNLOAD_MSTR_RCPT                      = "errUnableUnloadMstrRcpt";
    public static final String ERR_TRKNUM_CLOSED                                = "errTrknumIsClosed";
    public static final String ERR_TRKNUM_IS_CI                                 = "errTrknumIsCheckedIn";
    public static final String ERR_TRKNUM_IS_SUSP                               = "errTrknumIsSuspended";
    public static final String ERR_TRKNUM_IS_EXP                                = "errTrknumIsExpected";
    public static final String ERR_INV_RCVTRK                                   = "errInvalidRcvtrk";
    public static final String ERR_INV_TRLRSTAT                                 = "errInvalidTrlrStat";
    public static final String ERR_NO_INVENTORY_RCV                             = "errNoInventoryRcv";
    public static final String ERR_WRK_ALR_ACK                                  = "errWorkAlreadyAcknowledged";
    public static final String ERR_NO_DISCREPANCY_FOUND                         = "errNoDiscrepancyFound";
    public static final String ERR_NO_ELIGIBLE_WORK                             = "errNoEligibleWork";
    public static final String ERR_INV_PRDLIN                                   = "errInvalidPrdlin";
    public static final String ERR_NO_WKOS                                      = "errNoWkosExist";
    public static final String ERR_PRDLIN_EXCLWKOSTART                          = "errPrdlinHasExclWkoStarted";
    public static final String ERR_EXCLPRDLIN_WKOSTART                          = "errExclPrdlinHasWkoStarted";
    public static final String ERR_DEL_COMMITTED_INV                            = "errDelCommittedInv";
    public static final String ERR_NO_LIST_ID                                   = "errNoEligiblelist";
    public static final String ERR_TOID                                         = "errInvalidToId";
    public static final String ERR_LISTID_EXISTS                                = "errListIdExists";
    public static final String ERR_INVALID_SUBUCC                               = "errInvalidSubucc";
    public static final String ERR_INVENTORY_ALREADY_COUNTED                    = "errInvAlreadyCounted";
    public static final String ERR_CANT_RECEIVE_AGAINST_UNEXPECTED_RIMSTS       = "err11434";
    public static final String ERR_CANT_MOV_INV_FROM_STG_TO_RDT                 = "err10664";

    public static final String ERR_SPEC_MST_RCP_REQ                             = "errSpecMstRcpReq";
    public static final String ERR_SPEC_RCP_REQ                                 = "errSpecRcpReq";
    public static final String ERR_NO_OPEN_RCP_FOR_PO                           = "errNoOpenRcpForPo";
    public static final String ERR_SERNUM_NOT_EXIST_OR_ALREADY_HAD_DEVICE          = "errSerNumHadDevice";
    public static final String ERR_SERNUM_NOT_CAP                               = "errSerialNumNotCapturedForLoads";
    public static final String ERR_GLOM_EMPTY_DEST_FAILED                       = "errGlomEmptyDestFailed";
    public static final String ERR_NOT_PROPER_LODLVL                            = "errNotProperLoadLevel";
    public static final String ERR_INVALID_VALUE_FOR_SERIALIZED_ASSET           = "errInvalidValueforSerializedAsset";
    public static final String ERR_CAN_NOT_SKIP_CAPTURE                         = "errCanNotSkipCapture";
    public static final String ERR_INVALID_DST_LOD_SUB_ASN                      = "errInvalidDstLodForSubASN";

    // Over Receive Policy Values
    public static final String RTSTR1_OVRRCV_IDENTIFY                           = "IDENTIFY";
    public static final String RTSTR1_OVRRCV_RECEIVE                            = "RECEIVE";
    public static final String RTSTR2_OVRRCV_RIMLIN                             = "RIMLIN";
    public static final String RTSTR2_OVRRCV_RCVLIN                             = "RCVLIN";
    public static final String RTSTR2_OVRRCV_BOTH                               = "BOTH";
    public static final String POLCOD_PRD_OVRRECV                               = "PRODUCT-OVERRECEIPT";
    public static final String POLVAR_CHECK_POSITION                            = "CHECK-POSITION";
    public static final String POLVAR_CLUSTER_PICK                              = "CLUSTER-PICK";
    
    // Auto Close Trucks Policy values 
    public static final String RTSTR1_RCV_THIS_TRUCK_ONLY                       = "THIS-TRUCK-ONLY"; 
    
    // Reversing
    public static final String POLCOD_RECEIVE_INVENTORY                         = "RECEIVE-INVENTORY";
    public static final String POLVAR_RECEIVE_UNDO                              = "UNDO-RECEIPTS";
    public static final String POLVAL_ALLOW_REVERSE_AFTER_CLOSE                 = "ALLOW-REVERSE-AFTER-CLOSE";
    
    public static final String POLVAL_PICK_WEEK                                 = "PICK-WEEK";
    
    // Consignment Tracking
    public static final String TRK_CNSG_TRKALL                                  = "TRKALL";
    public static final String TRK_CNSG_TRKPARTS                                = "TRKPARTS";
    public static final String TRK_CNSG_TRKPRTSEXPT                             = "TRKPRTSEXPT";
    
    public static final String ERR_ACK_WRK_EXISTS                               = "errAckWrkExists";
    public static final String LOAD_PREPARATION_FOR_PICK                        = "P";
    public static final String LOAD_PREPARATION_FOR_DEPOSIT                     = "D";
    public static final String LOAD_PREPARATION_FOR_PICK_DEPOSIT                = "B";

    public static final String LOD_PREP_PROCESSING                              = "hlpLodPrepProcessing";
    
    // Auto Allocation Rule
    public static final String AUTO_ALC_RULE_TYPE_ORDER                         = "ORD";
    public static final String AUTO_ALC_RULE_TYPE_WORK_ORDER                    = "WKO";
    public static final String SCHEDULE_DATE_TYPE_EARLY_DLV_DTE                 = "EDD";
    public static final String SCHEDULE_DATE_TYPE_EARLY_SHP_DTE                 = "ESD";
    public static final String SCHEDULE_DATE_TYPE_LATE_DLV_DTE                  = "LDD";
    public static final String SCHEDULE_DATE_TYPE_LATE_SHP_DTE                  = "LSD";
    public static final String SCHEDULE_DATE_TYPE_SCH_BEG_DTE                   = "SBD";
    public static final String SCHEDULE_DATE_TYPE_SCH_END_DTE                   = "SED";
    public static final String SCHEDULE_DATE_TYPE_DUE_DTE                       = "DD";
    public static final String AUTO_ALLOCATION_METHOD_OFF                       = "OFF";
    public static final String AUTO_ALLOCATION_METHOD_ON_DOWNLOAD               = "D";
    public static final String AUTO_ALLOCATION_METHOD_SCHEDULED                 = "SCH";

    // Reason Code
    public static final String REACOD_AUTO_SKIP                                 = "AUTO_SKIP";
    public static final String REACOD_MANUAL_SKIP                               = "MANUAL_SKIP";
    public static final String REACOD_BAD_TRL_CNT                               = "BAD-TRL-CNT";
    public static final String REACOD_BAD_TRL_DATA                              = "BAD-TRL-DATA";
    public static final String REACOD_TRL_AUD_PASS                              = "TRL-AUD-PASS";
    public static final String REACOD_PRODUCTION_LINE_HOLD                      = "PRDLIN-HOLD";
    public static final String REACOD_LOCATION_FULL                             = "LOC_FULL";
    public static final String REACOD_DEKIT_ADJOUT                              = "DEKIT-ADJOUT";
    public static final String REACOD_DEKIT_ADJIN                               = "DEKIT-ADJIN";

    // Pallet Stack Restriction
    public static final String RESTRICTION_PALLET_STACK_HEIGHT                  = "P";
    public static final String RESTRICTION_INTERLOCK_STACK                      = "I";
    public static final String RESTRICTION_UNRESTRICTED                         = "N";
    
    // In Line Replenish Stages
    public static final String INLIN_RPL_STG_DISPLAY                            = "DISPLAY";
    public static final String INLIN_RPL_STG_RPLPCK                             = "RPLPCK";
    public static final String INLIN_RPL_STG_MULRPLPCK                          = "MULRPLPCK";
    public static final String INLIN_RPL_STG_RPLDEP                             = "RPLDEP";
    public static final String INLIN_RPL_STG_FKEY_RPLPCK                        = "FKEY_RPLPCK";
    
    // Constants for "Trailer is Busy" functionality
    public static final String CURRENT_TRAILER_ACTIVITY_OUTBOUND                = "Load";
    public static final String CURRENT_TRAILER_ACTIVITY_INBOUND                 = "Receive";
    public static final String CURRENT_TRAILER_ACTIVITY_SYSTEM_USER             = "SYSTEM";
    public static final String CURRENT_TRAILER_ACTIVITY_SYSTEM_DEVICE_CODE      = "SYSTEM";
    
    //Box in Box labels
    public static final String BOXID                                            = "box_id";

    //Truck Consignment Code
    public static final String TRK_CNSG_COD_TRKALL                              = "TRKALL";
    public static final String TRK_CNSG_COD_TRKPARTS                            = "TRKPARTS";
    public static final String TRK_CNSG_COD_TRKPRTSEXPT                         = "TRKPRTSEXPT";
    
    // WCS Integration 
    public static final String WCS_RELEASED                                     = "R";
    public static final String WCS_IS_RELEASED                                  = "released";
    public static final String WCS_PCKGRP                                       = "wcs_pck_grp";
    public static final String WCS_WRKREF                                       = "wrkref";
    public static final String WCS_WHID                                         = "wh_id";
    public static final String WCS_WRKTYP                                       = "wrktyp";
    public static final String WCS_PCKSTS                                       = "pcksts";
    public static final String WCS_EVT                                          = "evt_id";
    public static final String WCS_ID                                           = "wcs_id";
    public static final String WCS_WHERE                                        = "whereclause";
    public static final String WCS_SYSID                                        = "wcs_sys_id";
    public static final String WCS_PCKGRPS                                      = "pck_grps";
    public static final String WCS_EVT_TYP                                      = "evt_typ";
    public static final String WCS_ENABLED                                      = "enabled";
    public static final String WCS_PUTAWAY_FLG                                  = "putaway_flg";
    public static final String WCS_NUMCOD                                       = "numcod";
    public static final String WCS_STOLOC                                       = "stoloc";
    public static final String WCS_DIRECTION                                    = "direction";
    public static final String WCS_DIR_INB                                      = "I";
    public static final String WCS_DIR_OUT                                      = "O";
    public static final String WCS_DIR_WITHIN                                   = "W";
    public static final String WCS_DIR_NA                                       = "X";
    public static final String WCS_ARECOD                                       = "arecod";
    public static final String WCS_CONFIG_LOAD                                  = "load";
    public static final String WCS_CONFIG_AREA                                  = "A";
    public static final String WCS_CONFIG_WCSMST                                = "W";
    public static final String WCS_CONFIG_STOLOC                                = "L";
    public static final String WCS_SRCLOC                                       = "srcloc";
    public static final String WCS_DSTLOC                                       = "dstloc";
    public static final String WCS_SRCARE                                       = "srcare";
    public static final String WCS_DSTARE                                       = "dstare";
    public static final String WCS_MOV_EVT_TYP                                  = "INVMOV";
    public static final String WCS_DISC_TYPE_DISCREPANT                         = "D";
    public static final String WCS_DISC_TYPE_MISSING                            = "M";
    public static final String WCS_DISC_TYPE_ADDITIONAL                         = "A";

    public static final String IDENTIFY_LOAD                                    = "IDENTIFY_LOAD";

    // SERV_RESULT value of cnfrm_noninv_serv table
    public static final String WRKFLW_RESULT_FAIL                               = "FAIL";

    // RF Mode for calculating UOM unit quantity
    public static final String MODE_QTY_BY_UOM                                  = "QTY_BY_UOM";
    public static final String MODE_QTY_FOR_PCK_UOM                             = "QTY_FOR_PCK_UOM";
    public static final String MODE_QTY_FOR_MAX_UOM                             = "QTY_FOR_MAX_UOM";


}
