
package com.redprairie.les.mtfutil;

import com.redprairie.moca.MocaException;
import com.redprairie.moca.NotFoundException;
import com.redprairie.moca.MocaResults;
import com.redprairie.mtf.MtfConstants;
import com.redprairie.mtf.foundation.presentation.AFormLogic;
import com.redprairie.mtf.foundation.presentation.IDisplay;
import com.redprairie.mtf.foundation.presentation.IForm;
import com.redprairie.mtf.session.IFrameworkSession;
import com.redprairie.les.WMDConstants;
import com.redprairie.wmd.WMDErrors;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * This class performs the logic associated with the PRODUCT_PUT screen
 * for list picking processing.  It is meant primarily for scenarios in
 * which the to_id value is known and fixed at invocation time.  The
 * alternative was to re-use the existing form logic, but this showed
 * unacceptable performance issues with the form re-loading between
 * iterations.  This class is, essentially, a wrapper for the onFormEntry
 * and onFormExit methods for the PRODUCT_PUT form done in an iterative
 * loop over the result set acquired from the "get rf list details" command.
 *
 * @author jkramer
 */
public class CProductPutUtil {
    /**
     * A utility logger used to emit trace messages
     * for output in debugging efforts.
     */
    private static final Log LOG = LogFactory.getLog(CProductPutUtil.class);

    /**
     * Needed to acquire various elements for list pick processing.
     */
    private CLesMtfUtil wmdMtf;

    /**
     * The display object containing the information for these list picks.
     */
    private IDisplay display;

    /**
     * A reference to the main form object for displaying error and dialog
     * messages.
     */
    private IForm frmMain;

    /**
     * The session for this operation.
     */
    private IFrameworkSession session;

    /**
     * List Detail Results to hold between method calls.
     */
    private MocaResults dtlRes;

    /**
     * The form that called this logic piece (either PICKUP_A or PICKUP_B).
     */
    private String callerForm;

    /**
     * The toId value.
     */
    private String toId;

    /**
     * The current list to be processed.
     */
    private String currentListId;

    /**
     * The current work reference to be processed.
     */
    private String currentWrkRef;

    /**
     * The display to_id value.
     */
    private String displayToId;

    /**
     * The position id for this list picking effort.
     */
    private String positionId;

    /**
     * The display uom field used during inventory movement.
     */
    private String displayUom;

    /**
     * The load number associated with the move to perform for this list
     * picking effort.
     */
    private String loadNumber;

    /**
     * The previous (older) load number used before inventory was exploded
     * to its detail level for serialized items.  This is kept track of to
     * ensure that the same value is not picked from again inadvertently.
     */
    private String oldLoadNumber;

    /**
     * A holder for the catch quantity field (if applicable).
     */
    private String catchQuantity;

    /**
     * Used to keep track of serial number outbound capture values for use
     * when moving serialized inventory for list picks.
     */
    private String serNumOutboundCapture;

    /**
     * Used to keep track of serial number values at the sub-level for use
     * when moving serialized inventory for list picks.
     */
    private String serNumSubLevelMove;

    /**
     * The serial level to keep track of for list picking processing of items
     * that are serialized.
     */
    private String serialLevel;

    /**
     * The quantity to move for a given list pick detail.
     */
    private int moveQuantity;

    /**
     * Indicates whether serial number capture has occurred or
     * not for an item involved in this list picking effort.
     */
    private boolean serialCaptured = false;

    /**
     * Default constructor.
     *
     * @param mainForm The main form object to throw error dialogues on.
     * @param theDisplay The display object invoking the session.
     * @param theCallerForm The name of the form invoking this logic.
     * @param theToId The id where to put the inventory for these list picks.
     * @param movQty The quantity to move for this entire list pick effort.
     */
    public CProductPutUtil(final IForm mainForm, final IDisplay theDisplay,
            final String theCallerForm, final String theToId,
            final int movQty) {

        frmMain       = mainForm;
        display       = theDisplay;
        session       = display.getSession();
        wmdMtf        = (CLesMtfUtil) session.getGlobalObjMap().get("WMDMTF");
        dtlRes        = session.getGlobalMocaResults();
        callerForm    = theCallerForm;
        toId          = theToId;
        moveQuantity  = movQty;
        catchQuantity = "";
    }

    /**
     * This method resolves the processing for single list picks where
     * the to_id field is known and no validation is required.  The logic
     * invoked here is, essentially, the same as that for the PRODUCT_PUT
     * form but does NOT require form flow loading and the unnecessary
     * performance issues associated with it.  As it is only for single
     * lists with a known to_id entry, some elements of the logic here were
     * omitted since they are not valid scenarios here (i.e. there is no
     * support for cartonization list picking).
     *
     * @throws Exception If any command invocations or executions fail.
     * @return boolean True if list processing is successful, false otherwise.
     */
    public final boolean processListPick() throws Exception {
        boolean success = true;
        boolean    done = false;

        // Loop through and process list details until they are completed.
        while (!done) {
            done = initListDetails();
            if (!done) {
                success = processListDetails();
                if (!success) {
                    break;
                }

                if (moveQuantity == 0) {
                    done = true;
                }
            }
        }

        return success;
    }

    /**
     * This method initializes field values between call invocations.
     *
     * @return boolean True if processing here is complete, false otherwise.
     * @throws Exception If any form interactions or executions fail.
     */
    private boolean initListDetails() throws Exception {
        int totalQty = 0;

        // Re-initialize these between method calls.
        currentWrkRef = "";
        currentListId = "";

        // Get the list details to process.
        dtlRes = CLesMtfUtil.getRfListDetails(display,
            display.getVariable("global.wh_id"),
            display.getVariable("UNDIR_LIST_PICK.list_grp_id"),
            display.getVariable("PICKUP_A.dsploc"),
            display.getVariable("PICKUP_A.dspprt"));

        // Add up the pick quantity fields.
        while (dtlRes != null && dtlRes.next()) {
            if (currentListId.isEmpty()) {
                currentListId = dtlRes.getString("list_id");
                currentWrkRef = dtlRes.getString("wrkref");
            }
            else if (!currentListId.equals(dtlRes.getString("list_id"))) {
                continue;
            }

            totalQty += dtlRes.getInt("pckqty");
        }

        /**
         * Determine how much to attempt to move (the lower quantity of
         * the amount picked or the remaining quantity on the pick).
         */
        int qtyToMove = totalQty;
        if (totalQty > moveQuantity) {
            qtyToMove = moveQuantity;
        }

        dtlRes.reset();
        dtlRes.next();

        // Indicate to the caller if ALL moves are completed.
        if (qtyToMove == 0) {
            return true;
        }

        displayToId = wmdMtf.getToIdMap(currentListId);
        loadNumber  = wmdMtf.getToIdMap(currentListId);

        // Check for carton scenarios here.
        String cartonNumber = dtlRes.getString("ctnnum");
        if (cartonNumber != null && !cartonNumber.isEmpty()) {
            displayToId = cartonNumber;
        }

        LOG.trace("GOT TOID:  " + displayToId);

        // Capture the position id value in case it is needed.
        positionId = CLesMtfUtil.getPositionID(display,
            display.getVariable("global.wh_id"),
            currentWrkRef);

        /**
         * If execution gets here, this means that there is still quantity
         * of list pick details to process (either move or processing some
         * carton picks.  As such, this must be flagged to the caller such
         * that processing can (and does) continue.
         */
        return false;
    }

    /**
     * This method performs (essentially) the same functionality that the
     * onFormExit() method performs for the "PRODUCT_PUT" screen.
     *
     * @return boolean True if processing is successful, false otherwise.
     * @throws Exception If any form interactions or executions fail.
     */
    private boolean processListDetails() throws Exception {
        AFormLogic     newFrm = null;
        int         retStatus = WMDErrors.eOK;
        boolean      moveDone = false;
        boolean serialCapFlag = false;
        boolean       success = true;

        // Set the activity code to be LSTPCK.
        display.setVariable("INIT_POLICIES.active_actcod",
            WMDConstants.RF_ACTCOD_LST_PICK);

        do {
            if (moveQuantity == 0) {
                break;
            }

            if (!currentListId.equals(dtlRes.getString("list_id"))) {
                continue;
            }

            /**
             * Determine how much to attempt to move (the smaller of
             * the amount picked or remaining quantity on the pick).
             */
            int qtyToMove = dtlRes.getInt("pckqty");
            if (dtlRes.getInt("pckqty") > moveQuantity) {
                qtyToMove = moveQuantity;
            }

            // Inside the method initListDetails() there existed the code
            // to calculate catch quantity and flow to 'capture catch quantity'
            // form, but when there are multiple orders for the same item
            // in a list user is allowed to pick the combined quantities
            // of all the orders at a time, in this case catch quantity form
            // was displayed once where user used to enter catch quantity,
            // this catch quantity was getting updated to all the records of
            // that ship id. So to avoid this the code that calculates the
            // catch quantity is placed before the call to 'process inventory
            // move' such that for each wrkref the catch quantity form will be
            // called where he can enter individual catch qty for each wrkref.
            captureCatchWgt(qtyToMove);

            currentListId = dtlRes.getString("list_id");
            currentWrkRef = dtlRes.getString("wrkref");
            displayToId   = wmdMtf.getToIdMap(currentListId);
            loadNumber    = wmdMtf.getToIdMap(currentListId);

            // Check for cartonization scenarios.
            String cartonNumber = dtlRes.getString("ctnnum");
            if (cartonNumber != null && !cartonNumber.isEmpty()) {
                displayToId = cartonNumber;
            }

            if ((displayToId == null || displayToId.isEmpty())
                && (positionId == null || positionId.isEmpty())) {

                frmMain.promptMessageAnyKey("errToPosId");
                success = false;
                break;
            }

            /**
             * For inventory with serialized items, ser_typ is OUTCAP_ONLY.
             * The system would explode the existing inventory and create
             * new inventory to pick from.  It is important here to keep
             * track of the old load number so that NO attempt is made to
             * select the old, immovable, load for movement.
             */
            if (oldLoadNumber == null || oldLoadNumber.isEmpty()) {
                oldLoadNumber = display.getVariable("PICKUP_A.movlod");
            }

            serialCaptured = true;
            display.setVariable("PRCS_SER_NUM_CAP.dtlnum", display.getVariable("PICKUP_A.movdtl"));
            display.setVariable("PRCS_SER_NUM_CAP.untcas", display.getVariable("PICKUP_A.dspuntcas"));
            display.setVariable("PRCS_SER_NUM_CAP.untpak", display.getVariable("PICKUP_A.dspuntpak"));
            newFrm = display.createFormLogic("PRCS_SER_NUM_CAP", MtfConstants.EFlow.SHOW_FORM);
            newFrm.run();

            // Signal to the user that this is being processed.
            frmMain.displayMessage(MtfConstants.RF_MSG_PROCESSING);
            
            if (display.getVariable("PICKUP_A.sernum_cancel_flg").equals(
                                    MtfConstants.RF_FLAG_TRUE)) {
                success = false;
                break;
            }

            // Set the serialization fields as appropriate.
            String prevForm = display.getVariable("INIT_POLICIES.prvfrm");
            if ("PICKUP_A".equals(prevForm) || "PICKUP_B".equals(prevForm)) {
                serNumOutboundCapture = display.getVariable(prevForm
                    + ".sernum_outcap");

                serNumSubLevelMove = display.getVariable(prevForm
                    + ".sernum_sublmov");
            }
            // The previous form is not always PICKUPA
            // or PICKUPB. During serial capture the previous
            // form is set as PRCS_SER_NUM_CAP_DTL.Hence setting
            // the sernum_outcap to null.To solve this we check
            // whether any of the caller form's sernum_outcap
            // is set.If yes then we set serNumOutboundCapture
            // variable.
            else if (display.getVariable(callerForm
                    + ".sernum_outcap").equals(MtfConstants.RF_FLAG_TRUE)) {
                serNumOutboundCapture = display.getVariable(callerForm
                        + ".sernum_outcap");

                serNumSubLevelMove = display.getVariable(callerForm
                        + ".sernum_sublmov");
            }
            // Check whether we need to defer capturing serial numbers.
            try {
                session.executeDSQL(String.format(
                  "[select 1 from dual"
                + "  where exists "
                + " (select 1"
                + "    from pckwrk_view,"
                + "         prtmst_view,"
                + "         pckmov,"
                + "         poldat_view"
                + "  where pckwrk_view.prtnum = prtmst_view.prtnum"
                + "    and pckwrk_view.wh_id = prtmst_view.wh_id"
                + "    and pckwrk_view.prt_client_id = " 
                + "        prtmst_view.prt_client_id"
                + "    and pckmov.cmbcod = pckwrk_view.cmbcod"
                + "    and pckmov.wh_id = pckwrk_view.wh_id"
                + "    and poldat_view.wh_id = pckmov.wh_id"
                + "    and pckwrk_view.list_id = '" + currentListId + "'"
                + "    and prtmst_view.pakflg = 1"
                + "    and poldat_view.polcod = '" 
                + WMDConstants.POLCOD_PACKOUT + "' "
                + "    and poldat_view.polvar = '" 
                + WMDConstants.POLVAR_MISCELLANEOUS + "' "
                + "    and poldat_view.polval = '" 
                + WMDConstants.POLVAL_ARECOD + "'" 
                + "    and poldat_view.wh_id = '" 
                + display.getVariable("global.wh_id") + "'"
                + "    and poldat_view.rtstr1 = pckmov.arecod)]"));
            }
            catch (NotFoundException ex) {
                // Check whether the inventory has any serialized part in it.
                serialCapFlag = isSerialCaptureRequired();
            }
            
            // Check whether the inventory has any serialized part in it.
            // If the list group has cartons, process the carton picks.
            if (cartonNumber != null && !cartonNumber.isEmpty()) {
                // Check whether the kit is created or not.
                String createKit = "N";
                try {
                    session.executeDSQL(String.format(
                          "list inventory                "
                        + "    where invsub.subnum = '%s'",
                        displayToId));
                }
                catch (MocaException invEx) {
                    /**
                     * The invsub record has not been created yet, so
                     * inform the PICKUP form that a consolidated load
                     * and overpack kit will need to be created.
                     */
                    createKit = "Y";
                }

                // Create the map of values for carton pick processing.
                Map<String, Object> ctnPckMap = new HashMap<String, Object>();
                ctnPckMap.put("crelod", 0);
                ctnPckMap.put("crekit", createKit);
                ctnPckMap.put("actcod", display.getVariable("INIT_POLICIES.active_actcod"));
                ctnPckMap.put("oprcod", wmdMtf.getWrkQue().getOprCod());
                ctnPckMap.put("stoloc", display.getVariable("global.devcod"));
                ctnPckMap.put("lodnum", loadNumber);
                ctnPckMap.put("kitnum", dtlRes.getString("ctnnum"));
                ctnPckMap.put("srcloc", dtlRes.getString("srcloc"));
                ctnPckMap.put("lotnum", display.getVariable("PICKUP_A.lotnum"));
                ctnPckMap.put("srclod", display.getVariable("PICKUP_A.movlod"));
                ctnPckMap.put("srcsub", display.getVariable("PICKUP_A.movsub"));
                ctnPckMap.put("srcdtl", display.getVariable("PICKUP_A.movdtl"));
                ctnPckMap.put("prtnum", dtlRes.getString("prtnum"));
                ctnPckMap.put("prt_client_id", dtlRes.getString("prt_client_id"));
                ctnPckMap.put("srcqty", qtyToMove);
                ctnPckMap.put("src_catch_qty", display.getVariable("PICKUP_A.catch_qty"));
                ctnPckMap.put("dstloc", display.getVariable("global.devcod"));
                ctnPckMap.put("dstsub", displayToId);
                ctnPckMap.put("untcas", display.getVariable("PICKUP_A.untcas"));
                ctnPckMap.put("wrkref", dtlRes.getString("wrkref"));
                ctnPckMap.put("usr_id", display.getVariable("global.usr_id"));
                ctnPckMap.put("devcod", display.getVariable("global.devcod"));
                ctnPckMap.put("sernum_outcap", serNumOutboundCapture);
                ctnPckMap.put("sernum_sublmov", serNumSubLevelMove);
                ctnPckMap.put("wh_id", display.getVariable("global.wh_id"));
                ctnPckMap.put("asset_typ", display.getVariable("PRODUCT_PUT.dst_asset_typ"));
                ctnPckMap.put("load_attr1_flg", display.getVariable("PICKUP_A.load_attr1_flg"));
                ctnPckMap.put("load_attr2_flg", display.getVariable("PICKUP_A.load_attr2_flg"));
                ctnPckMap.put("load_attr3_flg", display.getVariable("PICKUP_A.load_attr3_flg"));
                ctnPckMap.put("load_attr4_flg", display.getVariable("PICKUP_A.load_attr4_flg"));
                ctnPckMap.put("load_attr5_flg", display.getVariable("PICKUP_A.load_attr5_flg"));

                if (!CLesMtfUtil.isLoadExist(display, loadNumber)) {
                    ctnPckMap.put("crelod", 1);
                }

                try {
                    retStatus = WMDErrors.eOK;
                    CLesMtfUtil.processCartonPick(display, ctnPckMap);
                }
                catch (MocaException ctnEx) {
                    retStatus = ctnEx.getErrorCode();
                }
    
                // Update the pck_to_id for the list with asset defined.
                if (retStatus == WMDErrors.eOK) {
                    display.setVariable("PICKUP_A.partial_pck_flg",
                        MtfConstants.RF_FLAG_FALSE);

                    if (serialCaptured) {
                        /**
                         * As a carton pick is being performed, remove
                         * the device context data that is exploded
                         * for serialized items.
                         */
                        CLesMtfUtil.deleteDevConOfSerial(display);
                    }

                    // Update pck_to_id here.
                    if (CLesMtfUtil.updatePickToIdForAssets(display,
                        currentListId, toId, currentWrkRef)) {

                        success = false;
                        break;
                    }
                }

                if (retStatus != WMDErrors.eOK) {
                    frmMain.displayErrorMessage();
                    if (serialCaptured) {
                        CLesMtfUtil.rollbackOubSerialCapture(display,
                            display.getVariable("PICKUP_A.dsploc"),
                            display.getVariable("PICKUP_A.dsplod"),
                            display.getVariable("PICKUP_A.movsub"),
                            display.getVariable("PICKUP_A.movlod"),
                            display.getVariable("PICKUP_A.lodnumP"),
                            display.getVariable("PICKUP_A.fndpos"),
                            dtlRes.getString("prtnum"),
                            dtlRes.getString("prt_client_id"),
                            display.getVariable("PICKUP_A.dspsts"),
                            display.getVariable("PICKUP_A.dsplot"),
                            display.getVariable("PICKUP_B.orgcod"),
                            display.getVariable("PICKUP_B.revlvl"),
                            display.getVariable("PICKUP_A.supnum"),
                            display.getVariable("PICKUP_A.dspuntcas"),
                            display.getVariable("PICKUP_A.dspuntpak"));

                        serialCaptured = false;
                    }
                }

                // Update the quantity remaining to move.
                moveQuantity -= qtyToMove;
                continue;
            }
            
            // If the pick work is assigned to an asset slot , then set the destination load number to
            // that slot load number.
            if(display.getVariable("SLOT_DEPOSIT.asset_slotdep_flg").equals(MtfConstants.RF_FLAG_TRUE)) {
                MocaResults rs = session.getGlobalMocaResults();
                try {
                    rs = null;
                    retStatus = WMDErrors.eOK;
                    rs = session.executeDSQL("[select slot_lodnum " +
                                             "  from pcklst_slot_load" +
                                             " where asset_slot = '" + dtlRes.getString("asset_slot") + "'" +
                                             "   and lodnum = '" + display.getVariable("SLOT_DEPOSIT.lodnum") + "']");
                }
                catch (MocaException e) {
                    retStatus = e.getErrorCode();
                }
                
                rs.next();
                toId = rs.getString("slot_lodnum");
            }

            // Create the map for inventory movement operations.
            Map<String, Object> movInvMap = new HashMap<String, Object>();
            movInvMap.put("wrkref", currentWrkRef);
            movInvMap.put("wh_id", display.getVariable("global.wh_id"));
            movInvMap.put("catch_qty", catchQuantity);
            movInvMap.put("srcloc", dtlRes.getString("srcloc"));
            movInvMap.put("srcsub", display.getVariable("PICKUP_A.movsub"));
            movInvMap.put("srcdtl", display.getVariable("PICKUP_A.movdtl"));
            movInvMap.put("prtnum", dtlRes.getString("prtnum"));
            movInvMap.put("prt_client_id", dtlRes.getString("prt_client_id"));
            movInvMap.put("srcqty", qtyToMove);
            movInvMap.put("src_catch_qty", catchQuantity);
            movInvMap.put("dstloc", display.getVariable("global.devcod"));
            movInvMap.put("dstlod", toId);
            movInvMap.put("dstsub", display.getVariable("PRODUCT_PUT.dest_sub"));
            movInvMap.put("newdst", display.getVariable("PICKUP_A.movnewdst"));
            movInvMap.put("untcas", display.getVariable("PICKUP_A.untcas"));
            movInvMap.put("untpak", display.getVariable("PICKUP_A.dspuntpak"));
            movInvMap.put("srcref", currentWrkRef);
            movInvMap.put("usr_id", display.getVariable("global.usr_id"));
            movInvMap.put("devcod", display.getVariable("global.devcod"));
            movInvMap.put("actcod", display.getVariable("INIT_POLICIES.active_actcod"));
            movInvMap.put("oprcod", wmdMtf.getWrkQue().getOprCod());
            movInvMap.put("sernum_outcap", serNumOutboundCapture);
            movInvMap.put("sernum_sublmov", serNumSubLevelMove);
            movInvMap.put("asset_typ", display.getVariable("PRODUCT_PUT.dst_asset_typ"));
            movInvMap.put("lotnum", display.getVariable("PICKUP_A.lotnum"));
            movInvMap.put("uomcod", displayUom);
            movInvMap.put("ftpcod", display.getVariable("PICKUP_A.dspftp"));

            /**
             * If the inventory has any serialized parts in it, then skip the
             * regular inventory move process, as these require movement at
             * the case level for items with a serialization level of S and
             * movement at the detail level for items with a serialization
             * level of D.
             */
            if (!serialCapFlag) {
                String ignRestrict = CLesMtfUtil.getIgnRestrictValue(display,
                    display.getVariable("global.wh_id"),
                    display.getVariable("PICKUP_A.dsploc"));

                /**
                 * It is possible the movlod was not set in PICKUP_A a if the
                 * system did not force the user to enter and inventory
                 * identifier.  Get the movlod here so the inventory can be
                 * successfully moved.  If picking from a location with
                 * multiple loads with the same item and attributes.
                 */
                String moveLoad = display.getVariable("PICKUP_A.movlod");
                if (("".equals(display.getVariable("PICKUP_A.movlod")))
                    || (display.getVariable("PICKUP_A.movlod") == null)) {

                    MocaResults lodRes = CLesMtfUtil.getAvailableInventoryForPickInLocation(
                        display,
                        display.getVariable("global.wh_id"),
                        currentWrkRef);

                    if (lodRes.next()) {
                 // Get the load number from this information to use.
                	moveLoad = lodRes.getString("lodnum");
                        
                        do {
                            if (lodRes.getString("lotnum").equals(movInvMap.get("lotnum"))) {                          
                                    moveLoad = lodRes.getString("lodnum");
                                    break;                          
                            }           
                        }while (lodRes.next());
                    }
                    else {
                        moveLoad = lodRes.getString("lodnum");
                    }  

                    }

                try {
                    retStatus = WMDErrors.eOK;
                    movInvMap.put("lodnum", moveLoad);
                    movInvMap.put("srclod", moveLoad);
                    movInvMap.put("ign_restrict", ignRestrict);
                    CLesMtfUtil.listPickInventoryMove(display, movInvMap);
                }
                catch (MocaException movEx) {
                    retStatus = movEx.getErrorCode();
                    frmMain.displayErrorMessage(movEx);

                    // Reset the position id field here.
                    if (!CLesMtfUtil.resetPositionId(display, currentWrkRef)) {
                        success = false;
                        break;
                    }
                }

                // Update the pck_to_id for the list with asset defined.
                if (retStatus == WMDErrors.eOK) {
                    display.setVariable("PICKUP_A.partial_pck_flg",
                        MtfConstants.RF_FLAG_FALSE);
                    if (display.getVariable("PICKUP_A.fndpos").length() > 0) {
                        CLesMtfUtil.enterDevConForMovCmplt(display, toId);
                    }

                    if (serialCaptured) {
                        /**
                         * As a carton pick is being performed, remove
                         * the device context data that is exploded
                         * for serialized items.
                         */
                        CLesMtfUtil.deleteDevConOfSerial(display);
                    }

                    // Update the pck_to_id value here.
                    if (!CLesMtfUtil.updatePickToIdForAssetsOnSpecificList(
                        display, currentListId, toId, currentWrkRef)) {

                        success = false;
                        break;
                    }
                }

                /**
                 * After the inventory is moved onto the asset, associate the
                 * load to that asset.
                 */
                if ("1".equals(display.getVariable("ASSET_IDENTIFY.ser_flg"))
                    && !CLesMtfUtil.processAssetForPick(display,
                        display.getVariable("PICKUP_A.asset_id"),
                        displayToId,
                        display.getVariable("global.wh_id"))) {

                    frmMain.displayErrorMessage();
                    success = false;
                    break;
                }

                // Update the quantity remaining to move.
                moveQuantity -= qtyToMove;

                // Determine if any inline count needs to be performed.
                processInlineCountIfRequired();

                // Process services here as needed.
                if (MtfConstants.RF_FLAG_TRUE.equals(display.getVariable("PICKUP_A.oub_serv_allowed"))) {
                    if (MtfConstants.RF_FLAG_TRUE.equals(display.getVariable("INIT_POLICIES.exitpntpostpick"))) {
                        /**
                         * If picked inventory is moved on to a new load, then
                         * efDestLod will have a value.  Use it here if present.
                         */
                        if (toId != null && !toId.isEmpty()) {
                            display.setVariable("GET_SERVICES.invtid", toId);
                        }
                        else {
                            display.setVariable("GET_SERVICES.invtid", "PICKUP_A.invtid");
                        }

                        display.setVariable("GET_SERVICES.wrkref", currentWrkRef);
                        display.setVariable("GET_SERVICES.untqty", Integer.toString(moveQuantity));
                        display.setVariable("GET_SERVICES.exitpnt_typ", WMDConstants.EXITPNT_TYP_SERVICE_OUB);
                        display.setVariable("GET_SERVICES.exitpnt", WMDConstants.EXITPNT_POST_PICK);
                        newFrm = display.createFormLogic("GET_SERVICES", MtfConstants.EFlow.SHOW_FORM);
                        newFrm.run();

                        /**
                         * If either a movlod or movsub value is available and
                         * wko_pick is false and invmov_typ is PICK, then do the
                         * MLSTFRMPRCORD and MOVEFRMPRCARE exit point services.
                         */
                        if (MtfConstants.RF_FLAG_FALSE.equals(display.getVariable("PICKUP_A.wko_pick"))) {
                            if (WMDConstants.INVMOV_TYP_PICK.equals(
                                display.getVariable("PICKUP_A.invmov_typ"))) {
                                if (!display.getVariable("PICKUP_A.invtid").equals(display.getVariable("PICKUP_A.movdtl"))) {
                                    /**
                                     * Determine if the area containing the location
                                     * is a processing area or not.
                                     */
                                    if (CLesMtfUtil.validateRfProcessingAreaForLocation(
                                        display, display.getVariable("global.wh_id"), toId)) {

                                        if (MtfConstants.RF_FLAG_TRUE.equals(display.getVariable("INIT_POLICIES.exitpntmovfrmprcare"))) {
                                            performMoveServices(WMDConstants.EXITPNT_MOVEFRMPRCARE);
                                        }

                                        if (MtfConstants.RF_FLAG_TRUE.equals(display.getVariable("INIT_POLICIES.exitpntmvlstfrmprcord"))) {
                                            performMoveServices(WMDConstants.EXITPNT_MVLSTFRMPRCORD);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                continue;
            }
            else if (serialCapFlag) {
                /**
                 * Enter this section of code only when
                 * considering serialized inventory.
                 */
                String ignRestrict = CLesMtfUtil.getIgnRestrictValue(display,
                    display.getVariable("global.wh_id"),
                    display.getVariable("PICKUP_A.dsploc"));

                String movDtl = "";
                int    movQty = 0;
                int pndQtyMov = qtyToMove;

                /**
                 * The "cnfrm_ser" field is set only when the serial number
                 * is already captured and the serial number needs to be
                 * confirmed.  When the serial number is confirmed in
                 * PRCS_SER_NUM_CNFRM, the inventory is moved and hence a
                 * move is not required again.  Therefore, the inventory
                 * move will be skipped and not performed here.
                 */
                if (!"1".equals(display.getVariable("PRODUCT_PUT.cnfrm_ser"))) {
                    MocaResults invRes = null;
                    /**
                     * Get all available inventory for the pick in the
                     * location for the given work reference.
                     */
                    if (display.getVariable("PICKUP_A.movlod") == null 
                        || display.getVariable("PICKUP_A.movlod").isEmpty()) {
                        
                        try {
                            invRes = CLesMtfUtil.getAvailableInventoryForPickInLocation(
                                display,
                                display.getVariable("global.wh_id"),
                                currentWrkRef);
                        }
                        catch (MocaException invEx) {
                            frmMain.displayErrorMessage(invEx);
                            success = false;
                            break;
                        }
                    }
                    else {
                        try {
                            invRes = session.executeDSQL(
                                         "[select invsub.lodnum, "
                                       + "        invsub.subnum, "
                                       + "        invdtl.dtlnum, "
                                       + "        invdtl.untqty "
                                       + "   from invsub, "
                                       + "        invdtl "
                                       + "  where invsub.subnum = "
                                       + "        invdtl.subnum "
                                       + "    and invsub.lodnum = '" + display.getVariable("PICKUP_A.movlod") + "'"
                                       + (display.getVariable("PICKUP_A.movsub").length() > 0 ?
                                         "    and invsub.subnum = '" + display.getVariable("PICKUP_A.movsub") + "'" : "")
                                       + (display.getVariable("PICKUP_A.movdtl").length() > 0 ?
                                         "    and invdtl.dtlnum = '" + display.getVariable("PICKUP_A.movdtl") + "'" : "")
                                       + "]");
                        }
                        catch (MocaException invEx) {
                            frmMain.displayErrorMessage(invEx);
                            success = false;
                            break;
                        }
                    }
                    
                    /**
                     * If the item in inventory is created with ser_typ
                     * OUTCAP_ONLY, then the old inventory load would have
                     * been exploded to create new loads.  If the ser_lvl is
                     * S/D, then the inventory needs to be moved one each at
                     * a time.  The system needs to loop until the quantity
                     * for that work reference is moved.  This is handled by
                     * pndQtyMov.  Also, this movement should not try using
                     * the inventory from the old load.
                     */
                    while (invRes != null && invRes.next() && pndQtyMov != 0) {
                        if (invRes.getString("lodnum").equals(
                            display.getVariable("PICKUP_A.movlod"))) {
                            /**
                             * This is used to build the values on the ser_lvl
                             * to make sure the inventory moves at a case
                             * level for the "S" ser_lvl value and at a detail
                             * level for the "D" ser_lvl value.
                             */
                            movQty = Integer.parseInt(invRes.getString("untqty"));
                            if (WMDConstants.SER_LVL_DETAIL.equals(serialLevel)) {
                                movDtl = invRes.getString("dtlnum");
                            }

                            // Update the map with these new values.
                            movInvMap.put("lodnum", invRes.getString("lodnum"));
                            movInvMap.put("srclod", invRes.getString("lodnum"));
                            movInvMap.put("srcsub", invRes.getString("subnum"));
                            movInvMap.put("srcdtl", movDtl);
                            movInvMap.put("srcqty", movQty);
                            movInvMap.put("ign_restrict", ignRestrict);

                            // Now try to move the inventory.
                            moveDone = true;
                            try {
                                CLesMtfUtil.listPickInventoryMove(display,
                                    movInvMap);
                            }
                            catch (MocaException movEx) {
                                moveDone = false;
                            }

                            if (!moveDone) {
                                if (!CLesMtfUtil.resetPositionId(display,
                                    currentWrkRef)) {

                                    success = false;
                                    break;
                                }

                                if (serialCaptured) {
                                    CLesMtfUtil.rollbackOubSerialCapture(display,
                                        display.getVariable("PICKUP_A.dsploc"),
                                        display.getVariable("PICKUP_A.dsplod"),
                                        display.getVariable("PICKUP_A.movsub"),
                                        display.getVariable("PICKUP_A.movlod"),
                                        display.getVariable("PICKUP_A.lodnumP"),
                                        display.getVariable("PICKUP_A.fndpos"),
                                        dtlRes.getString("prtnum"),
                                        dtlRes.getString("prt_client_id"),
                                        display.getVariable("PICKUP_A.dspsts"),
                                        display.getVariable("PICKUP_A.dsplot"),
                                        display.getVariable("PICKUP_B.orgcod"),
                                        display.getVariable("PICKUP_B.revlvl"),
                                        display.getVariable("PICKUP_A.supnum"),
                                        display.getVariable("PICKUP_A.dspuntcas"),
                                        display.getVariable("PICKUP_A.dspuntpak"));

                                    serialCaptured = false;
                                }
                            }
                            if (moveDone) {
                                if (display.getVariable(
                                    "PICKUP_A.fndpos").length() > 0) {
                                   CLesMtfUtil.enterDevConForMovCmplt(
                                        display, toId);
                                }
                                /**
                                 * Update the pck_to_id for the list with the
                                 * asset defined.
                                 */
                                if (moveDone
                                    && !CLesMtfUtil.updatePickToIdForAssetsOnSpecificList(
                                        display,
                                        currentListId,
                                        toId,
                                        currentWrkRef)) {

                                    success = false;
                                    break;
                                }
                            }

                            /**
                             * Update the quantity remaining to move and store
                             * the quantity that is yet to be moved (necessary
                             * when performing multiple list picking).
                             */
                            moveQuantity -= movQty;
                            pndQtyMov -= movQty;
                        }
                    }
                }

                /**
                 * If the "cnfrm_ser" is set, the inventory was already moved
                 * in PRCS_SER_NUM_CNFRM and that processing can be skipped
                 * here.  However, care must be taken to ensure that the
                 * pck_to_id for the list is updated properly if it has any
                 * assets associated to it.
                 */
                if ("1".equals(display.getVariable("PRODUCT_PUT.cnfrm_ser"))) {
                    // Update the quantity remaining to move.
                    moveQuantity -= qtyToMove;
                    if (!CLesMtfUtil.updatePickToIdForAssetsOnSpecificList(
                        display, currentListId, toId, currentWrkRef)) {

                        success = false;
                        break;
                    }
                }
            }
        }
        while (dtlRes != null && dtlRes.next());

        // Return the status of this operation.
        return success;
    }

    /**
     * This is to validate whether we the part is a serialized part.
     *
     * @return boolean True if serial capture is required, false otherwise.
     * @throws Exception If any form interactions or executions fail.
     */
    private boolean isSerialCaptureRequired() throws Exception {
        boolean retVal = true;

        try {
            MocaResults serRes = CLesMtfUtil.checkSerialNumberCaptureRequired(
                display,
                display.getVariable("global.wh_id"),
                dtlRes.getString("prtnum"),
                dtlRes.getString("prt_client_id"),
                display.getVariable("PRODUCT_PUT.cur_frm_typ"));

            serRes.next();
            serialLevel = serRes.getString("ser_lvl");
        }
        catch (MocaException serEx) {
            retVal = false;
        }

        return retVal;
    }

    /**
     * This method captures the catch quantity for this product put effort
     * by loading the "modal" form CATCH_QTY_CAP to acquire the value.
     *
     * @param uomQuantity The uom quantity value for this item.
     * @return boolean True if a catch quantity was captured, false otherwise.
     * @throws Exception If any form interactions or executions fail.
     */
    private boolean captureCatchQty(final int uomQuantity) throws Exception {
        AFormLogic newFrm = null;
        int       unitQty = 0;

        // Acquire the untqty value first.
        try {
            unitQty = CLesMtfUtil.getItemFootprintUnitQuantity(display,
                display.getVariable("global.wh_id"),
                dtlRes.getString("prtnum"),
                dtlRes.getString("prt_client_id"),
                dtlRes.getString("ftpcod"),
                displayUom);
        }
        catch (MocaException untQtyEx) {
            LOG.trace(String.format(
                  "Problem acquiring catch quantity for item [%s --> %s].",
                dtlRes.getString("prtnum"),
                dtlRes.getString("prt_client_id")));

            return false;
        }

        // The total is the product of the srcqty and untqty
        int totQty = uomQuantity * unitQty;

        // Get the original form value for this.
        String origForm = display.getVariable("CATCH_QTY_CAP.orig_frm");

        // Capture the catch quantity on the CATCH_QTY_CAP form here.
        Map<String, String> varMap = new HashMap<String, String>();
        varMap.put("prt_client_id", dtlRes.getString("prt_client_id"));
        varMap.put("prtnum", dtlRes.getString("prtnum"));
        varMap.put("current_num", "1");
        varMap.put("barcod_flg", "0");
        varMap.put("untqty", Integer.toString(uomQuantity));
        varMap.put("capuom", displayUom);
        varMap.put("totqty", Integer.toString(totQty));
        varMap.put("orig_frm", callerForm);

        setDisplayVariables("CATCH_QTY_CAP", varMap);
        newFrm = display.createFormLogic("CATCH_QTY_CAP",
            MtfConstants.EFlow.SHOW_FORM);

        newFrm.run();

        /**
         * After the form runs, the catch_qty field was set with the
         * correct value, so this can be flagged as a successful catch
         * quantity capture operation.  Also, be sure to reset the
         * original form value for the CATCH_QTY_CAP screen so we do NOT
         * return there again.
         */
        catchQuantity = display.getVariable(callerForm + ".catch_qty");
        display.setVariable("CATCH_QTY_CAP.orig_frm", origForm);
        return true;
    }

    /**
     * Sets the variables on this display and form to those values specified
     * in the mapping.
     *
     * @param formName The name of the form to use for ALL map elements.
     * @param varMap The map of name/value pairs to set on the display form.
     */
    private void setDisplayVariables(final String formName,
            final Map<String, String> varMap) {

        Iterator<String> varIter = varMap.keySet().iterator();
        while (varIter.hasNext()) {
            String     key = varIter.next();
            String varName = formName + "." + key;
            display.setVariable(varName, varMap.get(key));
        }
    }

    /**
     * This method will perform the outbound services associated with the
     * provided exit point.
     *
     * @param exitPoint The exit point for our service point hooks.
     * @throws Exception If any form interactions or executions fail.
     */
    private void performMoveServices(final String exitPoint) throws Exception {
        AFormLogic newFrm = null;

        if (toId != null && !toId.isEmpty()) {
            display.setVariable("GET_SERVICES.invtid", toId);
        }
        else {
            display.setVariable("GET_SERVICES.invtid",
                display.getVariable("PICKUP_A.invtid"));
        }

        display.setVariable("GET_SERVICES.exitpnt", exitPoint);
        display.setVariable("GET_SERVICES.stoloc",
            display.getVariable("PICKUP_A.dsploc"));

        display.setVariable("GET_SERVICES.exitpnt_typ",
            WMDConstants.EXITPNT_TYP_SERVICE_OUB);

        newFrm = display.createFormLogic("GET_SERVICES",
            MtfConstants.EFlow.SHOW_FORM);

        newFrm.run();
    }

    /**
     * This method determines if an inline count is needed or not.  It calls
     * the "get count near zero status" command to help determine this.  If
     * an inline count is needed, then it verifies whether the user is even
     * authorized to perform it or not before form flowing to the appropriate
     * form location.
     *
     * @throws Exception If any form invocations or interactions fail.
     */
    private void processInlineCountIfRequired() throws Exception {

        AFormLogic newFrm = null;

        // Only consider trying this if the count is required.
        if (CLesMtfUtil.isPerformCountRequired(display,
            display.getVariable("global.wh_id"),
            display.getVariable("PICKUP_A.dspprt"),
            display.getVariable("PICKUP_A.prt_client_id"),
            loadNumber,
            display.getVariable("PICKUP_A.dsploc"))) {

            // Validate that the user can perform this inline count.
            try {
                if (CLesMtfUtil.isUserValidForOpCode(display,
                    display.getVariable("global.wh_id"),
                    display.getVariable("global.devcod"),
                    display.getVariable("global.usr_id"),
                    getOperationCode())) {

                    /**
                     * Display STS_CNT_REQ message only when the
                     * user is authorized to do this CNT work.
                     */
                    frmMain.promptMessageAnyKey(WMDConstants.STS_CNT_REQ);
                    display.setVariable("PICKUP_A.nxtcntfrm", callerForm);
                    newFrm = display.createFormLogic("CNT_NEAR_ZERO");
                    newFrm.run();
                }
            }
            catch (MocaException oprCodEx) {
                display.beep();
                frmMain.displayErrorMessage();
            }
        }
    }

    /**
     * Acquires the operation code for the COUNT NEAR ZERO status.
     *
     * @return String The operation code for the COUNT NEAR ZERO status.
     * @throws MocaException If any command invocations or executions fail.
     */
    private String getOperationCode() throws MocaException {
        MocaResults cRes = CLesMtfUtil.getCountNearZeroOperationCode(display);
        cRes.next();
        return cRes.getString("oprcod");
    }

    /**
     * This method allows the user to enter catch quantity.
     *
     * @param qtyToMove The quantity that is sent to capture catch quantity.
     * @throws Exception if any form interactions or executions fail.
     */
    private void captureCatchWgt(int qtyToMove) throws Exception {
        // The uomQty is needed for catch qty capture.
        int uomQuantity = qtyToMove;
        try {
            MocaResults pckQtyRes = CLesMtfUtil.getQuantityForPickingUom(
                display,
                display.getVariable("global.wh_id"),
                dtlRes.getString("prtnum"),
                dtlRes.getString("prt_client_id"),
                dtlRes.getString("srcloc"),
                qtyToMove);

            // Grab the fields here.
            pckQtyRes.next();
            displayUom = pckQtyRes.getString("uomcod");
            uomQuantity = Integer.parseInt(pckQtyRes.getString("uomqty"));
        }
        catch (MocaException pckQtyEx) {
            displayUom = WMDConstants.UOMCOD_EACH;
        }

        // Handle the catch quantity field here if applicable.
        catchQuantity = "";
        if (("0".equals(display.getVariable("PICKUP_A.catchqtyBP")))
            && (CLesMtfUtil.isItemCatchQtyEnabled(display,
                dtlRes.getString("prtnum"),
                dtlRes.getString("prt_client_id")))) {

            // Attempt to capture the catch quantity value.
            captureCatchQty(uomQuantity);
        }
    }
}
