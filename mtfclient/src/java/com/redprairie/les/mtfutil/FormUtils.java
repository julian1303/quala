/*
 *  $URL$
 *  $Author$
 *  $Date$
 *  
 *  $Copyright-Start$
 *
 *  Copyright (c) 2011
 *  RedPrairie Corporation
 *  All Rights Reserved
 *
 *  This software is furnished under a corporate license for use on a
 *  single computer system and can be copied (with inclusion of the
 *  above copyright) only for use on such a system.
 *
 *  The information in this document is subject to change without notice
 *  and should not be construed as a commitment by RedPrairie Corporation.
 *
 *  RedPrairie Corporation assumes no responsibility for the use of the
 *  software described in this document on equipment which has not been
 *  supplied or approved by RedPrairie Corporation.
 *
 *  $Copyright-End$
 */

package com.redprairie.les.mtfutil;

import org.apache.log4j.Logger;
import com.redprairie.moca.MocaException;
import com.redprairie.moca.MocaResults;
import com.redprairie.mtf.MtfConstants.EFlow;
import com.redprairie.mtf.foundation.presentation.AFormLogic;
import com.redprairie.mtf.foundation.presentation.IDisplay;
import com.redprairie.mtf.session.IFrameworkSession;
import com.redprairie.les.WMDConstants;
import com.redprairie.wmd.WMDErrors;
import com.redprairie.wmd.formlogic.ErrorDisplay;
import java.util.HashMap;
import java.util.Map;

/**
 * A utility class that provides help utility methods for common form API's.
 * 
 * <b><pre>
 * Copyright (c) 2011 RedPrairie Corporation
 * All Rights Reserved
 * </pre></b>
 * 
 * @author smedic
 * 
 * @version $Revision$
 */
public class FormUtils {
    
    /**
     * Prompts a user with the Quantity Capture form where a user is able to
     * enter quantities for multi-level UOMs. The Quantity Capture screen has
     * been designed to be extensible - i.e. to allow it to be used anywhere
     * where a user needs to capture quantity in the RF. This method can be
     * used as a common utility API to allow uniform flowing to Quantity Capture
     * screen.<br><br>
     * 
     * <b>NOTE:</b> Quantity Capture form SHOULD NOT contain any WMS related
     * business logic in order to allow it to be extensible across the system.
     * 
     * @param display Reference to the IDisplay object
     * @param prtNum Item Number
     * @param prtClientId Item Client Id
     * @param stoLoc Storage Location
     * @param lodNum Load Number
     * 
     * @throws Exception thrown when form is popped off the stack or some other
     *             form runtime exception occurs.
     */
    public static void displayQuantityCaptureForm(IDisplay display,
            String prtNum, String prtClientId, String stoLoc, String lodNum)
            throws Exception {
        
        log.trace("Attempting to prompt a user with Quantity Capture form...");
        
        // First, let's setup the Quantity Capture form
        display.setVariable("QUANTITY_CAPTURE.prtnum", prtNum);
        display.setVariable("QUANTITY_CAPTURE.prt_client_id", prtClientId);
        display.setVariable("QUANTITY_CAPTURE.stoloc", stoLoc);
        display.setVariable("QUANTITY_CAPTURE.lodnum", lodNum);
        
        // OK, let's flow to the Quantity Capture screen. Here, we are going to
        // flow to Quantity Capture form with SHOW_FORM option, the program
        // execution will continue with the next line of instruction in calling
        // form after coming back from Quantity Capture screen.
        AFormLogic frm = display.createFormLogic("QUANTITY_CAPTURE",
            EFlow.SHOW_FORM);
        frm.run();
    }
    
    /**
     * Flows to {@link ErrorDisplay} form and prompts a user with the error
     * message specified per passed in parameter.
     * 
     * @param display Reference to the IDisplay object
     * 
     * @param errorMessage Error message text that will be displayed to a user
     * 
     * @throws Exception thrown when form is popped off the stack or some other
     *             form runtime exception occurs.
     */
    public static void showErrorDisplayForm(IDisplay display,
            String errorMessage) throws Exception {
        
        // First set the message text to be displayed to a user.
        display.setVariable("ERROR_DISPLAY.error_string", errorMessage);
        
        // Flow to the ERROR_DISPLAY form and prompt a user with the previuosly
        // set error message text...
        AFormLogic frm = display.createFormLogic("ERROR_DISPLAY",
            EFlow.SHOW_FORM);
        frm.run();
    }
    
    /**
     * This method accepts any identifier and it will try to find the trailer id
     * from it:
     * <ol>
     * <li>If f the id is a trailer id, then it will return that.</li>
     * <li>If the id is an inventory item, then it will check to see if the item
     * is on a trailer. if so, it will return the trailer_id.</li>
     * <li>If the id is a ship_id, then it will check to see if the ship id is
     * associated with a trailer via the stop id and carrier move id.</li>
     * If so, it will return the trailer id
     * <li>if the id is a master receipt id (trknum), then it will check to see
     * if a trailer is associated with it. If so, it will return the trailer id.
     * </li>
     * </ol>
     * 
     * @param session Reference to the MTF session object for this device.
     * @param identifier A value to identify the trailer master receipt ID, dock
     *            door, carrier move, or trailer ID
     * @param whId Warehouse Identifier
     * 
     * @return If found, a trlr_id is returned. Otherwise, an empty string is
     *         returned to the caller.
     */
    public static String getTrailerIdFromIdentifier(IFrameworkSession session,
            String identifier, String whId) {
        
        String trlrId = "";
        
        try {
            MocaResults rs = session.executeCommand(
                  "get trailer id from identifier" 
                + "    where identifier = '" + identifier + "'"
                + "      and wh_id      = '" + whId + "'");
            
            if (rs != null && rs.next()) {
                trlrId = rs.getString("trlr_id");
            }
        }
        catch (MocaException e) {
            // Could not retrieve trailer ID from the identifier.
        }
        
        return trlrId;
    }
    
    /**
     * Returns a boolean specifying whether or not a trailer is active. A
     * trailer is considered to be active if it's status is NOT Closed or
     * Expected.
     * 
     * @param session Reference to the MTF session object for this device.
     * @param trlrId Trailer ID
     * @param whId Warehouse Identifier
     * 
     * @return False, if the trailer isn't active. Otherwise, it returns true.
     */
    public static Boolean isTrailerActive(IFrameworkSession session,
            String trlrId, String whId) {
        
        try {
            MocaResults rs = session.executeCommand(
                  "[select trlr_stat"
                + "   from trlr"
                + "  where trlr_id      = '" + trlrId + "'"
                + "    and stoloc_wh_id = '" + whId + "']");
            
            if (rs != null && rs.next()) {
                String trlrStatus = rs.getString("trlr_stat");
                
                if (trlrStatus != null
                        && (trlrStatus.equals(WMDConstants.TRLSTS_CLOSED)
                          || trlrStatus.equals(WMDConstants.TRLSTS_EXPECTED))) {
                    
                    // Trailer is closed or expected!
                    return false;
                }
            }
        }
        catch (MocaException e) {
            // Since we could not retrieve the trailer records, we are going
            // to assume that the trailer is not busy.
            log.error("Trailer does not exist. Could not retrieve the trailer "
                + "status based on the arguments passed in: trlr_id ["
                + trlrId + "]; stoloc_wh_id [" + whId + "].", e);
            
            return false;
        }
        
        return true;
    }
    
    /**
     * Returns a boolean specifying whether or not a matching current trailer
     * activity record exists for passed arguments.
     * 
     * @param session Reference to the MTF session object for this device.
     * @param identifier Used to retrieve trlr_id. An identifier is either the
     *            trailer master receipt ID, dock door, carrier move ID, or
     *            trailer ID.
     * @param devCod Device Code
     * @param whId Warehouse Id
     * 
     * @return True if activity record exists. Otherwise, false is returned.
     */
    public static boolean trailerActivityExists(IFrameworkSession session,
            String identifier, String usrId, String devCod,
            String whId) {
        
        String trlrId = getTrailerIdFromIdentifier(session, identifier, whId);
        
        if (trlrId != null && !trlrId.isEmpty()) {
            try {
                session.executeCommand(
                      "list current trailer activity"
                    + "    where trlr_id = '" + trlrId + "'"
                    + "      and usr_id  = '"  + usrId + "'"
                    + "      and devcod  = '" + devCod + "'"
                    + "      and wh_id   = '" + whId + "'");
                
                // The current trailer activity was found.
                return true;
            }
            catch (MocaException e) {
                // No records found. It means that the current trailer activity
                // does not exist for this trailer.
            }
        }
        
        return false;
    }
    
    /**
     * Returns a boolean specifying whether or not a matching current trailer
     * activity record exists for passed arguments. <br>
     * This method overloads the {@link trailerActivityExists} method for when
     * we just care about if there is a lock and not if it our current user.
     * Common scenarios is when a user wants to move a trailer and we need to
     * ensure there is no activity from any user or device currently logged.
     * 
     * @param session Reference to the MTF session object for this device.
     * @param identifier Used to retrieve trlr_id. An identifier is either the
     *            trailer master receipt ID, dock door, carrier move ID, or
     *            trailer ID.
     * @param devCod Device Code
     * @param whId Warehouse Id
     * 
     * @return True if activity record exists. Otherwise, false is returned.
     */
    public static boolean trailerActivityExists(IFrameworkSession session,
            String identifier, String whId) {
        
        String trlrId = getTrailerIdFromIdentifier(session, identifier, whId);
        
        if (trlrId != null && !trlrId.isEmpty()) {
            try {
                session.executeCommand(
                      "list current trailer activity"
                    + "    where trlr_id = '" + trlrId + "'"
                    + "      and wh_id   = '" + whId + "'");
                
                // The current trailer activity was found.
                return true;
            }
            catch (MocaException e) {
                // No records found. It means that the current trailer activity
                // does not exist for this trailer.
            }
        }
        
        return false;
    }
    
    /**
     * Returns a boolean specifying whether or not the current trailer activity
     * record was created successfully for given parameters. <b> This
     * effectively MARKS the trailer as BUSY, which means that someone is
     * actively working on this trailer, so the system prevents users (RF or
     * GUI) from moving a trailer from the dock door.</b><br><br>
     * 
     * The purpose for marking trailer as busy is for safety purposes, so that a
     * warehouse manager can be advised when someone may physically be in a
     * trailer either loading or unloading the product. At times, it may become
     * necessary to move a trailer and the supervisor needs to have information
     * available to them to advise if it would be safe to do so.
     * 
     * @param session Reference to the MTF session object for this device.
     * @param identifier Used to retrieve trlr_id. An identifier is either the
     *            trailer master receipt ID, dock door, carrier move ID, or
     *            trailer ID.
     * @param userId User Id
     * @param devCode Device Code
     * @param whId Warehouse Id
     * @param activity Activity
     * 
     * @return True if record was created successfully. Otherwise, it returns
     *         false.
     */
    public static boolean createTrailerActivity(IFrameworkSession session,
            String identifier, String userId, String devCode, String whId,
            String activity) {
        
        String trlrId = getTrailerIdFromIdentifier(session, identifier, whId);
        
        if (trlrId != null && !trlrId.isEmpty()) {
            if (!trailerActivityExists(session, trlrId, userId, devCode, whId)
                    && isTrailerActive(session, trlrId, whId)) {
                try {
                    session.executeCommand(
                          "create current trailer activity"
                        + "    where trlr_id  = '" + trlrId + "'"
                        + "      and usr_id   = '" + userId + "'"
                        + "      and devcod   = '" + devCode + "'"
                        + "      and wh_id    = '" + whId + "'"
                        + "      and activity = '" + activity + "'");
                    
                    return true;
                }
                catch (MocaException e) {
                    log.error("Failed to MARK trailer as busy! Could not "
                        + "create the current trailer activity record. Args: "
                        + "trlr_id [" + trlrId + "]; usr_id [" + userId
                        + "]; devcod [" + devCode + "]; wh_id [" + whId
                        + "]; activity [" + activity + "].", e);
                }
            }
            else {
                return true;
            }
        }
        else {
            log.error("Cannot create the current trailer activity because the "
                + "system was unable to retrieve trailer ID from identifier."
                + "Args: trlr_id [" + trlrId + "]; usr_id [" + userId
                + "]; devcod [" + devCode + "]; wh_id [" + whId
                + "]; activity [" + activity + "].");
        }
        
        return false;
    }
    
    /**
     * Removes a current trailer activity record given the values it needs for
     * the primary key. If this is the last record in this table for this
     * trailer, it means that a trailer is <b>NOT BUSY</b> anymore.<br>
     * <br>
     * The purpose for marking trailer as busy is for safety purposes, so that
     * a warehouse manager can be advised when someone may physically be in a
     * trailer either loading or unloading the product. At times, it may become
     * necessary to move a trailer and the supervisor needs to have information
     * available to them to advise if it would be safe to do so.
     * 
     * @param session Reference to the MTF session object for this device.
     * @param identifier Used to retrieve trlr_id. An identifier is either the
     *            trailer master receipt ID, dock door, carrier move ID, or
     *            trailer ID.
     * @param userId User Id
     * @param devCode Device Code
     * @param whId Warehouse Id
     * 
     * @return True if record was removed successfully. Otherwise, false is
     *         returned.
     */
    public static boolean removeTrailerActivity(IFrameworkSession session,
            String identifier, String userId, String devCode, String whId) {
        
        String trlrId = getTrailerIdFromIdentifier(session, identifier, whId);
        
        if (trlrId != null && !trlrId.isEmpty()) {
            
            // Before removing the trailer activity, let's make sure that it
            // exists...
            if (trailerActivityExists(session, trlrId, userId, devCode, whId)) {
                try {
                    session.executeCommand(
                          "remove current trailer activity"
                        + "    where trlr_id = '" + trlrId + "'"
                        + "      and usr_id  = '" + userId + "'"
                        + "      and devcod  = '" + devCode + "'"
                        + "      and wh_id   = '" + whId + "'");
                    
                    return true;
                }
                catch (MocaException e) {
                    log.error("Failed to remove the current trailer activity. "
                        + "Trailer cannot be UNMARKED as busy. Args: trlr_id ["
                        + trlrId + "]; usr_id [" + userId + "]; devcod ["
                        + devCode + "]; wh_id [" + whId + "].", e);
                }
            }
        }
        else {
            log.error("Cannot remove the current trailer activity because the "
                + "system was unable to retrieve trailer ID from identifier."
                + "Args: trlr_id [" + trlrId + "]; usr_id [" + userId
                + "]; devcod [" + devCode + "]; wh_id [" + whId + "].");
        }
        
        return false;
    }
    
    /**
     * Overloaded method that is <b>specifically used when entering WMS
     * recovery</b>. Here, we want to make sure that we clear out all trailer
     * activity for this device to prevent system from making a lock on the 
     * trailer when it should not. If the system is able to remove all trailer
     * entries for this device, it will return TRUE. Otherwise, it returns
     * false.<br><br>
     * 
     * <b>NOTE:</b> <i>Only use this method when clearing out trailer activity
     * for WMS recovery case. Do NOT use it for the typical form flow when
     * device is in the pristine state because it is possible that you may
     * delete locks for multiple trlr_id's</i>.
     * 
     * @param session Reference to the MTF session object for this device.
     * @param devCode Device Code
     * @param whId Warehouse Identifier
     * 
     * @return boolean True, if the system is able to remove activity from
     *         cur_trlr_act table. Otherwise, it returns false.
     */
    public static boolean removeTrailerActivity(IFrameworkSession session,
            String devCode, String whId) {
        
        try {
            session.executeCommand(
                  "[delete from cur_trlr_act"
                + "  where devcod = '" + devCode + "'"
                + "    and wh_id  = '" + whId + "']");
        }
        catch (MocaException e) {
            return false;
        }
        
        return true;
    }
    
    /**
     * Returns a boolean specifying whether or not the size of terminal screen
     * is narrow (i.e handheld) for a passed-in session's display object. If the
     * screen is NOT narrow, then it is considered to be of wide type (i.e.
     * vehicle).
     * 
     * @param display References to session's display object
     * 
     * @return True if the terminal screen is narrow. Otherwise, it returs
     *         false.
     */
    public static boolean isNarrowTerminalScreen(IDisplay display) {
        return (display.getWidth() <= 20);
    }
    
    /**
     * Determines the maximum height on the screen in the terms of number of
     * rows based on terminal dimensions.
     * 
     * @param isNarrowTerminalScreen Boolean that specifies whether or not this
     *            terminal screen is narrow (i.e. handheld). If it is not
     *            narrow, then it is considered wide (i.e. vehicle).
     * 
     * @return Maximum height on the screen in terms of the number of rows based
     *         on terminal dimensions.
     */
    public static int getMaxTerminalRows(boolean isNarrowTerminalScreen) {
        return isNarrowTerminalScreen ? NARROW_TERMINAL_MAX_ROWS
                : WIDE_TERMINAL_MAX_ROWS;
    }
    
    /**
     * Returns the translated inventory identifier.
     * 
     * @param session Reference to the MTF session object for this device.
     * @param identifier A value to identify the trailer master receipt ID, dock
     *            door, carrier move, or trailer ID
     * @param itemClientId Item Client ID
     * @param whId Warehouse Identifier
     * 
     * @return If successful, MocaResults containing columns for translated
     *         inventory identifier. Otherwise, null is returned.
     */
    public static MocaResults getTranslatedInventoryIdentifier(
            IFrameworkSession session, String identifier, String itemClientId,
            String whId) {
        
        MocaResults rs = null;
        
        try {
            rs = session.executeCommand(
                  "get translated inventory identifier"
                + "    where identifier  = '" + identifier + "'"
                + "    and prt_client_id = '" + itemClientId + "'"
                + "    and wh_id         = '" + whId + "'");
        }
        catch (MocaException e) {
            log.error("Failed to retrieve translated inventory identifier.", e);
        }
        
        return rs;
    }
    
    /**
     * Returns the calculated unit quantity for supplied arguments.
     * 
     * @param session Reference to the MTF session object for this device.
     * @param itemNumber Item Number
     * @param itemClientId Item Client ID
     * @param footprintCode Footprint Code
     * @param whId Warehouse ID
     * @param uomQuantity UOM Quantity
     * @param uomCode UOM Code
     * 
     * @return If successful, calculated unit quantity is returned. Otherwise,
     *         zero is returned by default.
     */
    public static int getUnitQuantityByUomForItem(IFrameworkSession session,
            String itemNumber, String itemClientId, String footprintCode,
            String whId, int uomQuantity, String uomCode) {
        
        int unitQuantity = 0;
        
        try {
            MocaResults rs = session.executeCommand(
                  "get quantity by uom for part "
                + "    where prtnum        = '" + itemNumber + "'"
                + "      and prt_client_id = '" + itemClientId + "'"
                + "      and ftpcod        = '" + footprintCode + "'"
                + "      and wh_id         = '" + whId + "'"
                + "      and uomqty        = '" + uomQuantity + "'"
                + "      and uomcod        = '" + uomCode + "'");
            
            if (rs != null && rs.next()) {
                unitQuantity += rs.getInt("untqty");
            }
        }
        catch (MocaException e) {
            log.error("Failed to calculate unit quantity by UOM code for item.",
                e);
        }
        
        return unitQuantity;
    }
    
    /**
     * Returns the MocaResults containing calculated UOM quantity for supplied
     * arguments.
     * 
     * @param session Reference to the MTF session object for this device.
     * @param itemNumber Item Number
     * @param itemClientId Item Client ID
     * @param footprintCode Footprint Code
     * @param whId Warehouse ID
     * @param unitQuantity Unit Quantity
     * 
     * @return If successful, MocaResults is returned. Otherwise, MocaException
     *         will be thrown.
     * 
     * @throws MocaException Thrown if the system is unable to retrieve
     *             calculated item quantity.
     */
    public static MocaResults getItemQuantityForLargestUomLevel(
            IFrameworkSession session, String itemNumber, String itemClientId,
            String footprintCode, String whId, int unitQuantity)
            throws MocaException {
        
        MocaResults rs = null;
        
        try {
            rs = session.executeCommand(
                  "get part quantity for largest uomlvl"
                + "    where prtnum        = '" + itemNumber + "'"
                + "      and prt_client_id = '" + itemClientId + "'"
                + "      and ftpcod        = '" + footprintCode + "'"
                + "      and wh_id         = '" + whId + "'"
                + "      and untqty        = " + unitQuantity);
        }
        catch (MocaException e) {
            log.error("Failed to calculate item UOM quantity for largest UOM "
                + "level. Args: prtnum [" + itemNumber + "], prt_client_id ["
                + itemClientId + "], ftpcod [" + footprintCode + "], wh_id ["
                + whId + "], untqty [" + unitQuantity + "].", e);
            throw e;
        }
        
        return rs;
    }
    
    /**
     * Common code extracted from DepositA and FluidLoad used to complete pick work after
     * depositing loads.
     * 
     * @param session Reference to the MTF session object for this device.
     * @param display References to session's display object
     * @param wrkRef Work reference to complete.
     */
    public static void completeWork(IFrameworkSession session, 
            IDisplay display, String wrkRef) {
        String tempReqnum = null;
        String lblbat= null;
        MocaResults rs = session.getGlobalMocaResults();
        int retStatus = WMDErrors.eOK;
        
        try{
            rs = null;
            retStatus = WMDErrors.eOK;
            rs = session.executeDSQL(
                    "[select pckwrk_hdr.lblbat " +
                    "   from pckwrk_hdr " +  
                    "  where pckwrk_hdr.lblbat in (select lblbat " +
                    "                            from pckwrk_hdr " +
                    "                           where wrkref = '" + wrkRef +"')" +
                    "    and pckwrk_hdr.appqty < pckwrk_hdr.pckqty " +
                    "    and pckwrk_hdr.wrkref != '" + wrkRef + "'" +
                    "    and wh_id = '" + display.getVariable("global.wh_id") + "']");
        }
        catch(MocaException e){
            try{
                rs = null;
                retStatus = WMDErrors.eOK;
                rs = session.executeDSQL(
                        "[select lblbat " +
                        "   from pckwrk_hdr " +  
                        "  where pckwrk_hdr.wrkref = '" + wrkRef + "'" +
                        "    and wh_id = '" + display.getVariable("global.wh_id") + "']");
                
                rs.next();
                
                lblbat = rs.getString("lblbat");
            }
            catch(MocaException e2){
            }
            
            if(lblbat != null)
            {
                try {
                    rs = null;
                    retStatus = WMDErrors.eOK;
                    rs = session.executeDSQL(
                            "[ select reqnum" +
                            "    from wrkque " +
                            "   where lblbat = '" + lblbat + "'" + 
                            "     and wh_id = '" + display.getVariable("global.wh_id") + "']");
                    rs.next();
                    tempReqnum = rs.getString("reqnum");
                }
                catch (MocaException f) {
                    retStatus = f.getErrorCode();
                }
                // If tempReqnum is not there, then call to complete work
                // results in Unexpected Server Error. Hence check whether
                // it exists before calling the command.
                if (tempReqnum != null &&  retStatus == WMDErrors.eOK) {
                    try {
                        rs = null;
                        retStatus = WMDErrors.eOK;
                        rs = session.executeDSQL(
                                "complete work " +
                                "    where reqnum = '" + tempReqnum + "'" +
                                "      and prcmod = 'WMDConstants.PRCMOD_NOMOVE'" + 
                                "      and wh_id = '" + display.getVariable("global.wh_id") + "'");
                    }
                    catch (MocaException f) {
                        retStatus = f.getErrorCode();
                    }
                }
            }
            else{
                try {
                        rs = null;
                        retStatus = WMDErrors.eOK;
                        rs = session.executeDSQL(
                                "[ select reqnum" +
                                "    from wrkque " +
                                "   where wrkref = '" + wrkRef + "'" + 
                                "     and wh_id = '" + display.getVariable("global.wh_id") + "']");
                        rs.next();
                        tempReqnum = rs.getString("reqnum");
                }
                catch (MocaException f) {
                       retStatus = f.getErrorCode();
                }
                if (tempReqnum != null &&  retStatus == WMDErrors.eOK) {
                    try {
                        rs = null;
                        retStatus = WMDErrors.eOK;
                        rs = session.executeDSQL(
                                "[select 'x' "
                              + "   from pckwrk_hdr "
                              + "  where pckwrk_hdr.appqty != "
                              + "        pckwrk_hdr.pckqty "
                              + "    and pckwrk_hdr.wrkref = '"
                              + wrkRef + "'"
                              + "    and wh_id = '"
                              + display.getVariable("global.wh_id") + "']");
                    }
                    catch (MocaException e3) {
                        try {
                            rs = null;
                            retStatus = WMDErrors.eOK;
                            rs = session.executeDSQL(
                                    "complete work "
                                  + "  where reqnum = '"
                                  + tempReqnum + "'"
                                  + "    and prcmod = "
                                  + "'WMDConstants.PRCMOD_NOMOVE'"
                                  + "    and wh_id = '"
                                  + display.getVariable("global.wh_id") + "'");
                        }
                        catch (MocaException f) {
                            retStatus = f.getErrorCode();
                        }
                    }
                }
            }
        }
    }

    /**
     * Get the untqty based on the UOM/item/wrkref information passed.
     * 
     * @param session Reference to the MTF session object for this device.
     * @param warehouseId warehouse id.
     * @param uomQty UOM quantity entered by the user.
     * @param uomCod UOM code selected by the user.
     * @param item Item number.
     * @param itemClient Item client.
     * @param ftpCod FTP Code.
     * @param wrkRef Work reference.
     * @param mode UOM mode.
     */
    public static HashMap<String, String> getProcessingUomQuantity(IFrameworkSession session, 
            String warehouseId,
            String uomQty,
            String uomCod, 
            String item,
            String itemClient, 
            String ftpCod,
            String wrkRef,
            String mode) {

        String untQty = null;
        String uomCode = null;
        Map<String, String> returnValues = new HashMap<String, String>();
        try {
            StringBuilder sb = new StringBuilder("get quantity for processing uom ");
            sb.append(" where wh_id = '" + warehouseId + "'");
            sb.append(" and mode = '" + mode + "'");

            if (item != null) {
                sb.append("  and prtnum = '" + item + "'");
            }
            if (itemClient != null) {
                sb.append("  and prt_client_id = '" + itemClient + "'");
            }
            if (ftpCod != null) {
                sb.append("  and ftpcod = '" + ftpCod + "'");
            }
            if (uomQty != null) {
                sb.append("  and untqty = '" + uomQty + "'");
            }
            if (uomCod != null) {
                sb.append("  and uomcod = '" + uomCod + "'");
            }
            if (wrkRef != null) {
                sb.append("  and wrkref = '" + wrkRef + "'");
            }

            MocaResults rs = session.executeCommand(sb.toString());

            if (rs != null && rs.next()) {
                untQty = Integer.toString(rs.getInt("untqty"));
                uomCode = rs.getString("uomcod");
                if (uomCode == null) {
                    uomCode = WMDConstants.UOMCOD_EACH;
                }
                log.trace("Quantity for UOM " + untQty);
            }
        }
        catch (MocaException e) {
            log.error("Error occured while obtaining quantity for processing UOM...");
            uomCode = WMDConstants.UOMCOD_EACH;
        }

        returnValues.put("uomqty", untQty);
        returnValues.put("uomcod", uomCode);
        return (HashMap)returnValues;
    }

    private static final Logger log = Logger.getLogger(FormUtils.class);
    

    /**
     * Defines maximum number of rows for wide terminal screen (i.e. vehicle).
     * The maximum number is actually 8, but framework includes row zero into
     * the count.
     */
    private static final int WIDE_TERMINAL_MAX_ROWS = 7;

    /**
     * Defines maximum number of rows for narrow terminal screen (i.e.
     * handheld). The maximum number is actually 16, but framework includes row
     * zero into the count.
     */
    private static final int NARROW_TERMINAL_MAX_ROWS = 15;

}
